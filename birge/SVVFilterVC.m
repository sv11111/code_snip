//
//  SDGFilterVC.m
//  Birge
//
//  Created by Семен Власов on 09.08.15.
//  Copyright (c) 2015 SVV. All rights reserved.
//

#import "SDGFilterVC.h"
#import "SDGComboCell.h"
#import "SDGFilterModel.h"
#import "SDGLabelCell.h"
#import "SDGSliderCell.h"
#import "SDGTextCell.h"
#import "AFNetworking.h"
#import "APIManager.h"
#import "SDGCategoryModel.h"
#import "NSString+Escaping.h"
#import "NSDictionary+NSNull.h"

static NSString *const DefaultCityId = @"524";

@interface SDGFilterVC () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, CellEventsDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *waitView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (IBAction)clearBtnTouchUp:(id)sender;
@end

@implementation SDGFilterVC {
    NSDictionary *_filterValues;
    NSArray *_defaultFilters;
    
    NSMutableArray *_categories;
    NSMutableArray *_filters;
    
    NSArray *_categoriesTree;
    NSDictionary *_categoriesDict;
    
    UITextField *_activeTextField;
    
    BOOL _tableReloadData;
    BOOL _waiting;
    
    CGFloat _curKeyboardHeight;
}


- (instancetype)initWithCategory:(NSString *)categoryId filterValues:(NSDictionary *)filterValues {
    self = [super initWithNibName:@"SDGFilterVC" bundle:nil];
    if (self) {
        if (categoryId) {
            _categoryId = [NSString stringWithString:categoryId];
        }
        if (filterValues) {
            _filterValues = [NSDictionary dictionaryWithDictionary:filterValues];
        }
        
        _waiting = YES;
        _filters = [[NSMutableArray alloc] init];
        
        __weak typeof(self) wself = self;
        [[APIManager sharedInstance] getCategoriesCompletion:^(NSArray *categories, NSDictionary *categoriesDict) {
            _categoriesTree = categories;
            _categoriesDict = categoriesDict;
            
            [wself getCategoriesAndFilters:_categoryId completionHandler:^() {
                [UIView animateWithDuration:0 animations:^{
                    _tableReloadData = NO;
                    [wself.tableView reloadData];
                } completion:^(BOOL finished) {
                    _tableReloadData = YES;
                    _waiting = NO;
                    [wself updateWaitView];
                }];
            }];
        }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    
    return self;
}

- (void)getCategoriesAndFilters:(NSString *)categoryId completionHandler:(void(^)())completionHandler {
    
    _categories = [[NSMutableArray alloc] init];
    [self addChildItemForId:categoryId];
    [self addParentItemsForId:categoryId];
    
    if (_categoryId != nil) {
        NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=getFilterByCategoryId&category_id=%@&city_id=%@",categoryId, DefaultCityId];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [[APIManager sharedInstance] loadRequest:request completion:^(NSArray *result) {
            _filters = [[NSMutableArray alloc] init];
            for (NSDictionary *filterData in result) {
                NSMutableArray *values = [[NSMutableArray alloc] init];
                NSMutableArray *valueIds = [[NSMutableArray alloc] init];
                NSArray *valuesData = [filterData safeObjectForKey:@"values"];
                for (NSDictionary *value in valuesData) {
                    [values addObject:[value safeObjectForKey:@"value"]];
                    [valueIds addObject:[value safeObjectForKey:@"value_id"]];
                }
                SDGFilterModel *item = [[SDGFilterModel alloc] initWithId:[filterData safeObjectForKey:@"code"] name:[filterData safeObjectForKey:@"name"] type:SDGFilterTypeCombo values:[NSDictionary dictionaryWithObjects:values forKeys:valueIds]];
                [_filters addObject:item];
            }
            [self addDefaultFilters];
            [self fillSelectedValues];
            if (completionHandler) {
                completionHandler();
            }
        
        } failure:^(NSError *error) {

        }];
    }
    else if (completionHandler) {
        completionHandler();
    }
}

- (void)addDefaultFilters {
    SDGFilterModel *item = [[SDGFilterModel alloc] initWithId:@"P1_MIN" name:@"от, RUB" type:SDGFilterTypeText values:nil];
    [_filters addObject:item];
    
    item = [[SDGFilterModel alloc] initWithId:@"P1_MAX" name:@"до, RUB" type:SDGFilterTypeText values:nil];
    [_filters addObject:item];
    
    item = [[SDGFilterModel alloc] initWithId:@"sort_field"
                                         name:@"Сортировка"
                                         type:SDGFilterTypeCombo
                                       values:[NSDictionary dictionaryWithObjects:@[@"По дате", @"По цене", @"По метро"]
                                                                          forKeys:@[@"date", @"price", @"metro",]]];
    item.selectedValue = @"date";
    [_filters addObject:item];
    
    item = [[SDGFilterModel alloc] initWithId:@"sort_order"
                                         name:@"Направление сортировки"
                                         type:SDGFilterTypeCombo
                                       values:[NSDictionary dictionaryWithObjects:@[@"Убывание", @"Возрастание"]
                                                                          forKeys:@[@"desc", @"asc"]]];
    item.selectedValue = @"desc";
    [_filters addObject:item];
    
    item = [[SDGFilterModel alloc] initWithId:@"onlyPhoto" name:@"Только с фото" type:SDGFilterTypeSlider values:nil];
    [_filters addObject:item];
    
    item = [[SDGFilterModel alloc] initWithId:@"onlyFavorite" name:@"Только избранное" type:SDGFilterTypeSlider values:nil];
    [_filters addObject:item];
    
}

- (void)fillSelectedValues {
    if (!_filterValues || ([_filterValues count] == 0)) {
        return;
    }
    for (SDGFilterModel *item in _filters) {
        if (item && item.filterId) {
            item.selectedValue = [_filterValues safeObjectForKey:item.filterId];
        }
    }
}

- (void)fillResults {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    for (SDGFilterModel *item in _filters) {
        if (item.selectedValue && item.filterId) {
            [result setObject:item.selectedValue forKey:item.filterId];
        }
    }
    _filterValues = [NSDictionary dictionaryWithDictionary:result];
}

- (void)addChildItemForId:(NSString *)categoryId {
    SDGCategoryModel *model = [_categoriesDict objectForKey:categoryId];
    if (model && model.childArr && [model.childArr count]) {
        NSMutableDictionary *comboValues = [[NSMutableDictionary alloc] init];
        for (SDGCategoryModel* cId in model.childArr) {
            [comboValues setObject:cId.name forKey:cId.categoryId];
        }
        SDGFilterModel *item = [[SDGFilterModel alloc] initWithId:categoryId name:@"Раздел" type:SDGFilterTypeCombo values:comboValues];
        [_categories addObject:item];
    }
}

- (void)addParentItemsForId:(NSString *)categoryId {
    SDGCategoryModel *model = [_categoriesDict objectForKey:categoryId];
    SDGCategoryModel *parentModel = model ? [_categoriesDict objectForKey:model.parentId] : nil;
    NSMutableDictionary *comboValues;
    for (; parentModel; ) {
         comboValues = [[NSMutableDictionary alloc] init];
        for (SDGCategoryModel* cId in parentModel.childArr) {
            [comboValues setObject:cId.name forKey:cId.categoryId];
        }
        SDGFilterModel *item = [[SDGFilterModel alloc] initWithId:parentModel.categoryId name:@"Раздел" type:SDGFilterTypeCombo values:comboValues];
        item.selectedValue = model.categoryId;
        [_categories insertObject:item atIndex:0];
        model = parentModel;
        parentModel = [_categoriesDict objectForKey:model.parentId];
    }
    comboValues = [[NSMutableDictionary alloc] init];
    for (SDGCategoryModel *rootModel in _categoriesTree) {
        [comboValues setObject:rootModel.name forKey:rootModel.categoryId];
    }
    SDGFilterModel *item = [[SDGFilterModel alloc] initWithId:nil name:@"Раздел" type:SDGFilterTypeCombo values:comboValues];
    item.selectedValue = (model) ? model.categoryId : nil;
    [_categories insertObject:item atIndex:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self prepareNavBar];
    
    UINib *nib = [UINib nibWithNibName:@"SDGComboCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SDGComboCell"];
    
    nib = [UINib nibWithNibName:@"SDGSliderCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SDGSliderCell"];
    
    nib = [UINib nibWithNibName:@"SDGTextCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SDGTextCell"];
    
    self.tableView.separatorColor = [UIColor clearColor];
    [self updateWaitView];
    // Do any additional setup after loading the view from its nib.
}

- (void)updateWaitView {
    if (!_waitView || !_spinner) {
        return;
    }
    if (_waiting) {
        [_waitView setHidden:NO];
        [_spinner setHidden:NO];
        [_spinner startAnimating];
    }
    else {
        [_waitView setHidden:YES];
        [_spinner setHidden:YES];
        [_spinner stopAnimating];
    }
}

- (void)prepareNavBar {
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    UIBarButtonItem *lb = [[UIBarButtonItem alloc] initWithTitle:@"Отмена" style:UIBarButtonItemStylePlain target:self
                                                          action:@selector(cancelBtnTouchUp:)];
    self.navigationItem.leftBarButtonItem = lb;
    UIBarButtonItem *rb = [[UIBarButtonItem alloc] initWithTitle:@"Применить" style:UIBarButtonItemStylePlain target:self
                                                          action:@selector(applyBtnTouchUp:)];
    self.navigationItem.rightBarButtonItem = rb;
    
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x30495D);
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont boldSystemFontOfSize:14]}];
    
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x0083F2),
                                                                    NSFontAttributeName: [UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x0083F2),
                                                                     NSFontAttributeName: [UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
}

- (void)cancelBtnTouchUp:(id)sender {
    [self cellSelected:nil];
    [self fillResults];
    if (_delegate && [_delegate respondsToSelector:@selector(filterCanceled)]) {
        [_delegate performSelector:@selector(filterCanceled)];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)applyBtnTouchUp:(id)sender {
    [self cellSelected:nil];
    [self fillResults];
    if (_delegate && [_delegate respondsToSelector:@selector(filterApplied:categoryId:)]) {
        [_delegate performSelector:@selector(filterApplied:categoryId:) withObject:[NSDictionary dictionaryWithDictionary:_filterValues] withObject:_categoryId];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (IBAction)clearBtnTouchUp:(id)sender {
    [self cellSelected:nil];
    [self fillResults];
    if (_delegate && [_delegate respondsToSelector:@selector(filterCleaned)]) {
        [_delegate performSelector:@selector(filterCleaned)];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];

}

- (void)comboCellWillOpened:(UITableViewCell *)cell {
    if ([cell isKindOfClass:[SDGComboCell class]] && _tableReloadData) {
        [_tableView reloadRowsAtIndexPaths:@[[_tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)comboCellWillClosed:(UITableViewCell *)cell {
    if ([cell isKindOfClass:[SDGComboCell class]] && _tableReloadData) {
        CGPoint pointInTable = [cell convertPoint:cell.frame.origin toView:_tableView];
        CGPoint contentOffset = _tableView.contentOffset;
        if (contentOffset.y > pointInTable.y) {
            [_tableView setContentOffset:(CGPoint){0, MAX(pointInTable.y -  50, 0)} animated:YES];
        }
        
        [_tableView reloadRowsAtIndexPaths:@[[_tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)cellValueChanged:(SDGFilterModel *)model cell:(UITableViewCell *)cell {
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    if (model.filterType == SDGFilterTypeCombo && index.section == 0) {
        _waiting = YES;
        [self updateWaitView];
        [self fillResults];
        _categoryId = [NSString stringWithString:model.selectedValue];
        [self getCategoriesAndFilters:model.selectedValue completionHandler:^() {
            [UIView animateWithDuration:0 animations:^{
                _tableReloadData = NO;
                [self.tableView reloadData];
            } completion:^(BOOL finished) {
                _tableReloadData = YES;
                _waiting = NO;
                [self updateWaitView];
            }];
        }];
        
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section != 0) return 20;
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) {
        UIView *v = [UIView new];
        v.backgroundColor = [UIColor clearColor];
        return v;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SDGFilterModel *tmpItem = [self getFilterModel:indexPath];
    return (tmpItem) ? [tmpItem rowHeightForModel] : 52;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    else {
        return [_filters count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row >= [_categories count]) {
        SDGComboCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SDGComboCell" forIndexPath:indexPath];
        SDGFilterModel *m = [[SDGFilterModel alloc] initWithId:nil name:@"Раздел" type:SDGFilterTypeCombo values:nil];
        m.isEnabled = NO;
        [cell setModel:m];
        cell.delegate = self;
        return cell;
    }
    SDGFilterType ft = [self getFilterModel:indexPath].filterType;
    if (ft == SDGFilterTypeCombo) {
        SDGComboCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SDGComboCell" forIndexPath:indexPath];
        [cell setModel:[self getFilterModel:indexPath]];
        cell.delegate = self;
        return cell;
    }
    if (ft == SDGFilterTypeSlider) {
        SDGSliderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SDGSliderCell" forIndexPath:indexPath];
        [cell setModel:[self getFilterModel:indexPath]];
        cell.delegate = self;
        return cell;
    }
    if (ft == SDGFilterTypeText) {
        SDGTextCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SDGTextCell" forIndexPath:indexPath];
        [cell setModel:[self getFilterModel:indexPath]];
        cell.tfDelegate = self;
        return cell;
    }
    return nil;
}

#pragma mark CellEventsDelegate

- (SDGFilterModel *)getFilterModel:(NSIndexPath*)index {
    if (index.section == 0) {
        return ([_categories count] > index.row) ? [_categories objectAtIndex:index.row] : nil;
    }
    else {
        return ([_filters count] > index.row) ? [_filters objectAtIndex:index.row] : nil;
    }
}

- (void)cellSelected:(UITableViewCell *)cell {
    if (_activeTextField) {
        [_activeTextField endEditing:YES];
        _activeTextField = nil;
    }
}

#pragma mark UITextFieldDelegate and keyboard handlers

- (void)keyboardWillShow:(NSNotification *)notif {
    CGSize kbSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _curKeyboardHeight = kbSize.height;
    if (_activeTextField) {
        [self animateTextFieldOffset];
    }
}

- (void)keyboardWillHide:(NSNotification *)notif {
    _curKeyboardHeight = 0;
    
    UIView *tmpV = nil;
    if (_activeTextField) {
        @try {
            tmpV = _activeTextField.superview;
            if (![tmpV isKindOfClass:[UITableViewCell class]]) {
                tmpV = _activeTextField.superview.superview;
                if (![tmpV isKindOfClass:[UITableViewCell class]]) {
                    tmpV = _activeTextField.superview.superview.superview;
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
        }
        
        if (tmpV != nil) {
            UITableViewCell *cell = (UITableViewCell*)tmpV;
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            _activeTextField = nil;
        }
    }
}

- (void)animateTextFieldOffset {
    if (_activeTextField) {
        CGPoint pointInTable = [_activeTextField.superview convertPoint:_activeTextField.frame.origin toView:self.tableView];
        CGPoint contentOffset = self.tableView.contentOffset;
        
        contentOffset.y = (pointInTable.y - _activeTextField.inputAccessoryView.frame.size.height) - (_tableView.frame.size.height/2 - _curKeyboardHeight/2) + 30;
        [UIView animateWithDuration:0.3 animations:^{
            self.tableView.contentOffset = contentOffset;
        }];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _activeTextField = textField;
    
    if (_curKeyboardHeight > 0) {
        [self animateTextFieldOffset];
    }
    
    return YES;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
