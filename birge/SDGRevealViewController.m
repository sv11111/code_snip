//
//  SDGevealViewController.m
//  Birge
//
//  Created by Семен Власов on 15.08.15.
//  Copyright (c) 2015 SVV. All rights reserved.
//

#import "SDGRevealViewController.h"

@interface SDGRevealViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong)IBOutlet UIView *frontView;
@property (nonatomic, strong)IBOutlet UIView *backView;

@end

@implementation SDGRevealViewController {
    CGFloat _backViewShownWidth;
    CGFloat _maxBackViewShownOverdraw;
    CGFloat _previousPanOffset;
    CGFloat _backViewPresentationWidth;
    CGFloat _triggerWidthToShowBackView;
    CGFloat _triggerWidthToHideBackView;
    CGFloat _flickVelocity;
    
    NSTimeInterval _toggleAnimationDuration;
    UIPanGestureRecognizer *_panGRecognizer;
    UITapGestureRecognizer *_frontViewTapGesture;
    BOOL _isPangGestureProcessing;
}

- (id)init {
    self = [super init];
    if (self != nil) {
        [self setDefaultsConfig];
    }
    return self;
}

static SDGRevealViewController *sharedReveal = nil;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        sharedReveal = self;
        [self setDefaultsConfig];
    }
    return self;
}

+ (SDGRevealViewController *)sharedInstance {
    if (!sharedReveal) {
        sharedReveal = [[SDGRevealViewController alloc] init];
    }
    
    return sharedReveal;
}

- (void)setDefaultsConfig {
    _toggleAnimationDuration = 0.3f;
    _backViewShownWidth = 275.0f;
	_maxBackViewShownOverdraw = 10.0f;
    _backViewPresentationWidth = 275.0f;
	_triggerWidthToShowBackView = 145.0f;
	_triggerWidthToHideBackView = 160.0f;
	_flickVelocity = 1300.0f;
    _isPangGestureProcessing = YES;
    _previousPanOffset = 0.0f;
    _curFrontViewPos = FrontViewPositionLeft;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0x1C2935);
    
    _backViewShownWidth = _backView.frame.size.width;
    
    _frontViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognize:)];
    _frontViewTapGesture.enabled = NO;
    [_frontView addGestureRecognizer:_frontViewTapGesture];
    
    _panGRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognize:)];
    _panGRecognizer.delegate = self;
    [self.view addGestureRecognizer:_panGRecognizer];
}

- (void)showToggle{
    if (_curFrontViewPos == FrontViewPositionLeft) {
		[self frontViewAnimation:_toggleAnimationDuration isBackHiding:NO];
		_curFrontViewPos = FrontViewPositionRight;
        [self setTapGestureForFrontView];
	}
	else if (self.curFrontViewPos == FrontViewPositionRight) {
		[self frontViewAnimation:_toggleAnimationDuration isBackHiding:YES];
        _curFrontViewPos = FrontViewPositionLeft;
        [self removeTapGestureForFrontView];
	}
}

- (void)setTapGestureForFrontView {
    NSArray *subviews = _frontView.subviews;
    for (UIView *tmpV in subviews) {
        tmpV.userInteractionEnabled = NO;
    }
    _frontViewTapGesture.enabled = YES;
}

- (void)removeTapGestureForFrontView {
    NSArray *subviews = _frontView.subviews;
    for (UIView *tmpV in subviews) {
        tmpV.userInteractionEnabled = YES;
    }
    _frontViewTapGesture.enabled = NO;
}

- (void)frontViewAnimation:(NSTimeInterval)duration isBackHiding:(BOOL)isBackHiding {
	[UIView animateWithDuration:duration delay:0.0f options:
     UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^ {
         if (isBackHiding)
             _frontView.frame = CGRectMake(0.0f, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
         else
             _frontView.frame = CGRectMake(_backViewShownWidth, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
     }
        completion:^(BOOL finished) {
     }];
}


- (void)frontViewAnimationForRightState:(NSTimeInterval)duration {
    [UIView animateWithDuration:duration delay:0.0f options:
     UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^ {
         _frontView.frame = CGRectMake(_backViewShownWidth - 10, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
     }
                     completion:^(BOOL finished) {
         [UIView animateWithDuration:duration delay:0.0f options:
          UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                          animations:^ {
              _frontView.frame = CGRectMake(_backViewShownWidth, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
          }
                          completion:^(BOOL finished) {
              
          }];
     }];
}

#pragma mark Gestures handlers

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (_curFrontViewPos == FrontViewPositionRight)
        return NO;
    return YES;
}

- (void)tapGestureRecognize:(UITapGestureRecognizer *)recognizer {
    switch ([recognizer state]) {
        case UIGestureRecognizerStateEnded: {
            if (self.curFrontViewPos == FrontViewPositionRight) {
                [self showToggle];
            }
        }
            break;
        default:
            break;
    }
}

- (void)panGestureRecognize:(UIPanGestureRecognizer *)recognizer {
	switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan:
            [self processGestureStateBegan:recognizer];
            break;
		case UIGestureRecognizerStateChanged:
            [self processGestureStateChanged:recognizer];
			break;
			
		case UIGestureRecognizerStateEnded:
			[self processGestureStateEnded:recognizer];
			break;
			
		default:
			break;
	}
}

- (void)processGestureStateBegan:(UIPanGestureRecognizer *)recognizer {
    CGPoint pos = CGPointZero;
    if (recognizer != nil) {
        if (recognizer.numberOfTouches > 0) {
            @try {
                pos = [recognizer locationOfTouch:0 inView:_frontView];
            }
            @catch (NSException *exception) {
            }
        }
        
        if (pos.x > 70)
            _isPangGestureProcessing = NO;
        else
            _isPangGestureProcessing = YES;
    }
}

- (void)processGestureStateChanged:(UIPanGestureRecognizer *)recognizer {
    if (!_isPangGestureProcessing || recognizer == nil)
        return;
    
    if (self.curFrontViewPos == FrontViewPositionLeft) {
		if ([recognizer translationInView:self.view].x < 0.0f) {
			_frontView.frame = (CGRect){0, 0, _frontView.frame.size};
		}
		else {
			float offset = [self calculateOffsetForTranslationInView:[recognizer translationInView:self.view].x];
			_frontView.frame = (CGRect){offset, 0, _frontView.frame.size};
		}
	}
	else {
		if ([recognizer translationInView:self.view].x > 0.0f) {
			float offset = [self calculateOffsetForTranslationInView:([recognizer translationInView:self.view].x + _backViewShownWidth)];
			_frontView.frame = (CGRect){offset, 0, _frontView.frame.size};
		}
		else if ([recognizer translationInView:self.view].x > -_backViewShownWidth) {
			_frontView.frame = (CGRect){[recognizer translationInView:self.view].x + _backViewShownWidth, 0, _frontView.frame.size};
		}
		else {
			_frontView.frame = (CGRect){0, 0, _frontView.frame.size};
		}
	}
}

- (void)processGestureStateEnded:(UIPanGestureRecognizer *)recognizer {
    if (!_isPangGestureProcessing || recognizer == nil)
        return;
    
	if (fabs([recognizer velocityInView:self.view].x) > _flickVelocity) {
		if ([recognizer velocityInView:self.view].x > 0.0f) {
			[self frontViewAnimation:_toggleAnimationDuration isBackHiding:NO];
		}
		else {
            [self frontViewAnimation:_toggleAnimationDuration isBackHiding:YES];
		}
	}
	else {
		float dynamicTriggerLevel = (_curFrontViewPos == FrontViewPositionLeft) ? _triggerWidthToShowBackView : _triggerWidthToHideBackView;
		
		if (_frontView.frame.origin.x >= dynamicTriggerLevel && _frontView.frame.origin.x != _backViewShownWidth) {
            if (_frontView.frame.origin.x < _backViewShownWidth)
                [self frontViewAnimation:_toggleAnimationDuration isBackHiding:NO];
            else
                [self frontViewAnimationForRightState:_toggleAnimationDuration];
		}
		else {
            [self frontViewAnimation:_toggleAnimationDuration isBackHiding:YES];
		}
	}

	if (_frontView.frame.origin.x <= 0.0f) {
		_curFrontViewPos = FrontViewPositionLeft;
        [self removeTapGestureForFrontView];
	}
	else {
		_curFrontViewPos = FrontViewPositionRight;
        [self setTapGestureForFrontView];
	}
}

- (CGFloat)calculateOffsetForTranslationInView:(CGFloat)x {
    CGFloat result;
    if (x <= _backViewShownWidth) {
        result = x;
    }
    else if (x <= _backViewShownWidth + (M_PI * _maxBackViewShownOverdraw / 2.0f)) {
        result = _maxBackViewShownOverdraw * sin((x - _backViewShownWidth) / _maxBackViewShownOverdraw) + _backViewShownWidth;
    }
    else {
        result = _backViewShownWidth + _maxBackViewShownOverdraw;
    }
    
    return result;
}

@end
