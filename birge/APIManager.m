//
//  APIManager.m
//  Birge
//
//  Created by Семен Власов on 11.08.15.
//  Copyright (c) 2014 SVV. All rights reserved.
//

#import "APIManager.h"
#import "SDGCategoryModel.h"
#import <AFNetworking.h>
#import "SDGStringDefines.h"
#import "SDGAnnouncementModel.h"
#import "NSDictionary+NSNull.h"
#import "SDGAnnDetailsModel.h"
#import "NSString+Escaping.h"

@implementation APIManager {
    AFNetworkReachabilityStatus _curStatus;
    NSArray *_categroies;
    NSDictionary *_categoriesDict;
    NSMutableArray *_categoryReqArr;
    NSDate *_lastCategoryUpdateDate;
    BOOL _isCategoryAlreadyLoaded;
}

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        _curStatus = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
        
        __weak typeof(self) wself = self;
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            if (status == AFNetworkReachabilityStatusNotReachable && _curStatus != AFNetworkReachabilityStatusNotReachable) {
                [wself connectionLost];
            }
            if (status != AFNetworkReachabilityStatusNotReachable && _curStatus == AFNetworkReachabilityStatusNotReachable) {
                [wself connectionEstablished];
            }
            
            _curStatus = status;
        }];
        
        _categroies = @[];
        _lastCategoryUpdateDate = [NSDate date];
        
        [self loadCategoriesFromCache];
        [self loadAllCategories:nil];
    }
    return self;
}

- (void)getCategoriesCompletion:(void(^)(NSArray *categories, NSDictionary *categoriesDict))completion {
    if (_categroies != nil && [_categroies count] > 0) {
        completion(_categroies, _categoriesDict);
        [self loadAllCategories:nil];
    }
    else {
        [self loadAllCategories:completion];
    }
}

static const NSUInteger kTimeForUpdateCategories = 600;
- (void)loadAllCategories:(void(^)(NSArray *categories, NSDictionary *categoriesDict))completion  {
    NSDate *curDate = [NSDate date];
    if (!_isCategoryAlreadyLoaded || [curDate timeIntervalSinceDate:_lastCategoryUpdateDate] > kTimeForUpdateCategories) {
        __weak typeof(self) wself = self;
        NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=getCategories"];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [self loadRequest:request completion:^(NSArray *result) {
            if (result != nil && [result count] > 0) {
                _categroies = @[];
                NSMutableDictionary *modelDict = [[NSMutableDictionary alloc] initWithCapacity:[result count]];
                for (NSDictionary *dict in result) {
                    SDGCategoryModel *model = [wself getCategoryModelFromDict:dict];
                    if (model.parentId != nil) {
                        SDGCategoryModel *parentModel = [modelDict objectForKey:model.parentId];
                        if (parentModel == nil) {
                            parentModel = [SDGCategoryModel new];
                            parentModel.categoryId = model.parentId;
                            [modelDict setObject:parentModel forKey:model.parentId];
                        }
                        parentModel.childArr = [parentModel.childArr arrayByAddingObject:model];
                    }
                    else {
                        _categroies = [_categroies arrayByAddingObject:model];
                    }
                    SDGCategoryModel *tmpModel = [modelDict objectForKey:model.categoryId];
                    if (tmpModel != nil) model.childArr = tmpModel.childArr;
                    
                    [modelDict setObject:model forKey:model.categoryId];
                }
                _categoriesDict = [NSDictionary dictionaryWithDictionary:modelDict];
            }
            _isCategoryAlreadyLoaded = YES;
            if (completion) {
                completion(_categroies, _categoriesDict);
            }
        } failure:^(NSError *error) {
            
        }];
    }
}

- (SDGCategoryModel *)getCategoryModelFromDict:(NSDictionary *)dict {
    SDGCategoryModel *model = nil;
    if (dict != nil) {
        model = [SDGCategoryModel new];
        model.categoryId = [dict safeObjectForKey:@"category_id"];
        model.name = [dict safeObjectForKey:@"name"];
        model.parentId = [dict safeObjectForKey:@"parent_id"];
    }
    return model;
}

- (void)loadCategoriesFromCache {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:ud_CategoriesModel];
    if (data != nil) {
        _categroies = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

- (void)saveCache {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_categroies];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:ud_CategoriesModel];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

- (void)getPopularAdsWithCompletion:(void(^)(NSArray *resultArr))completion page:(NSUInteger)pageNumber {
    NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=getPopular&city_id=326&page_num=%d&page_size=30", pageNumber];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    __weak typeof(self) wself = self;
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        NSArray *items = [result safeObjectForKey:@"items"];
        NSMutableArray *resultArr = [[NSMutableArray alloc] initWithCapacity:items.count];
        for (NSDictionary *dict in items) {
            SDGAnnouncementModel *model = [wself getAnnModelFromDict:dict];
            [resultArr addObject:model];
        }
        if (completion) completion(resultArr);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (completion) completion(nil);
    }];
}

#warning Город везде стоит 326
- (void)getAdsById:(NSString *)categoryId page:(NSUInteger)pageNumber completion:(void(^)(NSArray *resultArr))completion {
    NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=getAdsByCategoryId&category_id=%@&city_id=326&page_num=%d&page_size=30", categoryId, pageNumber];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    __weak typeof(self) wself = self;
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if ([result isKindOfClass:[NSDictionary class]] && result != nil) {
            NSArray *items = [result safeObjectForKey:@"items"];
            NSMutableArray *resultArr = [[NSMutableArray alloc] initWithCapacity:items.count];
            for (NSDictionary *dict in items) {
                SDGAnnouncementModel *model = [wself getAnnModelFromDict:dict];
                [resultArr addObject:model];
            }
            if (completion) completion(resultArr);
        }
        else {
            if (completion) completion(nil);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (completion) completion(nil);
    }];
}

- (void)getAdsByCategoryId:(NSString *)categoryId andFilter:(NSDictionary *)filterDict page:(NSUInteger)pageNumber completion:(void(^)(NSArray *resultArr))completion {
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"http://birge.ru/web-service/?service=getAdsByCategoryId&category_id=%@&city_id=326&page_num=%d&page_size=30",
                        categoryId, pageNumber];
    NSArray *allKeys = [filterDict allKeys];
    for (NSString *key in allKeys) {
        [urlStr appendFormat:@"&%@=%@", key, filterDict[key]];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    __weak typeof(self) wself = self;
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if ([result isKindOfClass:[NSDictionary class]] && result != nil) {
            NSArray *items = [result safeObjectForKey:@"items"];
            NSMutableArray *resultArr = [[NSMutableArray alloc] initWithCapacity:items.count];
            for (NSDictionary *dict in items) {
                SDGAnnouncementModel *model = [wself getAnnModelFromDict:dict];
                [resultArr addObject:model];
            }
            if (completion) completion(resultArr);
        }
        else {
            if (completion) completion(nil);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (completion) completion(nil);
    }];
}

- (void)getAdDetailsById:(NSString *)adId completion:(void(^)(SDGAnnDetailsModel *detailsModel))completion {
    NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=getAdById&ad_id=%@", adId];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    __weak typeof(self) wself = self;
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if ([result isKindOfClass:[NSArray class]] && [result count] > 0) {
            result = [(NSArray *)result objectAtIndex:0];
        }
        if ([result isKindOfClass:[NSDictionary class]] && result != nil) {
            NSMutableDictionary *properties = [[NSMutableDictionary alloc] init];
            if ([result objectForKey:@"image"]) {
                [properties setObject:[result safeObjectForKey:@"image"] forKey:@"images"];
            }
            [properties addEntriesFromDictionary:[result safeObjectForKey:@"properties"]];
            SDGAnnDetailsModel *details = [wself getAnnDetailsFromDict:properties];
            if (details) {
                details.fullDescription = [[result safeObjectForKey:@"description"] xmlSimpleUnescapeString];
                details.cityName = [result safeObjectForKey:@"city_name"];
                details.metroName = [result safeObjectForKey:@"metro_name"];
            }
            if (completion) completion(details);
        }
        else {
            if (completion) completion(nil);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (completion) completion(nil);
    }];
}

- (SDGAnnDetailsModel *)getAnnDetailsFromDict:(NSDictionary *)dict {
    SDGAnnDetailsModel *details = nil;
    if (dict != nil) {
        details = [SDGAnnDetailsModel new];
        details.imgUrls = [dict safeObjectForKey:@"images"];
        
        details.userName = [dict safeObjectForKey:@"NAME"];
        details.userPhone = [dict safeObjectForKey:@"PHONE"];
        details.userMail = [dict safeObjectForKey:@"EMAIL"];
        details.userId = [dict safeObjectForKey:@"USER_ID"];
        details.cityId = [dict safeObjectForKey:@"CITY_ID"];
        
        details.annTimeout = [dict safeObjectForKey:@"TIME"];
        
        details.priceRub = [dict safeObjectForKey:@"PRICE_RUB"];
        
        details.purpose = [dict safeObjectForKey:@"NAZNACHENIE"];
        details.square = [dict safeObjectForKey:@"SQUARE"];
        details.square_room = [dict safeObjectForKey:@"PLOSHAD_KOMNATY"];
        details.mest_v_komnate = [dict safeObjectForKey:@"MEST_V_KOMNATE"];
        details.room_count = [dict safeObjectForKey:@"KOMNA"];
        details.bedroom_count = [dict safeObjectForKey:@"BEDROOM_COUNT"];
        details.floors = [dict safeObjectForKey:@"ETAZH"];
        details.lease = [dict safeObjectForKey:@"SROK_ARENDY"];
        details.condition = [dict safeObjectForKey:@"SOSTOYANIE"];
        details.metro = [dict safeObjectForKey:@"METRO"];
        
        details.taxi_type = [dict safeObjectForKey:@"TAXI"];
        details.transportation_type = [dict safeObjectForKey:@"TIP_PEREVOZKI"];
        details.owner = [dict safeObjectForKey:@"KTO_OKAZYVAET"];
        details.taxi_condition = [dict safeObjectForKey:@"SOSTOYANIE_TS"];
        
        details.job_type = [dict safeObjectForKey:@"TIP_RABOTY"];
        details.oplata = [dict safeObjectForKey:@"OPLATA"];
        details.specialization = [dict safeObjectForKey:@"SPECIALNOST"];
        details.education_level = [dict safeObjectForKey:@"OBRAZOVANIE"];
        
        details.car_brand = [dict safeObjectForKey:@"MARKA"];
        details.kuzov = [dict safeObjectForKey:@"KUZOV"];
        details.probeg = [dict safeObjectForKey:@"PROBEG"];
        details.year = [dict safeObjectForKey:@"GOD"];
        details.kpp = [dict safeObjectForKey:@"KPP"];
        details.rul = [dict safeObjectForKey:@"RUL"];
        details.privod = [dict safeObjectForKey:@"PRIVOD"];
        details.engine = [dict safeObjectForKey:@"DVIGATEL"];
        details.engine_capacity = [dict safeObjectForKey:@"OBEM"];
        details.engine_power = [dict safeObjectForKey:@"MOSCH"];
        details.car_condition = [dict safeObjectForKey:@"SOST"];
        details.pts = [dict safeObjectForKey:@"ORIGINAL"];
    }
    return details;
}

- (SDGAnnouncementModel *)getAnnModelFromDict:(NSDictionary *)dict {
    SDGAnnouncementModel *model = nil;
    if (dict != nil) {
        model = [SDGAnnouncementModel new];
        model.annId = [dict safeObjectForKey:@"id"];
        model.categoryId = [dict safeObjectForKey:@"category_id"];
        model.name = [dict safeObjectForKey:@"name"];
        model.price = [dict safeObjectForKey:@"price"];
        model.imageUrl = [dict safeObjectForKey:@"image"];
        model.metroName = [dict safeObjectForKey:@"metro_name"];
        model.date = [dict safeObjectForKey:@"date"];
        if ([dict safeObjectForKey:@"properties"]) {
            NSDictionary *propDict = [dict safeObjectForKey:@"properties"];
            if ([propDict safeObjectForKey:@"SELECTED"]) {
                model.annType = SELECTED;
            }
            if ([propDict safeObjectForKey:@"STATUS"]) {
                
            }
            model.ownerId = [propDict safeObjectForKey:@"USER_ID"];
        }
    }
    return model;
}

/*
 hash = aeb867f7fcb6c20a75999272538bd547;
 "user_id" = 84340;
 */
#warning На сайте m.birge есть выход. В сервисах нет логаута.
#warning Надо разобраться как должно работать избранное. На сайте m.birge избранное появляется только после авторизации. Значит оно должно быть привязано к конкретному пользователю и соответсвенно должен быть сервис для получения/добавления/удаления избранного
- (void)signInWithLogin:(NSString *)login pass:(NSString *)pass completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=checkUser&login=%@&password=%@", login, pass];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if (completion) completion(result);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (failure) failure(error);
    }];
}

#warning косяк с тем что сервак ожидает в качестве token значение tokentoken
#warning вообще ожидание подтверждения по почте через приложение это полная хрень. Лушче пускать в приложение сразу и через определенный промежуток времени на все запросы отвчеать ошибкой - подтвердите регистрацию
- (void)registerWithLogin:(NSString *)login pass:(NSString *)pass completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    NSString *urlStr = @"http://birge.ru/web-service/?service=registration";
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:ud_DeviceTokenKey];
    token = @"tokentoken";
    if (token != nil && [token length] > 0) urlStr = [urlStr stringByAppendingFormat:@"&token=%@", token];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"login=%@&password=%@&confirm_password=%@", login, pass, pass];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    [self loadRequest:request completion:^(id result) {
        NSLog(@"%@", result);
        if (completion) completion(result);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (failure) failure(error);
    }];
}

- (void)restorePassWithLogin:(NSString *)login completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:ud_DeviceTokenKey];
    token = @"tokentoken";
    NSString *urlStr = [NSString stringWithFormat:@"http://birge.ru/web-service/?service=getRestorePassword&token=%@&email=%@", token, login];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if (completion) completion(result);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (failure) failure(error);
    }];
}

- (void)loadRequest:(NSURLRequest *)request
         completion:(void(^)(id result))completion
            failure:(void(^)(NSError *error))failure {
    
    if (_curStatus != AFNetworkReachabilityStatusNotReachable) {
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        operation.responseSerializer.acceptableContentTypes = [operation.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (completion) {
                completion(responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
        
        [operation start];
    }
    else {
        if (failure) {
            NSError *err = [NSError errorWithDomain:@"" code:-1004 userInfo:nil];
            failure(err);
        }
    }
}

- (void)connectionEstablished {
    
}

- (void)connectionLost {
    
}

@end
