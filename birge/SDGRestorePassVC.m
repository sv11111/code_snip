//
//  SDGRestorePassVC.m
//  Birge
//
//  Created by Семен Власов on 31.08.15.
//  Copyright (c) 2015 SVV. All rights reserved.
//

#import "SDGRestorePassVC.h"
#import "MBProgressHUD.h"
#import "SDGUtils.h"
#import "APIManager.h"

@interface SDGRestorePassVC () <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *loginTF;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation SDGRestorePassVC {
    UITapGestureRecognizer *_tapGesture;
    UITextField *_activeTextField;
    CGFloat _curKeyboardHeight;
    CGFloat _initialTopOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognize:)];
    _tapGesture.enabled = NO;
    [self.view addGestureRecognizer:_tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _initialTopOffset = _topConstraint.constant;
}

- (void)tapGestureRecognize:(UITapGestureRecognizer *)recognizer {
    if (_loginTF.editing) [_loginTF resignFirstResponder];
}

- (IBAction)restoreButtTouchUp:(id)sender {
    [self tryRestore];
}

- (void)tryRestore {
    if (_loginTF.text.length > 0) {
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.dimBackground = YES;
        [self.navigationController.visibleViewController.view addSubview:HUD];
        [HUD show:YES];
        
        [[APIManager sharedInstance] restorePassWithLogin:_loginTF.text completion:^(id result) {
            [HUD hide:YES];
            
            if ([result isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict = result;
                if ([dict[@"status"] isEqualToString:@"success"]) {
                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:@"Для восстановления пароля, Вам на почту, отправлена ссылка, по которой необходимо пройти" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    av.tag = 1;
                    [av show];
                }
                else {
                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:dict[@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [av show];
                }
            }
        } failure:^(NSError *error) {
            [HUD hide:YES];
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не удалось восстановить пароль. Попробуйте еще раз." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
        }];
    }
    else {
        [SDGUtils borderColorChangeAnimation:@[_loginTF] toColor:nil];
    }
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UITextFieldDelegate and keyboard handlers

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _tapGesture.enabled = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _activeTextField = textField;
    
    if (_curKeyboardHeight > 0) {
        [self animateTextFieldOffset];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _loginTF) {
        [_loginTF resignFirstResponder];
        [self tryRestore];
    }
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notif {
    CGSize kbSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _curKeyboardHeight = kbSize.height;
    if (_activeTextField) {
        [self animateTextFieldOffset];
    }
}

- (void)keyboardWillHide:(NSNotification *)notif {
    _curKeyboardHeight = 0;
    
    if (_activeTextField && _topConstraint.constant != _initialTopOffset) {
        _topConstraint.constant = _initialTopOffset;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)animateTextFieldOffset {
    if (_activeTextField) {
        CGFloat offset = (_activeTextField.frame.origin.y + _activeTextField.frame.size.height + 10) - (self.view.frame.size.height - _curKeyboardHeight);
        if (offset > 0) {
            _topConstraint.constant = _topConstraint.constant - offset;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
