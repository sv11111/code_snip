//
//  BWDRecordedPeriodsCache.h
//  camDrive
//
//  Created by adm on 26.02.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BWDRecordedPeriodsCacheItem;

@interface BWDRecordedPeriodsCache : NSObject

+ (id)sharedManager;


- (BOOL)existsDataForChannelId:(NSString *)channelId date:(NSDate *)date direction:(int)direction;
- (BWDRecordedPeriodsCacheItem *)getDataForChannelId:(NSString *)channelId date:(NSDate *)date direction:(int)direction;
- (void)loadDataForChannelId:(NSString *)channelId date:(NSDate *)date direction:(int)direction completion:(void(^)(BWDRecordedPeriodsCacheItem *result))completion;
- (void)clearCache;

@end
