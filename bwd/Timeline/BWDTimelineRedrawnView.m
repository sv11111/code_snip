//
//  BWDTimelineRedrawnView.m
//  camDrive
//
//  Created by adm on 26.01.16.
//  Copyright © 2016 adm. All rights reserved.
//


#import "BWDTimelineRedrawnView.h"


@implementation BWDTimelineRedrawnView  {

}

- (void)setVisibleFrame:(CGRect)visibleFrame {
    _visibleFrame = visibleFrame;
    if (CGRectIsEmpty(_drawnRect) ||
        ( (_visibleFrame.origin.x <= (_drawnRect.origin.x + _drawnRect.size.width / 6 ))
         && (_drawnRect.origin.x > 0) ) ||
        (((_visibleFrame.origin.x + _visibleFrame.size.width) >= (_drawnRect.origin.x + _drawnRect.size.width / 6 * 5))
         && (_drawnRect.origin.x + _drawnRect.size.width < self.frame.size.width) ) ) {
            [self setNeedsDisplay];
        }
}

- (id)init {
    self = [super init];
    if (self) {
        self.clearsContextBeforeDrawing = NO;
        _drawnRect = CGRectZero;
        
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    if (CGRectEqualToRect(self.frame, frame)) {
        return;
    }
    [super setFrame:frame];
    if (CGRectIsEmpty(frame)) {
        return;
    }
    _drawnRect = CGRectZero;
    self.clearsContextBeforeDrawing = YES;
    [self.layer setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    
}

- (void)calculateDrawnRect {
    CGRect rect = self.layer.bounds;
    
    float drawnRectX = 0.0;
    float drawnRectWidth = 0.0;
    
    drawnRectX = MAX(_visibleFrame.origin.x - _visibleFrame.size.width, 0);
    drawnRectWidth = drawnRectX + _visibleFrame.size.width * 3 > rect.size.width ? rect.size.width - drawnRectX : _visibleFrame.size.width * 3;
    _drawnRect = CGRectMake(drawnRectX, 0, drawnRectWidth, _visibleFrame.size.height);
}


- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    [self calculateDrawnRect];
}


@end


