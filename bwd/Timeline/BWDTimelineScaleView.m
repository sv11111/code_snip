//
//  BWDTimelineScaleView.m
//  testTimeline
//
//  Created by adm on 23.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import "BWDTimelineScaleView.h"
#import "BWDTimelineItem.h"
#import "BWDTimeline.h"
#import "BWDUtils.h"

@implementation BWDTimelineScaleView  {
    NSArray *_scaleLevels;
}


static float const BWDTimelineScaleViewMaxGapBetweenLabels = 100;
static float const BWDTimelineScalePointerHeight = 4;
static float const BWDTimelineScalePointerWidth = 2;


- (id)init {
    self = [super init];
    if (self) {
        _scaleLevels = @[@2, @4, @8, @12, @24, @48, @96, @288, @720, @1440];
        self.opaque = NO;
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    return self;
}


- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    [super drawLayer:layer inContext:ctx];
    CGContextRef context = ctx;
    CGRect rect = layer.bounds;
    
    UIGraphicsPushContext(ctx);
    if (self.clearsContextBeforeDrawing) {
        self.clearsContextBeforeDrawing = NO;
        
    }
    CGContextClearRect(ctx, self.drawnRect);
    
    float drawXOffset = BWDTimelineHorizontalGaps;
    float drawWidth = rect.size.width - BWDTimelineHorizontalGaps * 2;
    float pointersCountEstimate = drawWidth / BWDTimelineScaleViewMaxGapBetweenLabels;
    
    float secondsPerPoint = 60 * 60 * 24 / drawWidth;
    int scaleLevel;
    for (scaleLevel = 0; scaleLevel < [_scaleLevels count]; scaleLevel++) {
        if (pointersCountEstimate <= ((NSNumber *)_scaleLevels[scaleLevel]).intValue) {
            break;
        }
    }
    int pointersCount = ((NSNumber *)_scaleLevels[(scaleLevel == [_scaleLevels count]) ? scaleLevel - 1 : scaleLevel]).intValue;
    float gapBetweenPointers = drawWidth / pointersCount;
    int pointersOffset = (int) self.drawnRect.origin.x / gapBetweenPointers;
    UIFont* font = [UIFont systemFontOfSize:10];
    UIColor* textColor = [UIColor whiteColor];
    NSDictionary* stringAttrs = @{ NSFontAttributeName : font, NSForegroundColorAttributeName : textColor };
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextFillRect(context, CGRectMake(self.drawnRect.origin.x, 0, self.drawnRect.size.width, 2));
    
    for (int i = pointersOffset; i <= pointersCount; i++) {
        CGContextSetRGBFillColor(context, 1, 1, 1, 1);
        
         int time = (int)(gapBetweenPointers * i * secondsPerPoint);
        time = ((time % 60) > 30) ? time + (60 - (time % 60)) : time - (time % 60);
        float roundedOffset = drawXOffset + time / secondsPerPoint;
        
        CGRect pointerRect = CGRectMake(/*drawXOffset + gapBetweenPointers * i*/ roundedOffset- (BWDTimelineScalePointerWidth) / 2,
                                        BWDTimelineItemScaleHeight - BWDTimelineScalePointerHeight,
                                        BWDTimelineScalePointerWidth,
                                        BWDTimelineScalePointerHeight);
        if (CGRectGetMaxX(self.drawnRect) < pointerRect.origin.x) {
            break;
        }
        CGContextFillRect(context, pointerRect);
        
        NSDate *date = [NSDate dateWithTimeInterval:time sinceDate:[BWDUtils dateOnlyFromDate:[NSDate date]]];
        NSAttributedString* attrStr = [[NSAttributedString alloc] initWithString:[formatter stringFromDate:date] attributes:stringAttrs];
        [attrStr drawAtPoint:CGPointMake(/*drawXOffset + gapBetweenPointers * i*/ roundedOffset - attrStr.size.width / 2,
                                         BWDTimelineItemScaleHeight - BWDTimelineScalePointerHeight - 5 - attrStr.size.height)];
        
        
    }
    UIGraphicsPopContext();
    
}


@end







