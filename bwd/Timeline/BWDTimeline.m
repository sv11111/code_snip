//
//  BWDTimeline.m
//  camDrive
//
//  Created by adm on 17.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import "BWDTimeline.h"
#import "BWDTimelineItem.h"
#import "BWDUtils.h"
#import "NSDate+Utilities.h"


@interface BWDTimeline () <BWDTimeLineItemDelegate>

@property (nonatomic, strong) UIScrollView *dateScrollView;

@property (nonatomic, strong) BWDTimelineItem *prevTimeline;
@property (nonatomic, strong) BWDTimelineItem *curTimeline;
@property (nonatomic, strong) BWDTimelineItem *nextTimeline;

@end

static float        const BWDTimelineMinZoomScale = 1;
static float        const BWDTimelineMaxZoomScale = 200;

@implementation BWDTimeline {
    
    NSMutableArray *_smoothDeltaArr;
    float _absScale;
    float _initWidth;
    
    float _curMinScale;
    float _curMaxScale;
    
    UITapGestureRecognizer *_singleTap;
    UITapGestureRecognizer *_doubleTap;
    
    BOOL _isNeedPageChange;
    
    BOOL _isNeedUpdateHiddenTimelineItems;
    
    
    BOOL _showPrevDay;
    BOOL _showNextDay;
}

#pragma mark- Initialization

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _curMinScale = BWDTimelineMinZoomScale;
        _curMaxScale = BWDTimelineMaxZoomScale;
        
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
        
        _absScale = 1;
        
        _dateScrollView = [[UIScrollView alloc] init];
        [self addSubview:_dateScrollView];
        _prevTimeline = [[BWDTimelineItem alloc] init];
        _curTimeline = [[BWDTimelineItem alloc] init];
        _nextTimeline = [[BWDTimelineItem alloc] init];
        
        _prevTimeline.offsetAlign = BWDTimelineItemOffsetAlignRight;
        
        for (UIScrollView *scr in @[_dateScrollView, _prevTimeline, _curTimeline, _nextTimeline]) {
            scr.minimumZoomScale = (scr == _dateScrollView) ? 1 : _curMinScale;
            scr.maximumZoomScale = (scr == _dateScrollView) ? 1 : _curMaxScale;
            scr.showsHorizontalScrollIndicator = NO;
            scr.showsVerticalScrollIndicator = NO;
            scr.bouncesZoom = NO;
            scr.bounces = (scr == _dateScrollView);
            scr.delegate = self;
        }
        
        _dateScrollView.pagingEnabled = YES;
        [_dateScrollView addSubview:_prevTimeline];
        [_dateScrollView addSubview:_curTimeline];
        [_dateScrollView addSubview:_nextTimeline];
        
        [self addTapRecognizers];
    }
    return self;
}

#pragma mark - Setters

- (void)setShowDate:(NSDate *)showDate {
    if (_showDate != showDate && showDate != nil) {
        _showDate = [BWDUtils dateOnlyFromDate:showDate];
        if (_minDate && _showDate && [_showDate timeIntervalSinceDate:_minDate] < 0) {
            _showDate = _minDate;
        }
        if (_maxDate && _showDate && [_showDate timeIntervalSinceDate:_maxDate] > 0) {
            _showDate = _maxDate;
        }
        [self updateTimelineItemsVisibility];
        [self reloadData];
        if (_delegate && [_delegate respondsToSelector:@selector(timeline:didScrollDay:)]) {
            [_delegate timeline:self didScrollDay:_showDate];
        }
    }
}

- (void)setMinDate:(NSDate *)minDate {
    if (minDate != _minDate) {
        _minDate = [BWDUtils dateOnlyFromDate:minDate];
        if (_minDate && _maxDate && [_maxDate timeIntervalSinceDate:_minDate] < 0) {
            self.maxDate = _minDate;
        }
        if (_minDate && _showDate && [_showDate timeIntervalSinceDate:_minDate] < 0) {
            self.showDate = _minDate;
        }
        [self updateTimelineItemsVisibility];
    }
}

- (void)setMaxDate:(NSDate *)maxDate {
    if (maxDate != _maxDate) {
        _maxDate = [BWDUtils dateOnlyFromDate:maxDate];
        if (_maxDate && _minDate && [_minDate timeIntervalSinceDate:_maxDate] > 0) {
            self.minDate = _maxDate;
        }
        if (_maxDate && _showDate && [_showDate timeIntervalSinceDate:_maxDate] > 0) {
            self.showDate = _maxDate;
        }
        [self updateTimelineItemsVisibility];
    }
}

- (void)setDelegate:(id<BWDTimelineDelegate>)delegate {
    _delegate = delegate;
}

- (void)setCurrentPointerDate:(NSDate *)currentPointerDate {
    if(![currentPointerDate isEqualToDate:_currentPointerDate]) {
        _currentPointerDate = [currentPointerDate copy];
        for (BWDTimelineItem *item in @[_prevTimeline, _curTimeline, _nextTimeline]) {
            [item updatePointVisibilityWithDate:_currentPointerDate];
        }
    }
}


- (void)setBackgroundColor:(UIColor *)backgroundColor {
    if (_backgroundColor != backgroundColor) {
        _backgroundColor = backgroundColor;
        _prevTimeline.backgroundColor = backgroundColor;
        _curTimeline.backgroundColor = backgroundColor;
        _nextTimeline.backgroundColor = backgroundColor;
    }
    
}


- (void)setScaleFont:(UIFont *)scaleFont {
    if (_scaleFont != scaleFont) {
        _scaleFont = scaleFont;
        _prevTimeline.scaleFont = scaleFont;
        _curTimeline.scaleFont = scaleFont;
        _nextTimeline.scaleFont = scaleFont;
    }
}


- (void)setContentColor:(UIColor *)contentColor {
    if (_contentColor != contentColor) {
        _contentColor = contentColor;
        _prevTimeline.contentColor = contentColor;
        _curTimeline.contentColor = contentColor;
        _nextTimeline.contentColor = contentColor;
    }
}


- (void)setScaleColor:(UIColor *)scaleColor {
    if (_scaleColor != scaleColor) {
        _scaleColor = scaleColor;
        _prevTimeline.scaleColor = scaleColor;
        _curTimeline.scaleColor = scaleColor;
        _nextTimeline.scaleColor = scaleColor;
    }
}



#pragma mark - External interface

- (float)zoomIn {
    if (_curTimeline.zoomScale >= _curMaxScale) {
        return _curMaxScale;
    }
    float scale = MAX(MIN(_curTimeline.zoomScale * 2, _curMaxScale), _curMinScale);
    [_curTimeline zoomToPoint:CGPointMake(_curTimeline.contentOffset.x + _curTimeline.frame.size.width / 2, _curTimeline.frame.size.height / 2)  withScale:scale  animated:YES];
    return scale;
}


- (float)zoomOut {
    if (_curTimeline.zoomScale <= _curMinScale) {
        return _curMinScale;
    }
    float scale = MAX(MIN(_curTimeline.zoomScale / 2, _curMaxScale), _curMinScale);
    [_curTimeline zoomToPoint:CGPointMake(_curTimeline.contentOffset.x + _curTimeline.frame.size.width / 2, _curTimeline.frame.size.height / 2)  withScale:scale  animated:YES];
    return scale;
}


- (float)zoomToScale:(float)scale {
    float trueScale = MAX(MIN(scale, _curMaxScale), _curMinScale);
    [_curTimeline zoomToPoint:CGPointMake(_curTimeline.contentOffset.x + _curTimeline.frame.size.width / 2, _curTimeline.frame.size.height / 2)  withScale:trueScale  animated:YES];
    return trueScale;
}


- (void)reloadData {
    [self getDataForTimelineItem:_curTimeline];
    if (_showPrevDay) {
        [self getDataForTimelineItem:_prevTimeline];
    }
    if (_showNextDay) {
        [self getDataForTimelineItem:_nextTimeline];
    }
}


#pragma mark - Set visual properties

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (_initWidth == 0) {
        _initWidth = self.frame.size.width;
    }
    else {
        //calculate scale bounds when timeline width has changed
        if (self.frame.size.width != _dateScrollView.frame.size.width) {
            _curMaxScale = BWDTimelineMaxZoomScale * self.frame.size.width / _initWidth;
            _curMinScale = BWDTimelineMinZoomScale * self.frame.size.width / _initWidth;
            for (UIScrollView *scr in @[_prevTimeline, _curTimeline, _nextTimeline]) {
                scr.minimumZoomScale = _curMinScale;
                scr.maximumZoomScale = _curMaxScale;
                if (scr.zoomScale < _curMinScale) {
                    scr.zoomScale = _curMinScale;
                }
                if (scr.zoomScale > _curMaxScale) {
                    scr.zoomScale = _curMaxScale;
                }
            }
        }
    }
    [self updateTimelineItemsVisibility];
    
    
}

#pragma mark - scroll view delegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView != _dateScrollView) {
        return;
    }
    _isNeedPageChange = [self isScrollNext:targetContentOffset->x] || [self isScrollPrev:targetContentOffset->x];
    //NSLog(@"1 needChange:%@", _isNeedPageChange ? @"YES" : @"NO");
    //NSLog(@"offset:%f   size:%f", targetContentOffset->x, _dateScrollView.contentSize.width);
}
 

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView != _dateScrollView) {
        return;
    }
    //NSLog(@"2 needChange:%@", _isNeedPageChange ? @"YES" : @"NO");
    if ( !_dataSource || !_isNeedPageChange) {
        //NSLog(@"3 needChange:%@", _isNeedPageChange ? @"YES" : @"NO");
        return;
    }
    _isNeedPageChange = NO;
    [self changeDatePage];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView != _dateScrollView) {
        return;
    }
    //NSLog(@"4 needChange:%@", _isNeedPageChange ? @"YES" : @"NO");
    if ( !_dataSource || !_isNeedPageChange) {
        //NSLog(@"5 needChange:%@", _isNeedPageChange ? @"YES" : @"NO");
        [self layoutDateScrollView];
        return;
    }
    _isNeedPageChange = NO;
    [self changeDatePage];
}

- (void)changeDatePage {
    [_curTimeline removeGestureRecognizer:_singleTap];
    [_curTimeline removeGestureRecognizer:_doubleTap];
    
    if ([self isScrollPrev:_dateScrollView.contentOffset.x]) {
        _showDate = [BWDUtils yesterdayFromDate:_showDate];
        
        BWDTimelineItem *t = _nextTimeline;
        _nextTimeline = _curTimeline;
        _curTimeline = _prevTimeline;
        _prevTimeline = t;
        t = nil;
        [self getDataForTimelineItem:_prevTimeline];
    } else {
        _showDate = [BWDUtils tomorrowFromDate:_showDate];
        BWDTimelineItem *t = _prevTimeline;
        _prevTimeline = _curTimeline;
        _curTimeline = _nextTimeline;
        _nextTimeline = t;
        t = nil;
        [self getDataForTimelineItem:_nextTimeline];
    }

    [self addTapRecognizers];
    
    _nextTimeline.offsetAlign = BWDTimelineItemOffsetAlignLeft;
    _prevTimeline.offsetAlign = BWDTimelineItemOffsetAlignRight;
    [self updateTimelineItemsVisibility];
    
    for (BWDTimelineItem *item in @[_prevTimeline, _curTimeline, _nextTimeline]) {
        [item updatePointVisibilityWithDate:_currentPointerDate];
    }
    if (_delegate && [_delegate respondsToSelector:@selector(timeline:didChangeDate:)]) {
        [_delegate timeline:self didChangeDate:[_showDate copy]];
    }

}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _dateScrollView) {
        if (_isNeedUpdateHiddenTimelineItems) {
            _isNeedUpdateHiddenTimelineItems = NO;
            if (_dateScrollView.contentOffset.x < _dateScrollView.frame.size.width) {
                [_prevTimeline setNeedsDisplay];
            }
            else {
                [_nextTimeline setNeedsDisplay];
            }
                
        }
        //NSLog(@"frame:%f    content:%f", _curTimeline.frame.size.width, _curTimeline.contentSize.width);
        NSDate *senderDate = [_showDate copy];
        if ([self isScrollPrev:_dateScrollView.contentOffset.x]) {
            senderDate = [BWDUtils yesterdayFromDate:senderDate];
        }
        if ([self isScrollNext:_dateScrollView.contentOffset.x]) {
            senderDate = [BWDUtils tomorrowFromDate:senderDate];
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(timeline:didScrollDate:)]) {
            [_delegate timeline:self didScrollDate:senderDate];
        }
    } else {
        
        if (_delegate && [_delegate respondsToSelector:@selector(timeline:didScrollDay:)]) {
            [_delegate timeline:self didScrollDay:_showDate];
        }
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return ([scrollView isKindOfClass:[BWDTimelineItem class]]) ? ((BWDTimelineItem *)scrollView).zoomView : nil;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    if (scrollView == _curTimeline) {
        _absScale = scale;
        _isNeedUpdateHiddenTimelineItems = YES;
        [_prevTimeline resizeContentWithScale:_absScale];
        [_nextTimeline resizeContentWithScale:_absScale];
    }
} 

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    if (_delegate && [_delegate respondsToSelector:@selector(timelineDidZoom:)]) {
        
        [_delegate timelineDidZoom:self];
    }
}


#pragma mark - tap recognizers

- (void)goToArchiveTime:(UITapGestureRecognizer *)recognizer {
    NSDate *date = [_curTimeline dateNearestToPointX:[recognizer locationInView:_curTimeline].x];
    if (date) {
        self.currentPointerDate = date;
        if (_delegate && [_delegate respondsToSelector:@selector(timeline:didSetArchiveDate:)]) {
            [_delegate timeline:self didSetArchiveDate:date];
        }
    }
}

- (void)zoomInByTap:(UITapGestureRecognizer *)recognizer {
    float scale =  MAX(MIN( (_absScale == _curMaxScale) ? _curTimeline.zoomScale / 2 : _curTimeline.zoomScale * 2, _curMaxScale), _curMinScale);
    [_curTimeline zoomToPoint:[recognizer locationInView:_curTimeline]  withScale:scale  animated:YES];

}

#pragma mark - timelineItem Delegate

- (void)timelineItem:(BWDTimelineItem *)timeline ItemDidSetPointerDate:(NSDate *)pointerDate {
    _currentPointerDate = pointerDate;
    if (_delegate && [_delegate respondsToSelector:@selector(timeline:didSetArchiveDate:)]) {
        [_delegate timeline:self didSetArchiveDate:[pointerDate copy]];
    }
}

- (void)timelineItem:(BWDTimelineItem *)timeline ItemDidScrollPointerDate:(NSDate *)pointerDate {
    _currentPointerDate = pointerDate;
    if (_delegate && [_delegate respondsToSelector:@selector(timeline:didScrollArchiveDate:)]) {
        [_delegate timeline:self didScrollArchiveDate:[pointerDate copy]];
    }
}


#pragma mark - Timeline Items Positioning

- (void)updateTimelineItemsVisibility {
    _showPrevDay = [_showDate timeIntervalSinceDate:_minDate] > 0 || (_minDate == nil);
    _showNextDay = [_showDate timeIntervalSinceDate:_maxDate] < 0 || (_maxDate == nil);
    
    [self layoutDateScrollView];
    
    _prevTimeline.frame = CGRectMake(0,
                                     0,
                                     (_showPrevDay) ? _dateScrollView.frame.size.width : _dateScrollView.frame.size.width,//0,
                                     _dateScrollView.frame.size.height);
    _curTimeline.frame = CGRectMake((_showPrevDay) ? _dateScrollView.frame.size.width : 0,
                                    0,
                                    _dateScrollView.frame.size.width,
                                    _dateScrollView.frame.size.height);
    _nextTimeline.frame = CGRectMake((_showPrevDay) ? _dateScrollView.frame.size.width * 2 : _dateScrollView.frame.size.width,
                                     0,
                                     (_showNextDay) ? _dateScrollView.frame.size.width : _dateScrollView.frame.size.width,//0,
                                     _dateScrollView.frame.size.height);
    _prevTimeline.hidden = !_showPrevDay;
    _nextTimeline.hidden = !_showNextDay;
    
    [_prevTimeline resizeContentWithScale:_absScale];
    [_nextTimeline resizeContentWithScale:_absScale];
    for (BWDTimelineItem *item in @[_prevTimeline, _curTimeline, _nextTimeline]) {
        [item updatePointVisibilityWithDate:_currentPointerDate];
    }
    
}

- (void)layoutDateScrollView {
    int multiplyer = 1 + (_showPrevDay ? 1 : 0) + (_showNextDay ? 1 : 0);
    _dateScrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    _dateScrollView.contentSize = CGSizeMake(_dateScrollView.frame.size.width * multiplyer, _dateScrollView.frame.size.height);
    _dateScrollView.contentOffset = CGPointMake((_showPrevDay) ? _dateScrollView.frame.size.width : 0, 0);
}

- (BOOL)isScrollPrev:(float)targetOffsetX {
    if (!_showPrevDay) {
        return NO;
    }
    int divider = (_showNextDay) ? 6 : 4;
    return  (targetOffsetX < (_dateScrollView.contentSize.width / divider));

}

- (BOOL)isScrollNext:(float)targetOffsetX {
    if (!_showNextDay) {
        return NO;
    }
    float mult = (_showPrevDay) ? (0.5) : (0.25);
    return (targetOffsetX > (_dateScrollView.contentSize.width * mult));
}

#pragma mark - Helpers


- (void)getDataForTimelineItem:(BWDTimelineItem *)item {
    item.dataState = BWDTimelineDataStateNoData;
    BWDTimelineItemPosition position = (item == _curTimeline) ? BWDTimelineItemPositionCurrDay : ((item == _prevTimeline) ? BWDTimelineItemPositionPrevDay : BWDTimelineItemPositionNextDay);
    if (_dataSource && [_dataSource respondsToSelector:@selector(timeline:dataForItemAtPosition:)]) {
        BWDRecordedPeriodsCacheItem *data = [_dataSource timeline:self dataForItemAtPosition:position];
        item.dataItem = data;
        
    }
}

- (void)addTapRecognizers {
    _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToArchiveTime:)];
    _singleTap.numberOfTapsRequired = 1;
    
    _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomInByTap:)];
    _doubleTap.numberOfTapsRequired = 2;
    [_singleTap requireGestureRecognizerToFail:_doubleTap];
    [_curTimeline addGestureRecognizer:_singleTap];
    [_curTimeline addGestureRecognizer:_doubleTap];
}

@end











