//
//  BWDTimeline.h
//  camDrive
//
//  Created by adm on 17.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BWDRecordedPeriodsCacheItem.h"

typedef enum {
    BWDTimelineItemPositionPrevDay,
    BWDTimelineItemPositionCurrDay,
    BWDTimelineItemPositionNextDay
} BWDTimelineItemPosition;


@class BWDTimeline;

@protocol BWDTimelineDelegate <NSObject>

@optional

//- (void)timeline:(BWDTimeline *)timeline didActionWithSender:(id)sender;
- (void)timelineDidZoom:(BWDTimeline *)timeline;
- (void)timeline:(BWDTimeline *)timeline didScrollDay:(NSDate *)day;
- (void)timeline:(BWDTimeline *)timeline didScrollDate:(NSDate *)date;
- (void)timeline:(BWDTimeline *)timeline didScrollArchiveDate:(NSDate *)date;
- (void)timeline:(BWDTimeline *)timeline didSetArchiveDate:(NSDate *)date;
- (void)timeline:(BWDTimeline *)timeline didChangeDate:(NSDate *)date;

@end



@protocol BWDTimeLineDataSource <NSObject>

@required
- (BWDRecordedPeriodsCacheItem *)timeline:(BWDTimeline *)timeline dataForItemAtPosition:(BWDTimelineItemPosition)position;

@optional

@end



@interface BWDTimeline : UIView

@property(nonatomic, strong) NSDate *showDate;

@property(nonatomic, strong) NSDate *minDate;
@property(nonatomic, strong) NSDate *maxDate;

@property(nonatomic, strong) NSDate *currentPointerDate;
@property(nonatomic, assign) BOOL alignPointerToRecordedPeriod;

@property(nonatomic, assign) BOOL restrictDayScrollBeforeLoading;


@property(nonatomic, strong) UIColor *backgroundColor;
@property(nonatomic, strong) UIColor *contentColor;
@property(nonatomic, strong) UIColor *scaleColor;
@property(nonatomic, strong) UIFont *scaleFont;


@property(nonatomic, weak) id<BWDTimelineDelegate> delegate;
@property(nonatomic, weak) id<BWDTimeLineDataSource> dataSource;

- (float)zoomIn;
- (float)zoomOut;

- (float)zoomToScale:(float)scale;

- (void)reloadData;




@end

