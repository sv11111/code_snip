//
//  BWDTimelineRedrawnView.h
//  camDrive
//
//  Created by adm on 26.01.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWDTimelineRedrawnView : UIView

@property(nonatomic, assign) CGRect visibleFrame;
@property(nonatomic, readonly) CGRect drawnRect;


@end
