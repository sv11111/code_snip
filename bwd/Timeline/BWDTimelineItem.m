//
//  BWDTimelineItem.m
//  testTimeline
//
//  Created by adm on 21.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import "BWDTimelineItem.h"
#import "BWDTimelineContentView.h"
#import "BWDTimeline.h"
#import "BWDTimelineScaleView.h"
#import "BWDUtils.h"



@interface BWDTimelineItem () <UIGestureRecognizerDelegate>

@property(nonatomic, strong) UIImageView *datePointer;
//@property(nonatomic, strong) UIView *bottomLine;
//@property(nonatomic, strong) UIView *topLine;

@property(nonatomic, strong, readonly) BWDTimelineContentView *contentView;
@property(nonatomic,strong, readonly) BWDTimelineScaleView *scaleView;

@property(nonatomic, strong, readonly) NSDate *itemDate;

@end


@implementation BWDTimelineItem {
    BOOL _isArchive;
    int _archivePeriodIndex;
    UIPanGestureRecognizer *_panRecognizer;
    NSDate *_pointerCurrentDate;
    id<BWDTimeLineItemDelegate> _delegate;
    
    float _lastWidth;
    
}

- (void)setContentOffset:(CGPoint)contentOffset {
    [super setContentOffset:contentOffset];
    _contentView.visibleFrame = _scaleView.visibleFrame = CGRectMake(contentOffset.x, contentOffset.y, self.frame.size.width, self.frame.size.height);
}


- (void)setDelegate:(id<BWDTimeLineItemDelegate>)delegate {
    if (_delegate != delegate) {
        [super setDelegate:delegate];
        _delegate = delegate;
    }
}

- (id<BWDTimeLineItemDelegate>)delegate {
    return _delegate;
}


//TODO: add activity indicator
- (void)setDataState:(BWDTimelineDataState)dataState {
    if (_dataState != dataState) {
        _dataState = dataState;
        //_topLine.backgroundColor = (_dataState == BWDTimelineDataStateLoading) ? [UIColor grayColor] : [UIColor whiteColor];
    }
}


- (void)setDataItem:(BWDRecordedPeriodsCacheItem *)dataItem {
    if (_dataItem != dataItem) {
        _dataItem = dataItem;
        self.dataState = _dataItem.state;
        [self refreshContentWithData:_dataItem.periods forDate:_dataItem.date];
    }
}


- (void)setBackgroundColor:(UIColor *)backgroundColor {
    if (_backgroundColor != backgroundColor) {
        _backgroundColor = backgroundColor;
        _contentView.backgroundColor = backgroundColor;
    }
    
}


- (void)setScaleFont:(UIFont *)scaleFont {
    if (_scaleFont != scaleFont) {
        
    }
}


- (void)setContentColor:(UIColor *)contentColor {
    if (_contentColor != contentColor) {
        _contentColor = _contentView.contentColor = contentColor;
        [_contentView setNeedsDisplay];
    }
}


- (void)setScaleColor:(UIColor *)scaleColor {
    if (_scaleColor != scaleColor) {
        
    }
}



- (id)init {
    self = [super init];
    if (self) {
        _dataState = BWDTimelineDataStateNoData;
        
        _zoomView = [[UIView alloc] init];
        [self addSubview:_zoomView];

        _datePointer = [[UIImageView alloc] init];
        _datePointer.image = [UIImage imageNamed:@"pointer"];
        _datePointer.hidden = YES;
        _offsetAlign = BWDTimelineItemOffsetAlignLeft;
        _contentView = [[BWDTimelineContentView alloc] init];
        _scaleView = [[BWDTimelineScaleView alloc] init];
        [self addSubview:_contentView];
        [self addSubview:_scaleView];

        [self addSubview:_datePointer];
        

        
        _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePointer:)];
        _panRecognizer.delegate = self;
        [_datePointer addGestureRecognizer:_panRecognizer];
        _datePointer.userInteractionEnabled = YES;
    }
    return self;
}


- (void)refreshContentWithData:(NSArray *)data forDate:(NSDate *)timelineDate {
    _contentView.recordedPeriods = [data copy];
    _itemDate = [timelineDate copy];
    [self updatePointVisibilityWithDate:_pointerCurrentDate];
    
}


- (void)updatePointVisibilityWithDate:(NSDate *)date {
    if (date && [[BWDUtils dateOnlyFromDate:date] isEqualToDate:[BWDUtils dateOnlyFromDate:_itemDate]]) {
        _datePointer.hidden = NO;
        _pointerCurrentDate = date;
        
        NSTimeInterval secondsOffset = ABS([[BWDUtils dateOnlyFromDate:date] timeIntervalSinceDate:date]);
        _isArchive = NO;
        int i = 0;
        for (BWDDatePeriod *period in _contentView.recordedPeriods) {
            if (secondsOffset >= period.startInterval && secondsOffset <= period.endInterval) {
                _isArchive = YES;
                _archivePeriodIndex = i;
                break;
            }
            i++;
        }
    }
    else
    {
        _datePointer.hidden = YES;
        _pointerCurrentDate = nil;
    }
    [self updateDatePointerFrame];
}


- (void)setItemDate:(NSDate *)itemDate {
    _itemDate = itemDate;

}


- (void)layoutSubviews {
    [super layoutSubviews];
    if (_lastWidth == self.contentSize.width && _lastWidth > 0) {
        return;
    }
    _lastWidth = self.contentSize.width;
    if (self.contentSize.width == 0) {
        _contentView.frame = CGRectMake(0,
                                        0,
                                        self.frame.size.width,
                                        self.frame.size.height);
        
        _scaleView.frame = CGRectMake(0, 0, self.frame.size.width, BWDTimelineItemScaleHeight);
        
        _datePointer.frame = CGRectMake(0, BWDTimelineItemScaleHeight, self.frame.size.height - BWDTimelineItemScaleHeight, self.frame.size.height - BWDTimelineItemScaleHeight);
        _datePointer.layer.cornerRadius = _datePointer.frame.size.height / 2;
        _datePointer.layer.masksToBounds = YES;
        self.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
        
        _zoomView.frame = self.bounds;

    }
    else {
        [self updateSubviewsFrame];
    }
}


- (void)resizeContentWithScale:(float)scale {
    if (scale != self.zoomScale) {
        self.zoomScale = scale;
    }
    float scrOffset = (_offsetAlign == BWDTimelineItemOffsetAlignLeft) ? 0 : self.contentSize.width - self.frame.size.width;
    self.contentOffset = CGPointMake(scrOffset, 0);
    self.contentSize = CGSizeMake( round(self.contentSize.width), self.contentSize.height);
}


- (void)updateSubviewsFrame {
    self.contentView.frame = CGRectMake(0,
                                        0,
                                        self.contentSize.width,
                                        self.frame.size.height);
    [self.contentView setNeedsDisplay];
    self.contentSize = CGSizeMake(self.contentSize.width, self.frame.size.height);
    self.contentOffset = CGPointMake(self.contentOffset.x, 0);
    self.scaleView.frame = CGRectMake( 0, 0, self.contentSize.width , BWDTimelineItemScaleHeight);
    
    [self updateDatePointerFrame];
    [_scaleView setNeedsDisplay];
    
    _zoomView.frame = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
}


- (void)updateDatePointerFrame {
    if (_pointerCurrentDate) {
        float gab = self.frame.size.height - BWDTimelineItemScaleHeight;
        float offsetX = [self xOffsetFromDate:_pointerCurrentDate] - gab / 2;
        self.datePointer.frame = CGRectMake( offsetX, BWDTimelineItemScaleHeight, gab, gab);
    }
}


- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)scale animated:(BOOL)animated
{
    float pointXinContentView = zoomPoint.x;
    float originalPointX = pointXinContentView / self.zoomScale;
    
    float newPointX = originalPointX * scale;
    float newOffsetX = MIN(MAX(self.contentOffset.x + newPointX - pointXinContentView, 0), self.contentSize.width / self.zoomScale * scale - self.frame.size.width);
 
    if (animated) {
        [UIView animateWithDuration:0.55f delay:0.0f usingSpringWithDamping:1.0f initialSpringVelocity:0.6f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            self.zoomScale = scale;
            [self updateSubviewsFrame];
            self.contentOffset = CGPointMake(newOffsetX, 0);
        } completion:^(BOOL completed) {
            if ([self.delegate respondsToSelector:@selector(scrollViewDidEndZooming:withView:atScale:)]) {
                [self.delegate scrollViewDidEndZooming:self withView:[self.delegate viewForZoomingInScrollView:self] atScale:scale];
            }
        }];
    }
    else {
        self.zoomScale = scale;
        [self updateSubviewsFrame];
        self.contentOffset = CGPointMake(newOffsetX, 0);
    }
}


- (void)zoomToArchivePointerWithScale:(CGFloat)scale animated:(BOOL)animated {
    if (_pointerCurrentDate) {
        [self zoomToPoint:_datePointer.center withScale:scale animated:animated];
    }
    else {
        [self zoomToPoint:self.center withScale:scale animated:animated];
    }
}


- (NSDate *)dateNearestToPointX:(float)x {
    NSDate *ret = nil;
    NSTimeInterval secondsOffset = [self secondsOffsetFromX:x];
    
    for (BWDDatePeriod *period in _contentView.recordedPeriods) {
        if (secondsOffset >= period.startInterval && secondsOffset <= period.endInterval) {
            ret = [NSDate dateWithTimeInterval:secondsOffset sinceDate:[BWDUtils dateOnlyFromDate:_itemDate]];
            break;
        }
    }
    
    if (!ret) {
        float nearWidth = MAXFLOAT;
        NSTimeInterval choosenInterval = -1;
        for (BWDDatePeriod *period in _contentView.recordedPeriods) {
            float delta = ABS([self xOffsetFromInterval:period.startInterval] - x);
            if (delta < nearWidth && delta <= 20) {
                choosenInterval = period.startInterval;
                nearWidth = delta;
            }
            delta = ABS([self xOffsetFromInterval:period.endInterval] - x);
            if (delta < nearWidth && delta <= 20) {
                choosenInterval = period.endInterval - MIN(10, period.endInterval - period.startInterval);
                nearWidth = delta;
            }
        }
        if (choosenInterval >= 0) {
            ret = [NSDate dateWithTimeInterval:choosenInterval sinceDate:[BWDUtils dateOnlyFromDate:_itemDate]];
        }
    }
    
    return ret;
}


- (void)movePointer:(UIPanGestureRecognizer *)recognizer {
    if (!_isArchive) {
        return;
    }
    CGPoint translation = [recognizer translationInView:self];
    float leftBound = MAX(self.contentOffset.x + BWDTimelineHorizontalGaps,
                          [self xOffsetFromInterval:((BWDDatePeriod *)_contentView.recordedPeriods[_archivePeriodIndex]).startInterval]);
    float rightBound = MIN(self.contentOffset.x + self.frame.size.width - BWDTimelineHorizontalGaps,
                           [self xOffsetFromInterval:((BWDDatePeriod *)_contentView.recordedPeriods[_archivePeriodIndex]).endInterval]);
    recognizer.view.center = CGPointMake( MIN(MAX(recognizer.view.center.x + translation.x, leftBound), rightBound),
                                         recognizer.view.center.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
    
    float secondsPerPoint = 3600 * 24 / (self.contentSize.width - BWDTimelineHorizontalGaps * 2);
    NSTimeInterval secondsOffset = (recognizer.view.center.x - BWDTimelineHorizontalGaps) * secondsPerPoint;
    _pointerCurrentDate = [NSDate dateWithTimeInterval:secondsOffset sinceDate:[BWDUtils dateOnlyFromDate:_itemDate]];
    if (_delegate && [_delegate respondsToSelector:@selector(timelineItem:ItemDidScrollPointerDate:)]) {
        [_delegate timelineItem:self ItemDidScrollPointerDate:_pointerCurrentDate];
    }
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        if (_delegate && [_delegate respondsToSelector:@selector(timelineItem:ItemDidSetPointerDate:)]) {
            [_delegate timelineItem:self ItemDidSetPointerDate:_pointerCurrentDate];
        }
    }
}


- (float)xOffsetFromDate:(NSDate *)date {
    float secondsPerPoint = 3600 * 24 / (self.contentSize.width - BWDTimelineHorizontalGaps * 2);
    float secondsOffset = ABS([[BWDUtils dateOnlyFromDate:date] timeIntervalSinceDate:date]);
    return BWDTimelineHorizontalGaps + secondsOffset / secondsPerPoint;
}


- (float)xOffsetFromInterval:(NSTimeInterval)interval {
    float secondsPerPoint = 3600 * 24 / (self.contentSize.width - BWDTimelineHorizontalGaps * 2);
    return BWDTimelineHorizontalGaps + interval / secondsPerPoint;
}


- (NSTimeInterval)secondsOffsetFromX:(float)x {
    float secondsPerPoint = 3600 * 24 / (self.contentSize.width - BWDTimelineHorizontalGaps * 2);
    return (x - BWDTimelineHorizontalGaps) * secondsPerPoint;
}


#pragma mark gesture recognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if(gestureRecognizer == _panRecognizer) {
        return YES;
    }
    return NO;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return (gestureRecognizer == _panRecognizer);
}

@end
