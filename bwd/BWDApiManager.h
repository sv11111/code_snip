//
//  BWDApiManager.h
//  camDrive
//
//  Created by adm on 27.11.15.
//  Copyright © 2015 adm. All rights reserved.
//

static NSString *const BWDServerName = @"";
static NSString *const BWDServerIP = @"";

#import <Foundation/Foundation.h>

@interface BWDApiManager : NSObject

+ (instancetype)sharedInstance;

- (void)getC____________________d:(NSString *)channelId completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (void)setS____________________e:(NSArray *)schedule byChannelId:(NSString *)channelId completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (void)getA____________________d:(NSString *)channelId completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (void)getA____________________d:(NSString *)c__d
                       s__e:(NSDate *)s__e
                         e__e:(NSDate *)e__e
                      completion:(void(^)(id result))completion
                         failure:(void(^)(NSError *error))failure;
- (void)getR____________________e:(NSDate *)d__e
                           c__d:(NSString *)c__d
                           d__n:(int)d__n
                              m__n:(NSNumber *)m___n
                          completion:(void(^)(id result))completion
                             failure:(void(^)(NSError *error))failure;
- (void)getC____________________t:(float)a__t completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (void)getR____________________n:(NSInteger)p__n channelId:(NSString *)channelId completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (void)getC____________________e:(NSDate *)a__e completion:(void(^)(id result))c__n failure:(void(^)(NSError *error))failure;
- (void)getU____________________n:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;


- (void)signInWithLogin:(NSString *)login pass:(NSString *)pass completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (void)logOutwithCompletion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure;
- (BOOL)isAuthorized;

@end
