//
//  BWDSignInViewController.m
//  camDrive
//
//  Created by adm on 27.11.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import "BWDSignInViewController.h"
#import "BWDUtils.h"

@interface BWDSignInViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *loginTF;
@property (nonatomic, weak) IBOutlet UITextField *passTF;
@property (nonatomic, weak) IBOutlet UIButton *enterBtn;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation BWDSignInViewController{
    UITapGestureRecognizer *_tapGesture;
    UITextField *_activeTextField;
    CGFloat _curKeyboardHeight;
    CGFloat _initialTopOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _loginTF.layer.cornerRadius = 4;
    _loginTF.layer.borderWidth = 0.5;
    _loginTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _passTF.layer.cornerRadius = 4;
    _passTF.layer.borderWidth = 0.5;
    _passTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognize:)];
    _tapGesture.enabled = NO;
    [self.view addGestureRecognizer:_tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _initialTopOffset = _topConstraint.constant;
    // Do any additional setup after loading the view.
}


- (void)tapGestureRecognize:(UITapGestureRecognizer *)recognizer {
    if (_loginTF.editing) [_loginTF resignFirstResponder];
    else [_passTF resignFirstResponder];
}

- (IBAction)enterButtTouchUp:(id)sender {
    if (_loginTF.editing) [_loginTF resignFirstResponder];
    if (_passTF.editing) [_passTF resignFirstResponder];
    [self tryAuth];
}

- (void)tryAuth {
    if (_loginTF.text.length > 0 && _passTF.text.length > 0){
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.dimBackground = YES;
        [self.view addSubview:HUD];
        [HUD show:YES];
        
        [[BWDApiManager sharedInstance] signInWithLogin:_loginTF.text pass:_passTF.text completion:^(id result) {
            [HUD hide:YES];
            //UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Велкам!" message:@"Логин прошёл успешно" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //[av show];
            
            //[[BWDApiManager sharedInstance] getCamerasList:nil failure:nil];
        } failure:^(NSError *error) {
            [HUD hide:YES];
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:error.userInfo[@"errDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
        }];
    }
    else {
        if (_loginTF.text.length == 0 && _passTF.text.length != 0) {
            [BWDUtils borderColorChangeAnimation:@[_loginTF] toColor:nil];
        }
        else if (_passTF.text.length == 0 && _loginTF.text.length != 0) {
            [BWDUtils borderColorChangeAnimation:@[_passTF] toColor:nil];
        }
        else {
            [BWDUtils borderColorChangeAnimation:@[_loginTF, _passTF] toColor:nil];
        }
    }
}

#pragma mark UITextFieldDelegate and keyboard handlers

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _tapGesture.enabled = NO;
}

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _activeTextField = textField;
    
    if (_curKeyboardHeight > 0) {
        [self animateTextFieldOffset];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _loginTF) {
        [_passTF becomeFirstResponder];
    }
    else {
        [_passTF resignFirstResponder];
        [self tryAuth];
    }
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notif {
    CGSize kbSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _curKeyboardHeight = kbSize.height;
    if (_activeTextField) {
        [self animateTextFieldOffset];
    }
}

- (void)keyboardWillHide:(NSNotification *)notif {
    _curKeyboardHeight = 0;
    
    if (_activeTextField && _topConstraint.constant != _initialTopOffset) {
        _topConstraint.constant = _initialTopOffset;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)animateTextFieldOffset {
    if (_activeTextField) {
        CGFloat offset = (_enterBtn.frame.origin.y + _activeTextField.frame.size.height + 10) - (self.view.frame.size.height - _curKeyboardHeight);
        if (offset > 0) {
            _topConstraint.constant -= offset;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

