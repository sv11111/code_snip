//
//  UIView+Appearance.m
//  PhotoViewer
//
//  Created by Semyon Vlasov on 27.07.15.
//  Copyright (c) 2015 SVV. All rights reserved.
//

#import "UIView+Appearance.h"

@implementation UIView (Appearance)

- (void)addSubviewOverlaySuperView:(UIView *)subview {
    if (subview.superview == self) return;
    
    [self addSubview:subview];
    [subview setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[subview]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subview]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
}

@end
