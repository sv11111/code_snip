//
//  BWWAudioManager.m
//  BWRTSPPlayer
//
//  Created by adm on 30.09.15.
//  Copyright © 2015 Beward Ltd. All rights reserved.
//


#import "BMPAudioManager.h"
#import "BMPMediaDecoder.h"
#import "TargetConditionals.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioSession.h>
#import <Accelerate/Accelerate.h>

#define MAX_FRAME_SIZE 4096
#define MAX_CHAN       2

#define DOMOFON_PACKET_SIZE 640

#define RECORD_BUS  1
#define PLAYBACK_BUS  0

static BOOL checkError(OSStatus error,
                       const char *operation);

static OSStatus playbackCallback (void *inRefCon,
                                AudioUnitRenderActionFlags	*ioActionFlags,
                                const AudioTimeStamp * inTimeStamp,
                                UInt32 inOutputBusNumber,
                                UInt32 inNumberFrames,
                                AudioBufferList* ioData);

static OSStatus recordCallback(void * inRefCon,
                                  AudioUnitRenderActionFlags * ioActionFlags,
                                  const AudioTimeStamp * inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList * ioData);


const AudioStreamBasicDescription kDefaultStreamFormat = {  8000.00,
                                                            kAudioFormatLinearPCM,
                                                            kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked,
                                                            2, //mBytesPerPacket
                                                            1, //mFramesPerPacket
                                                            2, //mBytesPerFrame
                                                            1, //mChannelsPerFrame
                                                            16,//mBitsPerChannel
                                                            0  //mReserved
                                                         };
    

@interface BMPAudioManager () {
    
    BOOL                        _playAfterSessionEndInterruption;
    BOOL                        _initialized;
    
    BOOL                        _recordActivated;
    BOOL                        _playbackActivated;
    
    AudioUnit                   _audioUnit;
    
    float                       *_playbackBufData;
    //NSMutableData               *_recordBufData;
    
}

@property(nonatomic, assign) BOOL unitActivated;
@property(nonatomic, assign) BOOL sessionActivated;

@property(nonatomic, strong) NSMutableData *recordBufData;

@end

@implementation BMPAudioManager

#pragma mark - Init methods

- (instancetype)init
{
    self = [super init];
	if (self) {
        
        _playbackBufData = (float *)calloc(MAX_FRAME_SIZE*MAX_CHAN, sizeof(float));
        
        _recordBufData = [NSMutableData dataWithCapacity:DOMOFON_PACKET_SIZE];
        
        [self initAudioUnit];
	}	
	return self;
}

- (void)dealloc
{
    if (_playbackBufData) {
        
        free(_playbackBufData);
        _playbackBufData = NULL;
    }
    [self releaseAudioUnit];
    self.sessionActivated = NO;
}


#pragma mark - Public methods

- (void)stopMode:(BMPAudioManagerMode)mode {
//    NSLog(@"[AUDIOMANAGER] set stop mode %@", mode == BMPAudioManagerModeRecord ? @"record" : @"playback");
    if ((mode == BMPAudioManagerModePlayback || mode == BMPAudioManagerModeGlobal) && _playbackActivated) {
        [self restartAudioUnitWithChangesBlock:^{
            [self makeMode:BMPAudioManagerModePlayback enabled:NO];
        }];
    }
    
    if ((mode == BMPAudioManagerModeRecord || mode == BMPAudioManagerModeGlobal) && _recordActivated) {
        [self restartAudioUnitWithChangesBlock:^{
            [self makeMode:BMPAudioManagerModeRecord enabled:NO];
        }];
    }
    
    if (!_playbackActivated && !_recordActivated) {
        self.unitActivated = NO;
        self.sessionActivated = NO;
    }
}


- (void)startMode:(BMPAudioManagerMode)mode {
//    NSLog(@"[AUDIOMANAGER] set start mode %@", mode == BMPAudioManagerModeRecord ? @"record" : @"playback");
    
    if ((mode == BMPAudioManagerModePlayback || mode == BMPAudioManagerModeGlobal) && !_playbackActivated) {
        [self restartAudioUnitWithChangesBlock:^{
            [self makeMode:BMPAudioManagerModePlayback enabled:YES];
        }];
        
    }
    
    if ((mode == BMPAudioManagerModeRecord || mode == BMPAudioManagerModeGlobal) && !_recordActivated) {
        [self restartAudioUnitWithChangesBlock:^{
            [self makeMode:BMPAudioManagerModeRecord enabled:YES];
        }];
    }
    self.unitActivated = YES;
    self.sessionActivated = YES;

    
}


#pragma mark - getters

- (BOOL)playing {
    return self.unitActivated && self.sessionActivated && _playbackActivated;
}

- (BOOL)recording {
    return self.unitActivated && self.sessionActivated && _recordActivated;
}

#pragma mark - external setters

- (void) setPlaybackFormat:(AudioStreamBasicDescription)playbackFormat {
//    NSLog(@"[AUDIOMANAGER] set playback format");
    [self restartAudioUnitWithChangesBlock:^{
        [self setStreamFormat:_playbackFormat = playbackFormat forMode:BMPAudioManagerModePlayback];
    }];
}

- (void) setRecordFormat:(AudioStreamBasicDescription)recordFormat {
//    NSLog(@"[AUDIOMANAGER] set record format");
    [self restartAudioUnitWithChangesBlock:^{
        [self setStreamFormat:_recordFormat = recordFormat forMode:BMPAudioManagerModeRecord];
    }];
}



#pragma mark - internal setters
- (void)setUnitActivated:(BOOL)unitActivated {
    if (_unitActivated != unitActivated) {
        _unitActivated = unitActivated;
        if (_unitActivated) {
//             NSLog(@"[AUDIOMANAGER] need to activate audioUnit");
            _unitActivated = !checkError(AudioOutputUnitStart(_audioUnit),
                                         "[AUDIOMANAGER] Couldn't start the output unit");
//             NSLog(@"[AUDIOMANAGER] audioUnit activation complete");
        }
        else {
//            NSLog(@"[AUDIOMANAGER] need to deactivate audioUnit");
            _unitActivated = checkError(AudioOutputUnitStop(_audioUnit),
                                        "[AUDIOMANAGER] Couldn't stop the output unit");
//            NSLog(@"[AUDIOMANAGER] audioUnit deactivation complete");
        }
    }
}

- (void)setSessionActivated:(BOOL)sessionActivated {
    if (_sessionActivated != sessionActivated) {
        if (!sessionActivated) {
            _sessionActivated = NO;
            [self deactivateAudioSession];
        }
        else {
            _sessionActivated = [self setupAudioSession];
        }
    }
}


#pragma mark - Audio unit operations

- (void)initAudioUnit {
    [self createAudioUnit];
    
    [self makeMode:BMPAudioManagerModePlayback enabled:NO];
    [self makeMode:BMPAudioManagerModeRecord enabled:NO];
    
    [self setGainCorrectionEnabled:YES];
    [self preventAudioUnitBufferInit];
    
    [self setStreamFormat:_playbackFormat = kDefaultStreamFormat forMode:BMPAudioManagerModePlayback];
    [self setStreamFormat:_recordFormat = kDefaultStreamFormat forMode:BMPAudioManagerModeRecord];
    
    [self setCallback:playbackCallback withContext:self forMode:BMPAudioManagerModePlayback];
    [self setCallback:recordCallback withContext:self forMode:BMPAudioManagerModeRecord];
    
    AudioUnitInitialize(_audioUnit);
    _initialized = YES;
}


- (void)createAudioUnit {
    AudioComponentDescription description;
    description.componentType = kAudioUnitType_Output;
    description.componentSubType = /*kAudioUnitSubType_RemoteIO; */kAudioUnitSubType_VoiceProcessingIO;
    description.componentManufacturer = kAudioUnitManufacturer_Apple;
    description.componentFlags = 0;
    description.componentFlagsMask = 0;
    
    // Get component
    AudioComponent component = AudioComponentFindNext(NULL, &description);
    checkError(AudioComponentInstanceNew(component, &_audioUnit), "[AUDIOMANAGER] Couldn't create the output audio unit");
}


- (void)releaseAudioUnit
{
    if(_initialized) {
        self.unitActivated = NO;
        
        checkError(AudioUnitUninitialize(_audioUnit), "[AUDIOMANAGER] Couldn't uninitialize the audio unit");
        checkError(AudioComponentInstanceDispose(_audioUnit), "[AUDIOMANAGER] Couldn't dispose the output audio unit");
        
        _initialized = NO;
    }
}


- (void)restartAudioUnitWithChangesBlock:(void(^)())changesBlock {
//    NSLog(@"[AUDIOMANAGER] restart audio unit");
    
    if ([_playbackDelegate respondsToSelector:@selector(audioManagerWillBeginRestart:)]) {
        [_playbackDelegate audioManagerWillBeginRestart:self];
    }
    if ([_recordDelegate respondsToSelector:@selector(audioManagerWillBeginRestart:)]) {
        [_recordDelegate audioManagerWillBeginRestart:self];
    }
    
    if (_unitActivated) {
//        NSLog(@"[AUDIOMANAGER] need to stop audioUnit after REinitialize");
        checkError(AudioOutputUnitStop(_audioUnit), "[AUDIOMANAGER] Couldn't stop the output unit");
//        NSLog(@"[AUDIOMANAGER] audio unit stopping complete");

    }
//    NSLog(@"[AUDIOMANAGER] audio unit UNinitialize start");
    AudioUnitUninitialize(_audioUnit);
//    NSLog(@"[AUDIOMANAGER] audio unit UNinitialize complete");
    
    if (changesBlock) {
        changesBlock();
    }
    
//    NSLog(@"[AUDIOMANAGER] audio unit initialize start");
    AudioUnitInitialize(_audioUnit);
//    NSLog(@"[AUDIOMANAGER] audio unit initialize complete");
    
    if (_unitActivated) {
//        NSLog(@"[AUDIOMANAGER] need to start audioUnit after REinitialize");
        checkError(AudioOutputUnitStart(_audioUnit), "Couldn't start the output unit");
//        NSLog(@"[AUDIOMANAGER] audio unit starting complete");
    }
    
    if ([_playbackDelegate respondsToSelector:@selector(audioManagerDidEndRestart:)]) {
        [_playbackDelegate audioManagerDidEndRestart:self];
    }
    if ([_recordDelegate respondsToSelector:@selector(audioManagerDidEndRestart:)]) {
        [_recordDelegate audioManagerDidEndRestart:self];
    }
}


- (void)setStreamFormat:(AudioStreamBasicDescription)format forMode:(BMPAudioManagerMode)mode {
    UInt32 size = sizeof(AudioStreamBasicDescription);
    //TODO: check for input scope
    checkError(AudioUnitSetProperty(_audioUnit,
                                    kAudioUnitProperty_StreamFormat,
                                    (mode == BMPAudioManagerModeRecord) ? kAudioUnitScope_Output : kAudioUnitScope_Input,
                                    (mode == BMPAudioManagerModeRecord) ? RECORD_BUS : PLAYBACK_BUS,
                                    &format,
                                    size),
               "[AUDIOMANAGER] Couldn't get the hardware output stream format");
}


- (void)makeMode:(BMPAudioManagerMode)mode enabled:(BOOL)enabled {
    UInt32 flag = enabled ? 1 : 0;
    if (mode == BMPAudioManagerModeRecord) {
        _recordActivated = enabled;
    }
    else {
        _playbackActivated = enabled;
    }
    checkError(AudioUnitSetProperty(_audioUnit,
                                        kAudioOutputUnitProperty_EnableIO,
                                        (mode == BMPAudioManagerModeRecord) ? kAudioUnitScope_Input : kAudioUnitScope_Output,
                                        (mode == BMPAudioManagerModeRecord) ? RECORD_BUS : PLAYBACK_BUS,
                                        &flag,
                                        sizeof(flag)),
               [[NSString stringWithFormat:@"[AUDIOMANAGER] Couldn't %@ audio unit %@",
                 enabled ? @"enabled" : @"disabled",
                 (mode == BMPAudioManagerModeRecord) ? @"record" : @"playback"] cStringUsingEncoding:NSASCIIStringEncoding]);
    
}


- (void)setGainCorrectionEnabled:(BOOL)enabled {
    UInt32 flag = enabled ? 1 : 0;
    checkError(AudioUnitSetProperty(_audioUnit,
                                  kAUVoiceIOProperty_VoiceProcessingEnableAGC,
                                  kAudioUnitScope_Global,
                                  RECORD_BUS,
                                  &flag,
                                  sizeof(flag)), "[AUDIOMANAGER] couldn't set gain correction");
}

- (void)setCallback:(AURenderCallback)callback withContext:(id)context forMode:(BMPAudioManagerMode)mode {
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = callback;
    callbackStruct.inputProcRefCon = (__bridge void *)(context);
    
    checkError(AudioUnitSetProperty(_audioUnit,
                                        (mode == BMPAudioManagerModeRecord) ? kAudioOutputUnitProperty_SetInputCallback : kAudioUnitProperty_SetRenderCallback,
                                        (mode == BMPAudioManagerModeRecord) ? kAudioUnitScope_Output : kAudioUnitScope_Input,
                                        (mode == BMPAudioManagerModeRecord) ? RECORD_BUS : PLAYBACK_BUS,
                                        &callbackStruct,
                                        sizeof(callbackStruct)), "[AUDIOMANAGER] Couldn't set the callback on the audio unit");
}

- (void)preventAudioUnitBufferInit {
    UInt32 enabled = 1;
    checkError(AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,
                                  RECORD_BUS,
                                  &enabled,
                                  sizeof(enabled)), "[AUDIOMANAGER] Couldn't prevent buffer allocating");
}


- (UInt32)setMaxFramePerSlice:(UInt32)framePerSlice {
    UInt32 maxFramesPerSlice = framePerSlice;
    if (checkError(AudioUnitSetProperty(_audioUnit,
                                        kAudioUnitProperty_MaximumFramesPerSlice,
                                        kAudioUnitScope_Global,
                                        0,
                                        &maxFramesPerSlice,
                                        sizeof(UInt32)), "[AUDIOMANAGER] couldn't set max frames per slice on AURemoteIO")) {
        return 0;
    }
    
    // Get the property value back from AURemoteIO. We are going to use this value to allocate buffers accordingly
    UInt32 propSize = sizeof(UInt32);
    if (checkError(AudioUnitGetProperty(_audioUnit,
                                        kAudioUnitProperty_MaximumFramesPerSlice,
                                        kAudioUnitScope_Global,
                                        0,
                                        &maxFramesPerSlice,
                                        &propSize), "[AUDIOMANAGER] couldn't get max frames per slice on AURemoteIO")) {
        return 0;
    }
    return maxFramesPerSlice;
}



#pragma mark - Audio session operations

- (void)deactivateAudioSession
{
        AVAudioSession *sessionInstance = [AVAudioSession sharedInstance];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVAudioSessionInterruptionNotification
                                                      object:sessionInstance];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVAudioSessionRouteChangeNotification
                                                      object:sessionInstance];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVAudioSessionMediaServicesWereResetNotification
                                                      object:sessionInstance];
        
        NSError *error;
        [[AVAudioSession sharedInstance] setActive:NO error:&error];

}


- (BOOL)setupAudioSession
{
    BOOL isSetupSucceed;
    
    @try {
        // Configure the audio session
        AVAudioSession *sessionInstance = [AVAudioSession sharedInstance];
        
        // we are going to play and record so we pick that category
        NSError *error = nil;
        
        //        [sessionInstance setCategory:AVAudioSessionCategoryPlayback error:&error];
        [sessionInstance setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error];
        //        NSAssert((OSStatus)error.code, "couldn't set session's audio category");
        
        // set the buffer duration to 5 ms
        NSTimeInterval bufferDuration = .005;
        [sessionInstance setPreferredIOBufferDuration:bufferDuration error:&error];
        //        NSAssert((OSStatus)error.code, "couldn't set session's I/O buffer duration");
        
        // set the session's sample rate 44100
        [sessionInstance setPreferredSampleRate:44100 error:&error];
        //        NSAssert((OSStatus)error.code, "couldn't set session's preferred sample rate");
        
        
        
        
        // add interruption handler
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleInterruption:)
                                                     name:AVAudioSessionInterruptionNotification
                                                   object:sessionInstance];
        
        // we don't do anything special in the route change notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleRouteChange:)
                                                     name:AVAudioSessionRouteChangeNotification
                                                   object:sessionInstance];
        
        // if media services are reset, we need to rebuild our audio chain
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleMediaServerReset:)
                                                     name:AVAudioSessionMediaServicesWereResetNotification
                                                   object:sessionInstance];
        
        // activate the audio session
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        //        NSAssert((OSStatus)error.code, "couldn't set session active");
        
        isSetupSucceed = YES;
    }
    @catch (NSException *e) {
        NSLog(@"Error returned from setupAudioSession: %@: %@", e.name, e.reason);
        isSetupSucceed = NO;
    }
    @catch (...) {
        NSLog(@"Unknown error returned from setupAudioSession");
        isSetupSucceed = NO;
    }
    
    return isSetupSucceed;
}


#pragma mark - Audio session handlers

- (void)handleInterruption:(NSNotification *)notification
{
    @try {
        UInt8 theInterruptionType = [[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
        //        NSLog(@"Session interrupted > --- %s ---\n", theInterruptionType == AVAudioSessionInterruptionTypeBegan ? "Begin Interruption" : "End Interruption");
        
        if (theInterruptionType == AVAudioSessionInterruptionTypeBegan) {
            _playAfterSessionEndInterruption = _unitActivated;
            self.unitActivated = NO;
        }
        
        if (theInterruptionType == AVAudioSessionInterruptionTypeEnded) {
            // make sure to activate the session
            NSError *error = nil;
            [[AVAudioSession sharedInstance] setActive:YES error:&error];
            if (nil != error) NSLog(@"[AUDIOMANAGER] AVAudioSession set active failed with error: %@", error);
            
            if(_playAfterSessionEndInterruption) {
                _playAfterSessionEndInterruption = NO;
                self.unitActivated = YES;
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"Error: %@ %@ %@", e.name, e.reason, e.userInfo);
    }
}


- (void)handleRouteChange:(NSNotification *)notification
{
    UInt8 reasonValue = [[notification.userInfo valueForKey:AVAudioSessionRouteChangeReasonKey] intValue];
    //    AVAudioSessionRouteDescription *routeDescription = [notification.userInfo valueForKey:AVAudioSessionRouteChangePreviousRouteKey];
    
    //    NSLog(@"Route change:");
    switch (reasonValue) {
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            //            NSLog(@"     NewDeviceAvailable");
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            //            NSLog(@"     OldDeviceUnavailable");
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            //            NSLog(@"     CategoryChange");
            //            NSLog(@" New Category: %@", [[AVAudioSession sharedInstance] category]);
            break;
        case AVAudioSessionRouteChangeReasonOverride:
            //            NSLog(@"     Override");
            break;
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            //            NSLog(@"     WakeFromSleep");
            break;
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            //            NSLog(@"     NoSuitableRouteForCategory");
            break;
        default:
            NSLog(@"     ReasonUnknown");
    }
    
}

- (void)handleMediaServerReset:(NSNotification *)notification
{
    self.sessionActivated = NO;
    self.sessionActivated = YES;
    [self restartAudioUnitWithChangesBlock:nil];
}


#pragma mark - Getters for recording callback
- (AudioUnit)getAudioUnit {
    return _audioUnit;
}

//- (NSMutableData *)getBufData {
//    return _recordBufData;
//}

#pragma mark - Playback processing

- (BOOL)renderFrames:(UInt32)numFrames
              ioData:(AudioBufferList*)ioData
{
    
    for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {
        memset(ioData->mBuffers[iBuffer].mData, 0, ioData->mBuffers[iBuffer].mDataByteSize);
    }
    
    if (self.playing && _outputBlock) {
        
        // Collect data to render from the callbacks
        _outputBlock(_playbackBufData, numFrames, _playbackFormat.mChannelsPerFrame);
        // Put the rendered data into the output buffer
        UInt32 bytesPerSample = _playbackFormat.mBitsPerChannel / 8;
        if (bytesPerSample == 4) // then we've already got floats
        {
            float zero = 0.0;
            
            for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {
                
                int thisNumChannels = ioData->mBuffers[iBuffer].mNumberChannels;
                
                for (int iChannel = 0; iChannel < thisNumChannels; ++iChannel) {
                    vDSP_vsadd(_playbackBufData+iChannel, _playbackFormat.mChannelsPerFrame, &zero, (float *)ioData->mBuffers[iBuffer].mData, thisNumChannels, numFrames);
                }
            }
        }
        else if (bytesPerSample == 2) // then we need to convert SInt16 -> Float (and also scale)
        {
            float scale = (float)INT16_MAX;
            vDSP_vsmul(_playbackBufData, 1, &scale, _playbackBufData, 1, numFrames * _playbackFormat.mChannelsPerFrame);
            
            for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {
                
                int thisNumChannels = ioData->mBuffers[iBuffer].mNumberChannels;
                
                for (int iChannel = 0; iChannel < thisNumChannels; ++iChannel) {
                    vDSP_vfix16(_playbackBufData+iChannel, _playbackFormat.mChannelsPerFrame, (SInt16 *)ioData->mBuffers[iBuffer].mData+iChannel, thisNumChannels, numFrames);
                }
            }
        }
    }
    
    return noErr;
}

@end

#pragma mark - callbacks

static OSStatus recordCallback(void * inRefCon,
                                  AudioUnitRenderActionFlags * ioActionFlags,
                                  const AudioTimeStamp * inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList * ioData) {
    BMPAudioManager *sm = (__bridge BMPAudioManager *)inRefCon;
    
    AudioBufferList *bufferList = (AudioBufferList *)malloc(sizeof(AudioBufferList)); // <- Fill this up with buffers (you will want to malloc it, as it's a dynamic-length list)
    bufferList->mNumberBuffers = 1;
    bufferList->mBuffers[0].mNumberChannels = sm.recordFormat.mChannelsPerFrame;
    UInt32 wordsize = ((sm.recordFormat.mFormatFlags & kAudioFormatFlagIsSignedInteger) == kAudioFormatFlagIsSignedInteger) ? sizeof(SInt16) : sizeof(float);
    UInt32 dataByteSize = sm.recordFormat.mChannelsPerFrame * inNumberFrames * wordsize;
    bufferList->mBuffers[0].mDataByteSize = dataByteSize;
    bufferList->mBuffers[0].mData = calloc(sm.recordFormat.mChannelsPerFrame * inNumberFrames, sizeof(SInt16));
    
    // Then:
    // Obtain recorded samples
    
    OSStatus status;
    
    status = AudioUnitRender([sm getAudioUnit],
                             ioActionFlags,
                             inTimeStamp,
                             inBusNumber,
                             inNumberFrames,
                             bufferList);
    
    //NSLog(@"in Timestamp:%f", inTimeStamp->mSampleTime);
    for( UInt32 y=0; y<bufferList->mNumberBuffers; y++ )
    {
        AudioBuffer audioBuff = bufferList->mBuffers[y];

        SInt16 *frame = (SInt16*)audioBuff.mData;
        //NSMutableData *bufData = [sm getBufData];
        UInt32 bufSize = sm.recordBufData.length;
        
        if (DOMOFON_PACKET_SIZE - bufSize > audioBuff.mDataByteSize) {
            [sm.recordBufData appendBytes:frame length:audioBuff.mDataByteSize];
        }
        else {
            NSMutableData * soundData = [NSMutableData dataWithCapacity:DOMOFON_PACKET_SIZE];
            [soundData appendBytes:sm.recordBufData.bytes length:sm.recordBufData.length];
            UInt32 deltaBytes = DOMOFON_PACKET_SIZE - bufSize;
            [soundData appendBytes:frame length: deltaBytes];
            
            UInt32 restBytes = audioBuff.mDataByteSize - deltaBytes;
            sm.recordBufData = [NSMutableData dataWithCapacity:DOMOFON_PACKET_SIZE];
            if (restBytes > 0) {
                [sm.recordBufData appendBytes:(frame + (deltaBytes)/2) length:restBytes];
            }
            
            if ([sm.recordDelegate respondsToSelector:@selector(audioManager:didRecordDataWithNumberFrames:soundData:)]) {
                [sm.recordDelegate audioManager:sm didRecordDataWithNumberFrames:DOMOFON_PACKET_SIZE/sizeof(SInt16) soundData:soundData];
            }

        }
        
    }
    
    
    return noErr;
}

static OSStatus playbackCallback (void						*inRefCon,
                                AudioUnitRenderActionFlags	* ioActionFlags,
                                const AudioTimeStamp 		* inTimeStamp,
                                UInt32						inOutputBusNumber,
                                UInt32						inNumberFrames,
                                AudioBufferList				* ioData)
{
    BMPAudioManager *sm = (__bridge BMPAudioManager *)inRefCon;
    return [sm renderFrames:inNumberFrames ioData:ioData];
//    return YES;
}

static BOOL checkError(OSStatus error, const char *operation)
{
	if (error == noErr)
        return NO;
	
	char str[20] = {0};
	// see if it appears to be a 4-char-code
	*(UInt32 *)(str + 1) = CFSwapInt32HostToBig(error);
	if (isprint(str[1]) && isprint(str[2]) && isprint(str[3]) && isprint(str[4])) {
		str[0] = str[5] = '\'';
		str[6] = '\0';
	} else
		// no, format it as an integer
		sprintf(str, "%d", (int)error);
    
	NSLog(@"[AUDIOMANAGER] Error: %s (%s)\n", operation, str);
    
	//exit(1);
    
    return YES;
}
