//
//  BWWAudioManager.h
//  BWRTSPPlayer
//
//  Created by adm on 30.09.15.
//  Copyright © 2015 Beward Ltd. All rights reserved.
//


#import <CoreFoundation/CoreFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

typedef enum {
    BMPAudioManagerModePlayback,
    BMPAudioManagerModeRecord,
    BMPAudioManagerModeGlobal
} BMPAudioManagerMode;

@class BMPMediaDecoder, BMPAudioManager;

@protocol BMPAudioManagerDelegate <NSObject>
@optional
- (void)audioManagerWillBeginRestart:(BMPAudioManager *)manager;
- (void)audioManagerDidEndRestart:(BMPAudioManager *)manager;

@end

@protocol BMPAudioManagerRecordDelegate <BMPAudioManagerDelegate>

- (void)audioManager:(BMPAudioManager *)manager didRecordDataWithNumberFrames:(UInt32)numberFrames soundData:(NSData *)soundData;

@end


typedef void (^BWWAudioManagerPlaybackBlock)(float *data, UInt32 numFrames, UInt32 numChannels);

@interface BMPAudioManager : NSObject

@property (nonatomic, readonly) BOOL playing;
@property (nonatomic, readonly) BOOL recording;

@property (nonatomic, assign) AudioStreamBasicDescription playbackFormat;
@property (nonatomic, assign) AudioStreamBasicDescription recordFormat;

@property (readwrite, copy) BWWAudioManagerPlaybackBlock outputBlock;
@property (nonatomic, weak) id<BMPAudioManagerRecordDelegate> recordDelegate;
@property (nonatomic, weak) id<BMPAudioManagerDelegate> playbackDelegate;

- (void)stopMode:(BMPAudioManagerMode)mode;
- (void)startMode:(BMPAudioManagerMode)mode;

@end
