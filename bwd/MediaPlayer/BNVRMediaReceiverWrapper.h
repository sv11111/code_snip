//
//  BNVRMediaReceiverWrapper.h
//  BWRTSPPlayer
//
//  Created by adm on 03.02.16.
//  Copyright © 2015 Beward Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMPFrameFormats.h"

extern NSString * const MediaReceiverIsReleased;

typedef NS_ENUM(NSInteger, MediaReceiverError)
{
    MediaReceiverErrorInvalidOperation = 0,
    MediaReceiverErrorInvalidURL,
    MediaReceiverErrorStreamInfoFindFail,
    MediaReceiverErrorAllocateFail,
    MediaReceiverErrorDecodingFail,
    MediaReceiverErrorCodecFindFail,
    MediaReceiverErrorCodecOpenFail,
    MediaReceiverErrorInvalidParameter,
    MediaReceiverErrorConvertingFail,
    MediaReceiverErrorConnectionTerminated,
};

typedef  NS_ENUM(NSInteger, MediaReceiverLogLevel)
{
    MediaReceiverLogLevelFatal = 0,
    MediaReceiverLogLevelError,
    MediaReceiverLogLevelWarning,
    MediaReceiverLogLevelInfo,
    MediaReceiverLogLevelDebug
};

@protocol MediaReceiverCallbackProtocol

- (void)videoStreamInfoChangedWidth:(size_t)width
                             height:(size_t)height
                             lenght:(int64_t)lenght;

- (void)audioStreamInfoChangedSampleRate:(size_t)sampleRate
                                channels:(size_t)channels
                                  lenght:(int64_t)lenght;

- (void)receivedVideoData:(BMPVideoFrame*)RGBAFrame;
- (void)receivedAudioData:(BMPAudioFrame*)S16Frame;
- (void)error:(MediaReceiverError)errorCode withMessage:(NSString*)message;
- (void)log:(MediaReceiverLogLevel)logLevel withMessage:(NSString*)message;

@end

@interface BNVRMediaReceiverWrapper : NSObject

@property (weak, nonatomic) id <MediaReceiverCallbackProtocol> delegate;
@property (strong, nonatomic) NSString *kMediaReceiverLockName;

- (instancetype)init;
- (instancetype)initWithDelegate:(id<MediaReceiverCallbackProtocol>)delegate;

- (void)setupMediaReceiver;
- (void)releaseMediaReceiver;
- (void)setDataSource:(NSDictionary*)values;
- (void)setOutParameters:(NSDictionary*)values;
- (void)play;
- (void)pause;
- (void)stop;
- (void)seek:(int64_t)to;

@end