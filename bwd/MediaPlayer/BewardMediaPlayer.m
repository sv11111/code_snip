//
//  BewardMediaPlayer.m
//  BWRTSPPlayer
//
//  Created by adm on 25.09.15.
//  Copyright © 2015 Beward Ltd. All rights reserved.
//

#import "BewardMediaPlayer.h"
#import "BMPMediaDecoder.h"
#import "BMPAudioManager.h"
#import "BMPFrameFormats.h"
#import "BMPVideoGLView.h"

#import "BMPGLKViewController.h"
#import <GLKit/GLKit.h>

NSString *const BMPMediaError = @"BMPMediaError";
NSString *const BMPMediaInfo = @"BMPMediaInfo";
NSString *const BMPMediaDuration = @"BMPMediaDuration";
NSString *const BMPMediaTimeStampChanged = @"BMPMediaTimeStampChanged";

@interface BewardMediaPlayer () {
    
    BMPMediaDecoder *_decoder;
    
    BMPGLKViewController *_glkvc;
    
    NSMutableArray      *_audioFrames;
    NSData              *_currentAudioFrame;
    NSUInteger          _currentAudioFramePos;
    
    NSTimer             *_timestampTimer;
    int64_t             _timestamp;
    
    NSTimer             *_seekTimer;
    int64_t             _nextSeekTime;
    int64_t             _settedSeekTime;
    
    BOOL                _firstSeek;

    BOOL _shouldStartPlayback;
}

@property (nonatomic, strong) BMPAudioManager *audioManager;

@end

@implementation BewardMediaPlayer

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (id)mediaPlayerWithParameters:(NSDictionary*)parameters
{
    return [[BewardMediaPlayer alloc] initWithParameters:parameters];
}

- (id)initWithParameters:(NSDictionary*)parameters
{
    return [self initWithParameters:parameters customAudioManager:nil];
}

+ (id)mediaPlayerWithParameters:(NSDictionary*)parameters customAudioManager:(BMPAudioManager *)audioManager
{
    return [[BewardMediaPlayer alloc] initWithParameters:parameters customAudioManager:audioManager];
}

- (id)initWithParameters:(NSDictionary *)parameters customAudioManager:(BMPAudioManager *)audioManager
{
    self = [super init];
    if (self) {
        
        _url = nil;
        _view = nil;
        _shouldStartPlayback = NO;
        
        _timestamp = 0;
        _timestampTimer = nil;
        
        _nextSeekTime = 0;
        _settedSeekTime = 0;
        
        _seekTimer = nil;
        
        _audioFrames = [[NSMutableArray alloc] init];
        
        _decoder = [[BMPMediaDecoder alloc] initWithURL:_url];
        _decoder.delegate = self;
        
        _state = BMPStateStopped;
        
        if (audioManager) {
            _audioManager = audioManager;
        }
        else {
            _audioManager = [[BMPAudioManager alloc] init];
        }
    }
    return self;
}

- (void)setUrl:(NSString*)url
{
    _url = url;
}

- (void)setView:(UIView *)view
{
    _view = view;
    [self setupPresentView];
}

- (void)setMuteAudio:(BOOL)muteAudio
{
    _muteAudio = muteAudio;
}

#pragma mark - public

- (void)prepare
{
    switch (_state) {
        case BMPStateStopped: {
            [self preparePlayback];
        }
        break;
            
        case BMPStateParsing: {
        }
            break;
            
        case BMPStateReady: {
        }
            
        case BMPStatePlaying: {
        }
            break;
            
        case BMPStatePaused: {
        }
            break;
            
        case BMPStateEnded: {
            
        }
            break;
            
        default:
            break;
    }
}

- (void)play
{
    if(!_url || !_view) {
        return;
    }
    
    switch (_state) {
        case BMPStateStopped: {
            [self preparePlayback];
            _shouldStartPlayback = YES;
        }
            break;
            
        case BMPStateParsing: {
            _shouldStartPlayback = YES;
        }
            break;
            
        case BMPStateReady: {
            _shouldStartPlayback = NO;
            [self startPlayback];
        }
            
        case BMPStatePlaying: {
            // playback is already started
        }
            break;
        
        case BMPStatePaused: {
            [self startPlayback];
        }
            break;
            
        case BMPStateError: {
            [self preparePlayback];
            _shouldStartPlayback = YES;
        }
            break;
            
        case BMPStateEnded: {
            
        }
            break;
            
        default:
            break;
    }
}

- (void)pause
{
    switch (_state) {
        case BMPStateStopped: {
            
        }
            break;
            
        case BMPStateParsing: {
            _shouldStartPlayback = NO;
        }
            break;
            
        case BMPStateReady: {
            _shouldStartPlayback = NO;
        }
            
        case BMPStatePlaying: {
            [self pausePlayback];
            _state = BMPStatePaused;
        }
            break;
            
        case BMPStatePaused: {
            //playback is already paused
        }
            break;
            
        case BMPStateError: {
            
        }
            break;
        
        case BMPStateEnded: {
                
        }
            break;
            
        default:
            break;
    }
}

- (void)stop
{
    switch (_state) {
        case BMPStateStopped: {
//            NSLog(@"already stoped");
        }
            break;
            
        case BMPStateParsing: {
//            NSLog(@"stop playback while parsing");
            [self stopPlayback];
        }
            break;
            
        case BMPStateReady: {
//            NSLog(@"stop playback while ready");

            [self stopPlayback];
        }
            break;
            
        case BMPStatePlaying: {
//            NSLog(@"stop playback while playing");
            [self stopPlayback];
        }
            break;
            
        case BMPStatePaused: {
//            NSLog(@"stop playback while paused");
            [self stopPlayback];
        }
            break;
            
        case BMPStateError: {
            
        }
            break;
            
        case BMPStateEnded: {
            
        }
            break;
            
        default:
            break;
    }
}

- (void)seekTo:(int64_t)microsecond
{
    if(_state == BMPStatePlaying || _state == BMPStatePaused) {
        if (_nextSeekTime != microsecond || _firstSeek) {
            _firstSeek = NO;
            _nextSeekTime = microsecond;
            if (_seekTimer) {
                return;
            }
            _seekTimer = [NSTimer scheduledTimerWithTimeInterval:BMP_SEEK_TIME
                                                          target:self
                                                        selector:@selector(makeSeek)
                                                        userInfo:nil
                                                         repeats:YES];
            [_decoder seekDecoderToTime:_nextSeekTime];
            _settedSeekTime = _nextSeekTime;
            
        }
    }
}

- (void)preparePlayback
{
    if(!_url || !_view) {
        return;
    }
    
    _state = BMPStateParsing;
    _decoder.url = _url;
    [_decoder setupMediaReceiver];
    [_decoder parseUrl];
}

- (void)startPlayback
{
    [_decoder startDecoding];
    [self playAudio:YES];
    [_glkvc playRenderingLoop];
    
    _shouldStartPlayback = NO;
    
    _state = BMPStatePlaying;
}

- (void)pausePlayback
{
    [_decoder pauseDecoding];
    [self playAudio:NO];
    [_glkvc pauseRenderingLoop];
    
    [_timestampTimer invalidate];
    _timestampTimer = nil;
    
    _state = BMPStatePaused;
}

- (void)stopPlayback
{
    self.url = nil;
    [_timestampTimer invalidate];
    _timestampTimer = nil;
    
    [_decoder stopDecoding];
    [_decoder releaseMediaReceiver];
    
    [self playAudio:NO];
    [_glkvc stopRenderingLoop];
    
    _state = BMPStateStopped;
}

- (void)setConnectTimeout:(NSUInteger)connectTimeout
{
    _decoder.connectTimeout = connectTimeout;
}

- (NSUInteger)connectTimeout
{
    return _decoder.connectTimeout;
}

- (void)setReadTimeout:(NSUInteger)readTimeout
{
    _decoder.readTimeout = readTimeout;
}

- (NSUInteger)readTimeout
{
    return _decoder.readTimeout;
}

- (void)setRtspTcpPreferred:(BOOL)rtspTcpPreferred
{
    _decoder.rtspTcpPreferred = rtspTcpPreferred;
}

- (BOOL)isRtspTcpPreferred
{
    return _decoder.rtspTcpPreferred;
}

- (void)setBufferLenghtTime:(NSUInteger)bufferLenghtTime
{
    _decoder.bufferLenghtTime = bufferLenghtTime;
}

- (NSUInteger)bufferLenghtTime
{
    return _decoder.bufferLenghtTime;
}

#pragma mark - BMPMediaDecoder delegate methods

- (void)mediaDecoderStateIsChanged:(BMPMediaDecoderState)newState
{
    switch (newState) {
        case BMPMediaDecoderStateReady: {
            
            if(_decoder.validAudio && _decoder.validVideo) {
                
                _state = BMPStateReady;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_PREPARED)}];
                
                if(_shouldStartPlayback) {
                    [self startPlayback];
                }
            }
        }
            break;
            
        case BMPMediaDecoderStateStoped: {
            
            _state = BMPStateStopped;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_RENDERING_STOP)}];

        }
            
            
        default:
            break;
    }
}

- (void)mediaDecoderSourcesStatusChanged:(BMPMediaDecoderMediaStatus)status
{
    switch (status) {
        case BMPMediaDecoderMediaStatusValidVideo: {
//                [self setupPresentView];
//            [_glkvc playRenderingLoop:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_VIDEO_PARAMS_CHANGED)}];
        }
            break;
            
        case BMPMediaDecoderMediaStatusValidAudio: {
            [self setupAudio];
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_AUDIO_PARAMS_CHANGED)}];
        }
            break;

        case BMPMediaDecoderMediaStatusPlayingVideo: {
            
            _timestampTimer = [NSTimer scheduledTimerWithTimeInterval:BMP_TIMESTAMP_TIME
                                                               target:self
                                                             selector:@selector(sendTimestamp)
                                                             userInfo:nil
                                                              repeats:YES];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_VIDEO_RENDERING_START)}];
        }
            break;
            
        case BMPMediaDecoderMediaStatusPlayingAudio: {
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_AUDIO_RENDERING_START)}];
        }
            break;
            
        case BMPMediaDecoderMediaStatusVideoEnded: {
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaInfo object:self userInfo:@{ BMPMediaInfo : @(MEDIA_INFO_RENDERING_STOP)}];
            [_timestampTimer invalidate];
            _timestampTimer = nil;
        }
            break;
            
        default:
            break;
    }
}

- (void)mediaDecoderError:(BMPMediaDecoderMediaError)error
{
    switch (error) {
        case BMPMediaDecoderMediaErrorConnect:
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaError object:self userInfo:@{ BMPMediaError : @(MEDIA_ERROR_CONNECT)}];
            break;
            
        case BMPMediaDecoderMediaErrorDisconnect:
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaError object:self userInfo:@{ BMPMediaError : @(MEDIA_ERROR_DISCONNECT)}];
            break;
            
        case BMPMediaDecoderMediaErrorSystem:
            [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaError object:self userInfo:@{ BMPMediaError : @(MEDIA_ERROR_SYSTEM)}];
            break;
            
        default:
            break;
    }
}

- (void)streamDurationReceive:(int64_t)duration
{
    self.duration = duration;

    [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaDuration object:self userInfo:@{ BMPMediaDuration : @(duration)}];
}

- (void)videoFrameReceive:(BMPVideoFrame*)videoFrame
{
    _timestamp = videoFrame.timestamp;
    _firstSeek = YES;
    
    if(_glkvc) {
        [_glkvc setFrame:videoFrame];
    }
}

- (void)audioFrameReceive:(BMPAudioFrame*)audioFrame
{
    @synchronized(_audioFrames) {
        [_audioFrames addObject:audioFrame];
    }
}

#pragma mark - timers selectors

- (void)sendTimestamp
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BMPMediaDuration object:self userInfo:@{ BMPMediaTimeStampChanged : @(_timestamp)}];
}

- (void)makeSeek
{
    [_seekTimer invalidate];
    _seekTimer = nil;
    if (_settedSeekTime == _nextSeekTime) {
        [_decoder seekDecoderToTime:_nextSeekTime];
        _settedSeekTime = _nextSeekTime;
    }
}

#pragma mark - private

- (void)setupPresentView
{
    if(!_view || !_decoder) {
        return;
    }
    
    if(_glkvc) {
        return;
    }

    _glkvc = [[BMPGLKViewController alloc] init];
    _glkvc.view.frame = self.view.bounds;
    [self.view addSubview:_glkvc.view];
    [_glkvc.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    UIView *subview = _glkvc.view;
    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[subview]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subview]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
}

- (void)setupAudio
{
    if (_decoder.validAudio) {
        
        _audioManager.playbackFormat = [self setupInputStreamFormatWithDecoder:_decoder];
        
        if(!_audioManager.outputBlock) {
        
            __weak typeof(self) weakself = self;
        
            _audioManager.outputBlock = ^(float *outData, UInt32 numFrames, UInt32 numChannels) {
                __strong typeof(weakself) strongSelf = weakself;
                [strongSelf audioCallbackFillData: outData numFrames:numFrames numChannels:numChannels];
            };
        }
        
    }
}

- (AudioStreamBasicDescription)setupInputStreamFormatWithDecoder:(BMPMediaDecoder *)decoder
{
    unsigned wordsize;
    BOOL isPlanar = NO;
    AudioStreamBasicDescription format;
    
    format.mSampleRate = decoder.sampleRate;
    format.mFormatID = kAudioFormatLinearPCM;
    format.mFormatFlags = kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked;
    format.mFramesPerPacket = 1;
    format.mChannelsPerFrame = (UInt32)decoder.channels;
    format.mBytesPerFrame = format.mBytesPerPacket = 0;
    format.mReserved = 0;
    
    switch (decoder.audioFrameFormat) {
        case BMPAudioFrameFormatS16:
            
            wordsize = 2;
            format.mFormatFlags |= kAudioFormatFlagIsSignedInteger;
            
            break;
            
        default:
            
            NSLog(@"Unknown sample format");
            
            break;
            
    }
    
    format.mBitsPerChannel = wordsize * 8;
    
    if (isPlanar) {
        format.mFormatFlags &= kAudioFormatFlagIsNonInterleaved;
        format.mBytesPerFrame = format.mBytesPerPacket = wordsize * format.mChannelsPerFrame;
    }
    else {
        format.mFormatFlags |= kAudioFormatFlagIsNonInterleaved;
        format.mBytesPerFrame = format.mBytesPerPacket = wordsize;
    }
    
    return format;
}

- (void)playAudio:(BOOL)on
{
    if (on && _decoder.validAudio) {
        
        [_audioManager startMode:BMPAudioManagerModePlayback];
        
    } else {
        
        [_audioManager stopMode:BMPAudioManagerModePlayback];
    }
}

- (void)audioCallbackFillData:(float*)outData
                    numFrames:(UInt32)numFrames
                  numChannels:(UInt32)numChannels
{
    @autoreleasepool {
        
        while (numFrames > 0) {
            
            if (!_currentAudioFrame) {
                
                @synchronized(_audioFrames) {
                    
                    NSUInteger count = _audioFrames.count;
                    
                    if (count > 0) {
                        
                        BMPAudioFrame *frame = _audioFrames[0];
                        
                        [_audioFrames removeObjectAtIndex:0];
                        
                        _currentAudioFramePos = 0;
                        _currentAudioFrame = frame.samples;
                    }
                }
            }
            
            if (_currentAudioFrame) {
                
                const void *bytes = (Byte *)_currentAudioFrame.bytes + _currentAudioFramePos;
                const NSUInteger bytesLeft = (_currentAudioFrame.length - _currentAudioFramePos);
                const NSUInteger frameSizeOf = numChannels * sizeof(float);
                const NSUInteger bytesToCopy = MIN(numFrames * frameSizeOf, bytesLeft);
                const NSUInteger framesToCopy = bytesToCopy / frameSizeOf;
                
                
                if(!_muteAudio) {
                    memcpy(outData, bytes, bytesToCopy);
                } else {
                    memset(outData, 0, bytesToCopy);
                }
                
                numFrames -= framesToCopy;
                outData += framesToCopy * numChannels;
                
                if (bytesToCopy < bytesLeft)
                    _currentAudioFramePos += bytesToCopy;
                else
                    _currentAudioFrame = nil;
                
            } else {
                
                memset(outData, 0, numFrames * numChannels * sizeof(float));
//                NSLog(@"silence audio");
                break;
            }
        }
    }

}

@end