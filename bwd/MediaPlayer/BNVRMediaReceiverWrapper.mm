//
//  BNVRMediaReceiverWrapper.m
//  BWRTSPPlayer
//
//  Created by adm on 03.02.16.
//  Copyright © 2016 Beward Ltd. All rights reserved.
//

#import "BNVRMediaReceiverWrapper.h"
#import <Accelerate/Accelerate.h>
#include "BMP/MediaReceiver.h"

using namespace std::placeholders;
using namespace MediaReceiver;


NSString *const MediaReceiverIsReleased = @"MediaReceiverIsReleased";


@interface BNVRMediaReceiverWrapper () {
    
#ifdef __cplusplus
    
    class MediaReceiverObserver {
        
    public:
        
        MediaReceiverObserver();
        virtual ~MediaReceiverObserver();
        
        void onVideoReceiveRGBA(std::shared_ptr<RGBAFrame> frame);
        void onAudioReceiveS16(std::shared_ptr<S16Frame> frame);
        void onAudioChange(uint32_t sampleRate, uint32_t channels, SampleFormat format, int64_t length);
        void onVideoChange(uint32_t width, uint32_t height, PixelFormat format, int64_t length);
        void onError(Error err);
        void onLog(LogLevel level, std::string msg);
        
        BNVRMediaReceiverWrapper *delegate;
    };
    
    std::shared_ptr<IMediaReceiver> _mediaReceiver;
    std::shared_ptr<MediaReceiverObserver> _mediaReceiverObserver;
    
#endif

    NSCondition *_condition;

}

@property (strong, nonatomic) NSSet *runLoopModes;
@property (strong, nonatomic) NSThread *mediaReceiverThread;

@property (nonatomic) NSUInteger frameWidth;
@property (nonatomic) NSUInteger frameHeight;
@property (nonatomic) NSUInteger sampleRate;
@property (nonatomic) NSUInteger channels;

@property (readwrite, nonatomic) int64_t lastAudioFrameTimestamp;
@property (readwrite, nonatomic) int64_t lastVideoFrameTimestamp;
@property (readwrite, nonatomic) int64_t startAudioFrameTimestamp;
@property (readwrite, nonatomic) int64_t startVideoFrameTimestamp;

@end

@implementation BNVRMediaReceiverWrapper

- (instancetype)init
{
    return [self initWithDelegate:nil];
}

- (instancetype)initWithDelegate:(id<MediaReceiverCallbackProtocol>)delegate
{
    self = [super init];
    if (self) {
        
        NSLog(@"MEDIARECEIVERWRAPPER created %@", self);
        
        self.runLoopModes = [NSSet setWithObject:NSRunLoopCommonModes];

        _condition = [[NSCondition alloc] init];
        
        _lastVideoFrameTimestamp = 0;
        _lastAudioFrameTimestamp = 0;
        
        self.mediaReceiverThread = nil;
        
        self.delegate = delegate;
        
        [self setupMediaReceiver];
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"MEDIARECEIVERWRAPPER dealocated %@", self);
}

#pragma mark - Thread methods

static void DoNothingRunLoopCallback(void *info)
{
}

- (void)mediaReceiverThreadEntryPoint:(id)__unused object
{
    @autoreleasepool {
        
        [[NSThread currentThread] setName:@"MediaReceiver"];

        CFRunLoopSourceContext context = {0};
        context.perform = DoNothingRunLoopCallback;
        
        CFRunLoopSourceRef source = CFRunLoopSourceCreate(NULL, 0, &context);
        CFRunLoopAddSource(CFRunLoopGetCurrent(), source, kCFRunLoopCommonModes);
        
        [_condition lock];
        [_condition signal];
        NSLog(@"THREAD has started %@", self);
        [_condition unlock];
        
        // Keep processing events until the runloop is stopped.
        CFRunLoopRun();
        
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), source, kCFRunLoopCommonModes);
        CFRelease(source);
        
        [_condition lock];
        [_condition signal];
        NSLog(@"THREAD has stopped %@", self);
        [_condition unlock];
    
    }
}

- (NSThread*)mediaReceiverThread
{
    if(!_mediaReceiverThread) {
        
        _mediaReceiverThread = [[NSThread alloc] initWithTarget:self selector:@selector(mediaReceiverThreadEntryPoint:) object:nil];
        
        [_condition lock];
        [_mediaReceiverThread start];
        [_condition wait];
        [_condition unlock];
    }
    
    return _mediaReceiverThread;
}



- (void)mediaReceiverThreadStop
{
    if (!_mediaReceiverThread) {
        return;
    }
    
    [_condition lock];
    [self performSelector:@selector(_stop)
                 onThread:self.mediaReceiverThread
               withObject:nil
            waitUntilDone:NO];
    
    [_condition wait];
    [_condition unlock];
    
    _mediaReceiverThread = nil;
    
//    NSLog(@"THREAD should have stopped %@", self);
}

- (void)_stop
{
    CFRunLoopStop(CFRunLoopGetCurrent());
}

#pragma mark - mediaReceiver initialization

- (void)_mediaReceiverInit
{    
    _mediaReceiver = create<FFmpegImpl>();
    
    _mediaReceiverObserver = std::make_shared<MediaReceiverObserver>();
    _mediaReceiverObserver->delegate = self;
    
    _mediaReceiver->setOnVideoReceive(createVideoReceiveCallback<RGBAFrame>(std::bind(&MediaReceiverObserver::onVideoReceiveRGBA, _mediaReceiverObserver, _1)));
    _mediaReceiver->setOnAudioReceive(createAudioReceiveCallback<S16Frame>(std::bind(&MediaReceiverObserver::onAudioReceiveS16, _mediaReceiverObserver, _1)));
    _mediaReceiver->setOnVideoChange(std::bind(&MediaReceiverObserver::onVideoChange, _mediaReceiverObserver, _1, _2, _3, _4));
    _mediaReceiver->setOnAudioChange(std::bind(&MediaReceiverObserver::onAudioChange, _mediaReceiverObserver, _1, _2, _3, _4));
    _mediaReceiver->setOnError(std::bind(&MediaReceiverObserver::onError, _mediaReceiverObserver, _1));
    _mediaReceiver->setOnLog(std::bind(&MediaReceiverObserver::onLog, _mediaReceiverObserver, _1, _2));
    
    NSLog(@"MEDIARECEIVER init %@", self);
}

- (void)_mediaReceiverDeInit
{
    _mediaReceiverObserver->delegate = nil;
    _mediaReceiverObserver = nullptr;
    _mediaReceiver = nullptr;
    
    NSLog(@"MEDIARECEIVER deinit %@", self);
    
    [self performSelectorOnMainThread:@selector(mediaReceiverThreadStop) withObject:nil waitUntilDone:NO];
}

#pragma mark - wrapper callbacks

#pragma mark video
- (void)receivedVideoData:(std::shared_ptr<RGBAFrame>)RGBAFrame
{
    @autoreleasepool {
        
        BMPVideoFrame *videoFrame;
        
        if(RGBAFrame == nullptr) {
            videoFrame = nil;
        } else {
            videoFrame = [self createRGBAVideoFrame:RGBAFrame];
        }
        
        //        NSLog(@"frame created");
        
//        [self performSelector:@selector(renderFrame:)
//                     onThread:self.mediaReceiverThread
//                   withObject:videoFrame
//                waitUntilDone:NO
//                        modes:[self.runLoopModes allObjects]];
        
        [self performSelectorOnMainThread:@selector(renderFrame:) withObject:videoFrame waitUntilDone:NO];
        
    }
}

- (void)renderFrame:(BMPVideoFrame*)frame
{
//    @autoreleasepool {
        [self.delegate receivedVideoData:frame];
//    }
}

#pragma mark audio
- (void)receivedAudioData:(std::shared_ptr<S16Frame>)S16Frame
{
    @autoreleasepool {
        
        BMPAudioFrame *audioFrame;
        
        if(S16Frame == nullptr) {
            audioFrame = nil;
        } else {
            audioFrame = [self createS16AudioFrame:S16Frame];
        }
        
//        [self performSelector:@selector(playAudioFrame:)
//                     onThread:self.mediaReceiverThread
//                   withObject:audioFrame
//                waitUntilDone:NO
//                        modes:[self.runLoopModes allObjects]];
        
        [self performSelectorOnMainThread:@selector(playAudioFrame:) withObject:audioFrame waitUntilDone:NO];
    }
}

- (void)playAudioFrame:(BMPAudioFrame*)frame
{
//    @autoreleasepool {
        [self.delegate receivedAudioData:frame];
//    }
}

#pragma mark info
- (void)videoStreamInfoChangedWidth:(size_t)width
                             height:(size_t)height
                             lenght:(int64_t)lenght
{
    self.frameWidth = width;
    self.frameHeight = height;
    
//    @autoreleasepool {
    
        dispatch_sync(dispatch_get_main_queue(), ^(){
            [self.delegate videoStreamInfoChangedWidth:width height:height lenght:lenght];
        });
//    }
}

- (void)audioStreamInfoChangedSampleRate:(size_t)sampleRate
                                channels:(size_t)channels
                                  lenght:(int64_t)lenght
{
    self.sampleRate = sampleRate;
    self.channels = channels;
 
//    @autoreleasepool {
    dispatch_sync(dispatch_get_main_queue(), ^(){
        [self.delegate audioStreamInfoChangedSampleRate:sampleRate channels:channels lenght:lenght];
    });
//    }
}

- (void)errorOccured:(Error)error
{
//    @autoreleasepool {
    dispatch_sync(dispatch_get_main_queue(), ^(){
        [self.delegate error:[self mediaReceiverErrorWith:error]
                 withMessage:[self mediaReceiverErrorToString:error]];
    });
//    }
}

- (void)receivedLogMessage:(std::string)message loglevel:(LogLevel)logLevel
{
//    @autoreleasepool {
    dispatch_sync(dispatch_get_main_queue(), ^(){
        [self.delegate log:[self mediaReceiverLogWith:logLevel]
               withMessage:[NSString stringWithCString:message.c_str() encoding:[NSString defaultCStringEncoding]]];
    });
//    }
}

#pragma mark - public & main thread

- (void)setupMediaReceiver
{
    [self performSelector:@selector(_mediaReceiverInit)
                 onThread:self.mediaReceiverThread
               withObject:nil
            waitUntilDone:YES
                    modes:[self.runLoopModes allObjects]];
}

- (void)releaseMediaReceiver
{
    _mediaReceiverObserver->delegate = nil;
    
    [self performSelector:@selector(_mediaReceiverDeInit)
                 onThread:self.mediaReceiverThread
               withObject:nil
            waitUntilDone:NO
                    modes:[self.runLoopModes allObjects]];
}

- (void)setDataSource:(NSDictionary*)values
{
    _mediaReceiver->setDataSource([self dictToMap:values]);
}

- (void)setOutParameters:(NSDictionary*)values
{
    _mediaReceiver->setOutParameters([self dictToMap:values]);
}

- (void)play
{
    _mediaReceiver->play();
}

- (void)pause
{
    _mediaReceiver->pause();
}

- (void)stop
{
    _mediaReceiver->stop();
}

- (void)seek:(int64_t)to
{
    _mediaReceiver->seek(to);
}

#pragma mark - data convertion

- (BMPVideoFrame*)createRGBAVideoFrame:(std::shared_ptr<RGBAFrame>)frame
{
//    NSTimeInterval startTimer = [[NSDate date] timeIntervalSince1970] * 1000;
    
    if(!frame->data.size()) {
        NSLog(@"empty video frame data");
        return nil;
    }
    
//    NSLog(@"statistic videoframe: (a-v)=%d us, (r-v)=%d us, total timestamp=%d us, total realtime=%d us",
//          _lastAudioFrameTimestamp - _lastVideoFrameTimestamp,
//          [NSDate date],
//          );
//    NSLog(@"onVideoReceive, frame->data.size() = %d,    timestamp=%d", frame);
    
    BMPVideoFrameRGBA *rgbFrame = [[BMPVideoFrameRGBA alloc] init];
    
    rgbFrame.linesize = self.frameWidth;
    
    rgbFrame.rgba = [NSData dataWithBytes:frame->data.data()
                                   length:frame->data.size()*4];
    
    rgbFrame.width = self.frameWidth;
    rgbFrame.height = self.frameHeight;
    
    rgbFrame.timestamp = frame->timestamp();
    rgbFrame.duration = frame->duration();
    
//    NSTimeInterval doneTimer = [[NSDate date] timeIntervalSince1970] * 1000;
//    NSLog(@"frame creation %f", doneTimer - startTimer);

    
    return (BMPVideoFrame*)rgbFrame;
}

- (BMPAudioFrame*)createS16AudioFrame:(std::shared_ptr<S16Frame>)frame
{
    if(frame->data.size() <= 0) {
        NSLog(@"empty audio frame data");
        return nil;
    }
    
    void *rawData = (void *)frame->data.data();
    
    const NSUInteger numElements = frame->data.size() * _channels;
    NSMutableData *audioData = [[NSMutableData alloc] initWithLength:numElements * sizeof(float)];
    
    float scale = 1.0 / (float)INT16_MAX ;
    vDSP_vflt16((SInt16 *)rawData, 1, (float*)  audioData.mutableBytes, 1, numElements);
    vDSP_vsmul((float*)audioData.mutableBytes, 1, &scale, (float*)audioData.mutableBytes, 1, numElements);
    
    BMPAudioFrame *s16Frame = [[BMPAudioFrame alloc] init];
    s16Frame.samples = audioData;
    s16Frame.timestamp = frame->timestamp();
    s16Frame.duration = frame->duration();
    
    return s16Frame;
}

- (NSString*)mediaReceiverErrorToString:(MediaReceiver::Error)error
{
    switch (error) {
        case MediaReceiver::Error::E_INVALID_OPERATION: return @"INVALID_OPERATION";
        case MediaReceiver::Error::E_INVALID_URL: return @"INVALID_URL";
        case MediaReceiver::Error::E_STREAM_INFO_FIND_FAIL: return @"STREAM_INFO_FIND_FAIL";
        case MediaReceiver::Error::E_ALLOCATE_FAIL: return @"ALLOCATE_FAIL";
        case MediaReceiver::Error::E_DECODEING_FAIL: return @"DECODING_FAIL";
        case MediaReceiver::Error::E_CODEC_FIND_FAIL: return @"CODEC_FIND_FAIL";
        case MediaReceiver::Error::E_CODEC_OPEN_FAIL: return @"CODEC_OPEN_FAIL";
        case MediaReceiver::Error::E_INVALID_PARAMRTER: return @"INVALID_PARAMRTER";
        case MediaReceiver::Error::E_CONVERTING_FAIL: return @"CONVERTING_FAIL";
        case MediaReceiver::Error::E_CONNECTION_TERMINATED: return @"CONNECTION_TERMINATED";
        default: return nil;
    }
}

- (MediaReceiverError)mediaReceiverErrorWith:(MediaReceiver::Error)error
{
    switch (error) {
        case MediaReceiver::Error::E_INVALID_OPERATION: return MediaReceiverErrorInvalidOperation;
        case MediaReceiver::Error::E_INVALID_URL: return MediaReceiverErrorInvalidURL;
        case MediaReceiver::Error::E_STREAM_INFO_FIND_FAIL: return MediaReceiverErrorStreamInfoFindFail;
        case MediaReceiver::Error::E_ALLOCATE_FAIL: return MediaReceiverErrorAllocateFail;
        case MediaReceiver::Error::E_DECODEING_FAIL: return MediaReceiverErrorDecodingFail;
        case MediaReceiver::Error::E_CODEC_FIND_FAIL: return MediaReceiverErrorCodecFindFail;
        case MediaReceiver::Error::E_CODEC_OPEN_FAIL: return MediaReceiverErrorCodecOpenFail;
        case MediaReceiver::Error::E_INVALID_PARAMRTER: return MediaReceiverErrorInvalidParameter;
        case MediaReceiver::Error::E_CONVERTING_FAIL: return MediaReceiverErrorConvertingFail;
        case MediaReceiver::Error::E_CONNECTION_TERMINATED: return MediaReceiverErrorConnectionTerminated;
    }
}

- (MediaReceiverLogLevel)mediaReceiverLogWith:(MediaReceiver::LogLevel)logLevel
{
    switch (logLevel) {
        case MediaReceiver::LogLevel::LL_FATAL : return MediaReceiverLogLevelFatal;
        case MediaReceiver::LogLevel::LL_ERROR: return MediaReceiverLogLevelError;
        case MediaReceiver::LogLevel::LL_WARNING: return MediaReceiverLogLevelWarning;
        case MediaReceiver::LogLevel::LL_INFO: return MediaReceiverLogLevelInfo;
        case MediaReceiver::LogLevel::LL_DEBUG: return MediaReceiverLogLevelDebug;
    }
}

- (std::unordered_map<std::string, any>)dictToMap:(NSDictionary*)dict
{
    std::unordered_map<std::string, any> values;
    
    NSArray *keyArray = [dict allKeys];
    
    for (NSString* key in keyArray) {

        id value = [dict valueForKey:key];

        if([key isEqualToString:@"url"]) {
            NSString *string = (NSString*)value;
            values.insert(std::make_pair([key UTF8String], std::string([string UTF8String])));
        } else if([key isEqualToString:@"rtsp_tcp_preferred"]) {
            NSNumber* numberValue = (NSNumber*)value;
            bool boolValue = [numberValue boolValue];
            values.insert(std::make_pair([key UTF8String],boolValue));
        } else {
            NSNumber* numberValue = (NSNumber*)value;
            uint32_t intValue = [numberValue intValue];
            values.insert(std::make_pair([key UTF8String], intValue));
        }
    }
    
    return values;
}


- (NSData*)copyFrameData:(const uint8_t *)src
                linesize:(int)linesize
                   width:(int)width
                  height:(int)height
{
    width = MIN(linesize, width);
    NSMutableData *md = [NSMutableData dataWithLength: width * height];
    Byte *dst = (Byte *)md.mutableBytes;
    for (NSUInteger i = 0; i < height; ++i) {
        memcpy(dst, src, width);
        dst += width;
        src += linesize;
    }
    return md;
}

@end

#pragma mark - observer callbacks

#ifdef __cplusplus

MediaReceiverObserver::MediaReceiverObserver()
{
    this->delegate = nil;
    NSLog(@"OBSERVER created %p", this);
}

MediaReceiverObserver::~MediaReceiverObserver()
{
    this->delegate = nil;
    NSLog(@"OBSERVER released %p", this);
}

void MediaReceiverObserver::onVideoReceiveRGBA(std::shared_ptr<RGBAFrame> frame)
{
    [this->delegate receivedVideoData:frame];
}

void MediaReceiverObserver::onAudioReceiveS16(std::shared_ptr<S16Frame> frame)
{
    [this->delegate receivedAudioData:frame];
}

void MediaReceiverObserver::onAudioChange(uint32_t sampleRate, uint32_t channels, SampleFormat format, int64_t length)
{
    [this->delegate audioStreamInfoChangedSampleRate:sampleRate
                                            channels:channels
                                              lenght:length];
}

void MediaReceiverObserver::onVideoChange(uint32_t width, uint32_t height, PixelFormat format, int64_t length)
{
    [this->delegate videoStreamInfoChangedWidth:width
                                         height:height
                                         lenght:length];
}

void MediaReceiverObserver::onError(Error err)
{
    [this->delegate errorOccured:err];
}

void MediaReceiverObserver::onLog(LogLevel level, std::string msg)
{
    [this->delegate receivedLogMessage:msg loglevel:level];
}

#endif