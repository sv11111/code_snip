//
//  BewardMediaPlayer.h
//  BWRTSPPlayer
//
//  Created by adm on 25.09.15.
//  Copyright © 2015 Beward Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BMPMediaDecoder.h"

#define BMP_TIMESTAMP_TIME 1
#define BMP_SEEK_TIME 0.5

#define BMP_FRAME_ERROR_LIMIT 100
#define BMP_FRAME_ERROR_RESET_TIME 1

typedef NS_ENUM(NSInteger, BMPState)
{
    BMPStateStopped,        //< Player has stopped
    BMPStateParsing,        //< Stream is opening
    BMPStateReady,          //< Stream is buffering
    BMPStatePlaying,        //< Stream is playing
    BMPStatePaused,         //< Stream is paused
    BMPStateEnded,          //< Stream has ended
    BMPStateError           //< Player has generated an error
};

/* Notification Messages */
extern NSString *const BMPMediaError;
extern NSString *const BMPMediaInfo;
extern NSString *const BMPMediaDuration;
extern NSString *const BMPMediaTimeStampChanged;

//Ошибки:
typedef NS_ENUM(NSInteger, MEDIA_ERROR)
{
    MEDIA_ERROR_CONNECT,    //- не возможно подключиться к источнику
    MEDIA_ERROR_DISCONNECT, //- обрыв соединения с источником медиаданных в процессе воспроизведения
    MEDIA_ERROR_SYSTEM      //- системная ошибка произошедшая в работе оболочки или ядра, в следствии которой воспроизведение видео/аудио невозможно
};

//Информационные:
typedef NS_ENUM(NSInteger, MEDIA_INFO)
{
    MEDIA_INFO_VIDEO_RENDERING_START,   //- отображение первого видео кадра на экране (поверхности)
    MEDIA_INFO_AUDIO_RENDERING_START,   //- воспроизведение первого аудио сэмпла на громкоговорителе устройства
    MEDIA_INFO_PREPARED,                //- видео/аудио готово для воспроизведения, (можно вызывать метод start())
    MEDIA_INFO_RENDERING_STOP,          //- прекращено воспроизведения видео/аудио
    MEDIA_INFO_VIDEO_PARAMS_CHANGED,    //- были изменения видео параметров потока в процессе воспроизведения
    MEDIA_INFO_AUDIO_PARAMS_CHANGED     //- были изменения аудио параметров потока в процессе воспроизведения
};

@class BMPAudioManager;

@interface BewardMediaPlayer : NSObject <BMPMediaDecoderDelegate>

+ (id)mediaPlayerWithParameters: (NSDictionary *) parameters;
+ (id)mediaPlayerWithParameters:(NSDictionary*)parameters customAudioManager:(BMPAudioManager *)audioManager;

@property (readonly, nonatomic) BMPState state;

@property (strong, nonatomic) UIView *view;
@property (strong, nonatomic) NSString *url;
@property (nonatomic) BOOL muteAudio;

@property (nonatomic) int64_t duration;

@property (nonatomic) NSUInteger connectTimeout;
@property (nonatomic) NSUInteger readTimeout;
@property (nonatomic, getter = isRtspTcpPreferred) BOOL rtspTcpPreferred;
@property (nonatomic) NSUInteger bufferLenghtTime;

- (void)prepare;
- (void)play;
- (void)pause;
- (void)stop;
- (void)seekTo:(int64_t)microsecond;

@end