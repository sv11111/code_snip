//
//  BWDApiManager.m
//  camDrive
//
//  Created by adm on 27.11.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import "BWDApiManager.h"
#import "BWDStringDefines.h"
#import "AFNetworking.h"
#import "NSDictionary+NSNull.h"
#import "NSString+Escaping.h"
#import "BWDUtils.h"
#import "NSDate+Utilities.h"

@implementation BWDApiManager {
    AFNetworkReachabilityStatus _curStatus;
    AFHTTPSessionManager *_manager;

}

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BWDServerName]];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        _curStatus = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
        
        __weak typeof(self) wself = self;
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            if (status == AFNetworkReachabilityStatusNotReachable && _curStatus != AFNetworkReachabilityStatusNotReachable) {
                [wself connectionLost];
            }
            if (status != AFNetworkReachabilityStatusNotReachable && _curStatus == AFNetworkReachabilityStatusNotReachable) {
                [wself connectionEstablished];
            }
            
            _curStatus = status;
        }];
    }
    return self;
}


#pragma mark- Public Methods

- (void)signInWithLogin:(NSString *)login pass:(NSString *)pass completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    NSString *urlStr = [NSString stringWithFormat:@"%@%@", BWDServerName, login, pass];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if (completion) completion(result);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (failure) failure(error);
    } isLogin:YES];
}

- (void)logOutwithCompletion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    if ([self isAuthorized]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ud_profileCredentials];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}


- (void)getCamerasListWithArchiveDate:(NSDate *)archiveDate completion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    NSString *urlStr = [NSString stringWithFormat:@"%@cameras", BWDServerName];
    if (archiveDate) {
        urlStr = [NSString stringWithFormat:@"%@?archive=%@", urlStr, [archiveDate serverDateString]];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if (completion) completion(result);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (failure) failure(error);
    } isLogin:NO];
}


- (BOOL)isAuthorized {
    return [[NSUserDefaults standardUserDefaults] objectForKey:ud_profileCredentials] != nil;
}


#pragma mark- Private

- (void)checkServerAuthorizationWithCompletion:(void(^)(id result))completion failure:(void(^)(NSError *error))failure {
    NSString *urlStr = [NSString stringWithFormat:@"%@", BWDServerName];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self loadRequest:request completion:^(NSDictionary *result) {
        NSLog(@"%@", result);
        if (completion) completion(result);
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
        if (failure) failure(error);
    } isLogin:NO];
}




- (void)needSignInProcessingWithFailure:(void(^)(NSError *error))failure {
    if ([self isAuthorized]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ud_profileCredentials];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:notif_NeedSignIn object:nil];
    if (failure){
        failure([[NSError alloc] initWithDomain:domain_CamDrive code:err_NeedSignIn userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:@"Authorization requied", @"errDesc", nil]]);
    }


- (NSURLRequest *)getRequestWithCookies:(NSURLRequest *)request isLogin:(BOOL)isLogin failure:(void(^)(NSError *error))failure {
    NSMutableURLRequest *headersReq = [request mutableCopy];
    [headersReq setValue:@"no-cache, no-store, must-revalidate"  forHTTPHeaderField:@"Cache-Control"];
    [headersReq setValue:[[NSLocale preferredLanguages] objectAtIndex:0]  forHTTPHeaderField:@"Accept-Language"];
    [headersReq setValue:[BWDUtils getUserAgentString]  forHTTPHeaderField:@"User-Agent"];
    if (!isLogin) {
        NSDictionary *creds = [[NSUserDefaults standardUserDefaults] objectForKey:ud_profileCredentials];
        NSString *session = [creds safeObjectForKey:@"session"];
        NSString *rememberCode = [creds safeObjectForKey:@"remember_code"];
        NSString *identity = [creds safeObjectForKey:@"identity"];
        if (!session || !rememberCode || !identity) {
            [self needSignInProcessingWithFailure:failure];
            return nil;
        }
        [headersReq setValue:[NSString stringWithFormat:@"identity=%@; remember_code=%@; session=%@", identity, rememberCode, session]  forHTTPHeaderField:@"Cookie"];
    }
    return headersReq;
}

- (void)loadRequest:(NSURLRequest *)request
         completion:(void(^)(id result))completion
            failure:(void(^)(NSError *error))failure isLogin:(BOOL)isLogin {
    if (![self isAuthorized] && !isLogin) {
        [self needSignInProcessingWithFailure:failure];
        return;
    }
    if (_curStatus != AFNetworkReachabilityStatusNotReachable) {
        NSURLRequest *headersReq = [self getRequestWithCookies:request isLogin:isLogin failure:failure];
        if (!headersReq) {
            return;
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:headersReq];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSArray* cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[operation.response allHeaderFields] forURL:[request URL]];
            NSMutableDictionary* cookieDictionary = [[NSMutableDictionary alloc] init];
            for (NSHTTPCookie* cookie in cookies) {
                [cookieDictionary setValue:(NSDictionary *)cookie.properties[@"Value"] forKey:cookie.name];
            }
            if ([cookieDictionary count] > 0) {
                [self saveProfileCredentials:cookieDictionary];
            }
            
            NSArray* autoCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[request URL]];
            for (NSHTTPCookie* cookie in autoCookies) {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            }
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                if ([(NSDictionary *)responseObject safeObjectForKey:@"status"] != nil) {
                    int statusValue = [(NSNumber *)[(NSDictionary *)responseObject safeObjectForKey:@"status"] integerValue];
                    switch (statusValue) {
                        //авторизован
                        case 1:
                            if (completion) {
                                completion(responseObject);
                            }
                            if (isLogin) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:notif_Authorized object:nil];
                            }
                            break;
                        //ошибка доступа - кидаем на страницу логина выбрасываем ошибку
                        case 2:
                            [self needSignInProcessingWithFailure:failure];
                            break;
                        //неизвестная ошибка - не продолжаем запрос, уходим на ошибку
                        default:
                            if (isLogin) {
                                [self needSignInProcessingWithFailure:nil];
                            }
                            if (failure) {
                                failure([[NSError alloc] initWithDomain:domain_CamDrive code:err_ServerError userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:[(NSDictionary *)responseObject safeObjectForKey:@"message"], @"errDesc", nil]]);
                            }
                            break;
                    }
                }
                else {
                    NSLog(@"can't find status in response");
                }

            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
        
        [operation start];
    }
    else {
        if (failure) {
            NSError *err = [NSError errorWithDomain:@"" code:-1004 userInfo:nil];
            failure(err);
        }
    }
}

- (void)saveProfileCredentials:(NSDictionary *)dict {
     NSMutableDictionary *resultDict = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:ud_profileCredentials]];
     NSArray *allKeys = [dict allKeys];
     for (NSString *key in allKeys) {
        if ([dict safeObjectForKey:key] != nil) {
            [resultDict setObject:[dict safeObjectForKey:key] forKey:key];
        }
     }
     [[NSUserDefaults standardUserDefaults] setObject:[resultDict copy] forKey:ud_profileCredentials];
     [[NSUserDefaults standardUserDefaults] synchronize];
 }

- (NSData *)httpBodyForParamsDictionary:(NSDictionary *)paramDictionary
{
    NSMutableArray *paramArray = [NSMutableArray array];
    [paramDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", key, [obj stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [paramArray addObject:param];
    }];
    
    NSString *string = [paramArray componentsJoinedByString:@"&"];
    
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}


- (void)appDidBecomeActive {
    [self connectionEstablished];
}

- (void)connectionEstablished {
    //if (_curStatus != AFNetworkReachabilityStatusNotReachable) {
    
    //}
}

- (void)connectionLost {
    
}


@end
