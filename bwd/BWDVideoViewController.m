//
//  videoViewController.m
//  camDrive
//
//  Created by adm on 09.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import "BWDVideoViewController.h"
#import "BWDCameraCell.h"
#import "BewardMediaPlayer.h"
#import <QuartzCore/QuartzCore.h>
#import "BWDTimeline.h"
#import "BWDTimeToaster.h"
#import "BWDCalendarVC.h"
#import "BWDUtils.h"
#import "NSDate+Utilities.h"
#import "BWDRecordedPeriodsCache.h"
#import "BWDPlayerVC.h"
#import "UIView+Appearance.h"
#import "UIColor+CustomColors.m"

//#import <CKRefreshControl/CKRefreshControl.h>

@interface BWDVideoViewController () <UIGestureRecognizerDelegate, BWDTimeLineDataSource, BWDTimelineDelegate, UIScrollViewDelegate, BWDPlayerDelegate>

//---------- landscape fs iphone/ipad constraints; portrait fs ipad constraints (mode1)----------
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintCollectionMode1;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewsToShowMode1;

//---------- portrait nfs iphone/ipad constraints (mode2)----------
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintCollectionMode2;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewsToShowMode2;

//---------- landscape nfs ipad constraints (mode3)----------
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintCollectionMode3;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewsToShowMode3;

//-------------------------------

//should be 16:9 in portrait nfs iphone;  should be with camera aspect ratio in portrait nfs ipad; should be turned off in modes 1,3
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playerAspectRatioConstraint;

@property (weak, nonatomic) IBOutlet UILabel *cameraTitle;

@property (weak, nonatomic) IBOutlet UICollectionView *videosCollection;
@property (weak, nonatomic) IBOutlet UIView *detailBar;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@property (weak, nonatomic) IBOutlet BWDTimeline *timeline;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *dateBarItem;
@property (weak, nonatomic) IBOutlet UIButton *onlineBtn;
@end

@implementation BWDVideoViewController {
    int _currentMode;
    UIInterfaceOrientation _curInterfaceOrientation;
    NSArray *_camerasArray;
    
    BWDPlayerVC *_playerVC;
    
    NSString *_lastCameraId;
    BWDCameraListItem *_choosenCamera;

    NSTimer *_refreshCamerasListTimer;
    
    BOOL _skipEmptyTimelineDays;
    
    BOOL _isCollectionLoading;
    
    NSString *_lastUpdateCollectionDate;
    UIRefreshControl *_refreshControl;
    
    NSDate *_maxDate;
    NSDate *_minDate;
    
    BOOL _shouldUpdatePointer;
    
}

static float const kRefreshCamerasListDuration = 120.0;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shouldUpdatePointer = YES;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _playerVC = (BWDPlayerVC *)[sb instantiateViewControllerWithIdentifier:@"storyIdPlayerVC"];
    [_playerView addSubviewOverlaySuperView:_playerVC.view];
    _playerVC.onlineMode = YES;
    [self player:_playerVC willChangeOnlineState:YES];
    _playerVC.delegate = self;
    
    _videosCollection.alwaysBounceVertical = YES;
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(refreshCamerasList)
             forControlEvents:UIControlEventValueChanged];
    [_videosCollection addSubview:_refreshControl];
    
    _maxDate = [[NSDate date] dateByAddingDays:1];
    _minDate = [_maxDate dateBySubtractingDays:archiveLivePeriod];
    
    [[BWDRecordedPeriodsCache sharedManager] clearCache];
    
    _timeline.dataSource = self;
    _timeline.delegate = self;
    
    _timeline.showDate = [BWDUtils dateOnlyFromDate:[NSDate date]];
    [self timeline:_timeline didChangeDate:_timeline.showDate];
    _timeline.maxDate = _maxDate;
    _timeline.minDate = _minDate;
    _skipEmptyTimelineDays = NO;
    
    _timeline.backgroundColor = [UIColor BWDGrayColor];
    _timeline.contentColor = [UIColor BWDBlueColor];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateArchiveDate:) name:notif_DateSelected object:nil];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(applicationWillResignActive:)
                   name:UIApplicationWillResignActiveNotification object:nil];
    [center addObserver:self selector:@selector(applicationDidBecomeActive:)
                   name:UIApplicationDidBecomeActiveNotification object:nil];
    [center addObserver:self selector:@selector(applicationDidEnterBackground:)
                   name:UIApplicationDidEnterBackgroundNotification object:nil];
    [center addObserver:self selector:@selector(applicationWillTerminate:)
                   name:UIApplicationWillTerminateNotification object:nil];
    [center addObserver:self selector:@selector(applicationNeedSignIn:)
                   name:notif_NeedSignIn object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewWillAppear:(BOOL)animated
{
    _curInterfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    [self updateConstraintsAndVisibility];
    _refreshCamerasListTimer = [NSTimer scheduledTimerWithTimeInterval:kRefreshCamerasListDuration target:self selector:@selector(refreshCamerasList) userInfo:nil repeats:YES];
    [self refreshDataWithHUD:YES];
    _lastCameraId = [[NSUserDefaults standardUserDefaults] objectForKey:ud_lastCameraId];
}

- (void)refreshCamerasList {
    [self refreshDataWithHUD:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_refreshCamerasListTimer invalidate];
    _refreshCamerasListTimer = nil;
    [[NSUserDefaults standardUserDefaults] setObject:_lastCameraId forKey:ud_lastCameraId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)refreshDataWithHUD:(BOOL)showHUD {
    if (_isCollectionLoading) {
        return;
    }
    _isCollectionLoading = YES;
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.removeFromSuperViewOnHide = YES;
    HUD.dimBackground = YES;
    if (showHUD) {
        [self.view addSubview:HUD];
        [HUD show:YES];
    }
    [[BWDApiManager sharedInstance] getCamerasListWithArchiveDate:nil completion:^(id result) {
        NSObject *data = [(NSDictionary *)[(NSDictionary *)result safeObjectForKey:@"data"] safeObjectForKey:@"cameras"];
        if (data && [data isKindOfClass:[NSArray class]]) {
            NSMutableArray *sourceArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *item in (NSArray *)data ) {
                BWDCameraListItem *model = [BWDCameraListItem itemWithDictionary:item];
                BOOL isExists = NO;
                for (BWDCameraListItem *oldModel in _camerasArray) {
                    if ([model cameraIsEqualToModel:oldModel]) {
                        [oldModel updateWithDictionary:item];
                        [sourceArray addObject:oldModel];
                        isExists = YES;
                        break;
                    }
                }
                if (!isExists) {
                    [sourceArray addObject:model];
                }
            }
            _camerasArray = [NSArray arrayWithArray:sourceArray];
            if (_choosenCamera) {
                _choosenCamera.choosen = YES;
            }
            
            [self updateCollection:nil];
        }
        if (showHUD) {
            [self updateArchiveExistsIndicatorWithDate:_timeline.showDate];
            [HUD hide:YES];
        }
        _isCollectionLoading = NO;
        [_refreshControl endRefreshing];
        
    } failure:^(NSError *error) {
        _isCollectionLoading = NO;
        [_refreshControl endRefreshing];
        if (showHUD) {
            [HUD hide:YES];
        }
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:error.userInfo[@"errDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
    }];
}


- (void)updateCollection:(NSNotification *)notification {
    [_videosCollection reloadData];
    if (_camerasArray.count == 0 || _choosenCamera) {
        return;
    }
    
    //choose last selected or the first camera in list
    NSInteger row = 0;
    if (_lastCameraId && [_lastCameraId length] > 0) {
        for (BWDCameraListItem *item in _camerasArray) {
            if ([item.channelId isEqualToString:_lastCameraId]) {
                row = [_camerasArray indexOfObject:item];
                [self collectionView:_videosCollection didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:row inSection:1]];
                break;
            }
        }
    }
    else {
        _lastCameraId = ((BWDCameraListItem *)_camerasArray[0]).channelId;
        [self collectionView:_videosCollection didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:row inSection:1]];
    }
    
    
}


- (void)updateConstraintsAndVisibility {
    if (_playerVC.isFullScreen && (IS_IPAD || UIInterfaceOrientationIsLandscape(_curInterfaceOrientation))) {
        _currentMode = 1;
        [self installConstraintsArray:_constraintCollectionMode1];
        [self showViewsArray:_viewsToShowMode1];
        return;
    }
    if (UIInterfaceOrientationIsPortrait(_curInterfaceOrientation)) {
        _currentMode = 2;
        [self installConstraintsArray:_constraintCollectionMode2];
        [self showViewsArray:_viewsToShowMode2];
        return;
    }
    if (IS_IPAD && UIInterfaceOrientationIsLandscape(_curInterfaceOrientation)) {
        _currentMode = 3;
        [self installConstraintsArray:_constraintCollectionMode3];
        [self showViewsArray:_viewsToShowMode3];
        return;
    }
}

- (void)installConstraintsArray:(NSArray *)constraintsArray {
    
    [NSLayoutConstraint activateConstraints:constraintsArray];
    NSArray *arrayOfCollections = @[_constraintCollectionMode1, _constraintCollectionMode2, _constraintCollectionMode3];
    for (NSArray *arr in arrayOfCollections) {
        if (arr != constraintsArray) {
            [NSLayoutConstraint deactivateConstraints:arr];
        }
    }
    [NSLayoutConstraint activateConstraints:constraintsArray];
}

- (void)showViewsArray:(NSArray *)viewsArray {
    NSArray *arrayOfCollections = @[_viewsToShowMode2, _viewsToShowMode3];
    for (NSArray *arr in arrayOfCollections) {
        for (UIView *view in arr) {
            view.hidden = !([viewsArray containsObject:view]);
        }
    }
    
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (_curInterfaceOrientation != toInterfaceOrientation) {
        _curInterfaceOrientation = toInterfaceOrientation;
        if (!IS_IPAD) {
            if ((UIInterfaceOrientationIsPortrait(_curInterfaceOrientation) && _playerVC.isFullScreen) ||
                (UIInterfaceOrientationIsLandscape(_curInterfaceOrientation) && !_playerVC.isFullScreen)) {
                _playerVC.isFullScreen = !_playerVC.isFullScreen;
                [self player:_playerVC willChangeFullScreenState:_playerVC.isFullScreen];
            } else {
                [self updateConstraintsAndVisibility];
            }
        }
        else {
            [self updateConstraintsAndVisibility];
        }
        
    }
}

- (void)updateArchiveDate:(NSNotification *)notification {
    NSDate *selectedDate = [notification.userInfo safeObjectForKey:dict_DateSelected];
    _timeline.showDate = [BWDUtils dateOnlyFromDate:selectedDate];
    _dateBarItem.title = [selectedDate mediumDateString];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [_videosCollection.collectionViewLayout invalidateLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Player VC delegate

- (NSArray <UIView *> *)viewsTohideWithPlayerControls:(BWDPlayerVC *)playerVC {
    return _playerVC.isFullScreen ? [NSArray arrayWithObject:_timeline] : nil;
}


- (void)player:(BWDPlayerVC *)player willChangeFullScreenState:(BOOL)isFullScreen {
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    [UIApplication sharedApplication].statusBarHidden = isFullScreen;
    if (!isFullScreen) {
        _timeline.alpha = 1;
    }
    if (!IS_IPAD && isFullScreen && UIInterfaceOrientationIsPortrait(_curInterfaceOrientation)) {
        [[UIDevice currentDevice] setValue:
         [NSNumber numberWithInteger: UIInterfaceOrientationLandscapeLeft]
                                    forKey:@"orientation"];
        return;
    }
    if (!IS_IPAD && !isFullScreen && UIInterfaceOrientationIsLandscape(_curInterfaceOrientation)) {
        [[UIDevice currentDevice] setValue:
         [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                    forKey:@"orientation"];
        return;
    }
    //костыль для лайату коллекции в лэндскейпе айпада
    _videosCollection.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self updateConstraintsAndVisibility];
        [self.view layoutSubviews];
    } completion:^(BOOL finished) {
        [_videosCollection.collectionViewLayout invalidateLayout];
        _videosCollection.alpha = 1;
    }];
}

- (void)playerCalendarButtonPressed:(BWDPlayerVC *)playerVC {
    [self performSegueWithIdentifier:@"segueIdShowCalendar" sender:self];
}

- (void)player:(BWDPlayerVC *)player willChangeOnlineState:(BOOL)isOnline {
    _onlineBtn.imageView.image = [UIImage imageNamed:isOnline ? @"online_selected_bl" : @"online_bl"];
    [_onlineBtn setImage:[UIImage imageNamed:isOnline ? @"online_selected_wh" : @"online_wh"] forState:UIControlStateNormal];
    if (isOnline) {
        _timeline.currentPointerDate = nil;
    }
}

- (void)playerZoomInButtonPressed:(BWDPlayerVC *)playerVC {
    [self zoomInToolbarBtnTap:playerVC];
}

- (void)playerZoomOutButtonPressed:(BWDPlayerVC *)playerVC {
    [self zoomOutToolbarBtnTap:playerVC];
}

- (void)noTimeForCameraToPlay:(BWDPlayerVC *)playerVC {
    _timeline.currentPointerDate = nil;
}

- (void)player:(BWDPlayerVC *)player didChangePlayingTime:(NSDate *)time {
    if (_shouldUpdatePointer) {
        _timeline.currentPointerDate = time;
    }
}


- (IBAction)showLeftVC:(id)sender {
    [kMainViewController showLeftViewAnimated:YES completionHandler:nil];
}


#pragma mark Collection View DataSource
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BWDCameraCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cameraItemCellId" forIndexPath:indexPath];
    BWDCameraListItem *model = _camerasArray[indexPath.row];
    cell.model = model;
    model.delegate = cell;
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _camerasArray.count;
}

#pragma mark Collection View Delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [collectionView bounds];
    CGFloat screenWidth = screenRect.size.width;
    float divider;
    if (_currentMode == 2) {
        divider = (IS_IPAD) ? 3 : 2;
    } else {
        divider = 1;
    }
    float spacesWidth = (divider + 1) * [self collectionView:_videosCollection layout:_videosCollection.collectionViewLayout minimumInteritemSpacingForSectionAtIndex:0];
    float cellWidth = (screenWidth - spacesWidth) / divider;
    CGSize size = CGSizeMake(cellWidth, cellWidth / 4 * 3);
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 8, 8, 8);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BWDCameraListItem *model = _camerasArray[indexPath.row];
    
    if (model.cameraState == BWDCameraStateUnavailable || model.choosen) {
        return;
    }
    for (BWDCameraListItem *item in _camerasArray) {
        if (item.choosen && item != model) {
            item.choosen = NO;
        }
    }
    model.choosen = YES;
    _choosenCamera = model;
    _cameraTitle.text = [NSString stringWithString:_choosenCamera.cameraName];
    [_timeline reloadData];
    _playerVC.camera = _choosenCamera;
    _lastCameraId = _choosenCamera.channelId;

}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y < -30) {
        if (_refreshControl) {
            [_refreshControl beginRefreshing];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [self refreshDataWithHUD:NO];
            });
        }
        
    }
}

#pragma mark toolBar Buttons operations
- (IBAction)onlineBtnTap:(id)sender {
    _playerVC.onlineMode = !_playerVC.onlineMode;
    [self player:_playerVC willChangeOnlineState:_playerVC.onlineMode];
}

- (IBAction)zoomInToolbarBtnTap:(id)sender {
    [_timeline zoomIn];
}

- (IBAction)zoomOutToolbarBtnTap:(id)sender {
    [_timeline zoomOut];
}


#pragma mark App States Notifications

- (void)applicationWillResignActive:(NSNotification *)aNotification
{
    [_refreshCamerasListTimer invalidate];
    _refreshCamerasListTimer = nil;

    [[NSUserDefaults standardUserDefaults] setObject:_lastCameraId forKey:ud_lastCameraId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    [_refreshCamerasListTimer invalidate];
    _refreshCamerasListTimer = nil;

    [[NSUserDefaults standardUserDefaults] setObject:_lastCameraId forKey:ud_lastCameraId];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)applicationDidBecomeActive:(NSNotification *)notification
{
    [_refreshCamerasListTimer invalidate];
    _refreshCamerasListTimer = nil;
    _refreshCamerasListTimer = [NSTimer scheduledTimerWithTimeInterval:kRefreshCamerasListDuration target:self selector:@selector(refreshCamerasList) userInfo:nil repeats:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [_refreshCamerasListTimer invalidate];
    _refreshCamerasListTimer = nil;
    [[NSUserDefaults standardUserDefaults] setObject:_lastCameraId forKey:ud_lastCameraId];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)applicationNeedSignIn:(NSNotification *)notification {
    _choosenCamera = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ud_lastCameraId];
}

#pragma mark timeline dataSource
- (BWDRecordedPeriodsCacheItem *)timeline:(BWDTimeline *)timeline dataForItemAtPosition:(BWDTimelineItemPosition)position {
    BWDRecordedPeriodsCacheItem *ret = nil;
    NSDate *date = [_timeline.showDate copy];
    NSDate *nextDate = [date copy];
    int direction = 0;
    
    if (position != BWDTimelineItemPositionCurrDay) {
        if (_skipEmptyTimelineDays) {
            direction = (position == BWDTimelineItemPositionPrevDay) ? -1 : 1;
            nextDate = (position == BWDTimelineItemPositionPrevDay) ? [BWDUtils yesterdayFromDate:date] : [BWDUtils tomorrowFromDate:date];
        }
        else {
            date = (position == BWDTimelineItemPositionPrevDay) ? [BWDUtils yesterdayFromDate:date] : [BWDUtils tomorrowFromDate:date];
        }
    }
    
    if (!_choosenCamera) {
        ret = [[BWDRecordedPeriodsCacheItem alloc] initWithDate:((_skipEmptyTimelineDays) ? nextDate : date) dataState:BWDTimelineDataStateNoData recordedPeriods:nil channelId:nil];
        return ret;
    }
    
    if ([[BWDRecordedPeriodsCache sharedManager] existsDataForChannelId:_choosenCamera.channelId date:date direction:direction]) {
        ret = [[BWDRecordedPeriodsCache sharedManager] getDataForChannelId:_choosenCamera.channelId date:date direction:direction];
    }
    else {
        ret = [[BWDRecordedPeriodsCacheItem alloc] initWithDate:((_skipEmptyTimelineDays) ? nextDate : date)
                                                      dataState:BWDTimelineDataStateLoading recordedPeriods:nil
                                                      channelId:_choosenCamera.channelId];
        [[BWDRecordedPeriodsCache sharedManager] loadDataForChannelId:_choosenCamera.channelId date:date direction:direction completion:^(BWDRecordedPeriodsCacheItem *result) {
            [_timeline reloadData];
        }];
    }
    return ret;
}


#pragma mark timeline delegate
- (void)timeline:(BWDTimeline *)timeline didSetArchiveDate:(NSDate *)date {
    if (_playerVC.onlineMode) {
        [self player:_playerVC willChangeOnlineState:NO];
    }
    _shouldUpdatePointer = YES;
    _playerVC.archiveTime = date;
    
    
}

- (void)timeline:(BWDTimeline *)timeline didScrollArchiveDate:(NSDate *)date {
    [_playerVC showToasterWithTime:date];
    [_playerVC showPlayerElements];
    _shouldUpdatePointer = NO;
}

- (void)timeline:(BWDTimeline *)timeline didChangeDate:(NSDate *)date {
    [_playerVC showPlayerElements];
    [_playerVC showToasterWithDate:date];
    _dateBarItem.title = [date mediumDateString];
    [self updateArchiveExistsIndicatorWithDate:date];
    
}

- (void)timeline:(BWDTimeline *)timeline didScrollDate:(NSDate *)date {
    [_playerVC showPlayerElements];
    [_playerVC showToasterWithDate:date];
}

- (void)timeline:(BWDTimeline *)timeline didScrollDay:(NSDate *)day {
    [_playerVC showPlayerElements];
}


- (void)updateArchiveExistsIndicatorWithDate:(NSDate *)date {
    __block NSDate *bDate = [date copy];
    [[BWDApiManager sharedInstance] getCamerasListWithArchiveDate:bDate completion:^(id result) {
        if ([_timeline.showDate isEqualToDate:bDate]) {
            NSObject *data = [(NSDictionary *)[(NSDictionary *)result safeObjectForKey:@"data"] safeObjectForKey:@"cameras"];
            if (data && [data isKindOfClass:[NSArray class]]) {
                for (NSDictionary *item in (NSArray *)data ) {
                    NSString *cameraId = [item safeObjectForKey:@"camera_channel_id"];
                    if (cameraId) {
                        for (BWDCameraListItem *cameraItem in _camerasArray) {
                            if ([cameraItem.channelId isEqualToString:cameraId] && [item safeObjectForKey:@"archive"]) {
                                cameraItem.archiveForDateExists = [[item safeObjectForKey:@"archive"] boolValue];
                                break;
                            }
                        }
                    }
                }
                
            }
        }
    } failure:^(NSError *error) { }];
}

#pragma mark navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueIdShowCalendar"] ) {
        BWDCalendarVC *vc = (BWDCalendarVC *)((UINavigationController *)segue.destinationViewController).topViewController;
        if (vc) {
            vc.selectEmptyDates = !_skipEmptyTimelineDays;
            vc.channelId = _choosenCamera != nil ? _choosenCamera.channelId : nil;
            vc.cameraName = _choosenCamera != nil ? _choosenCamera.cameraName : nil;
        }
    }
}

- (BOOL)prefersStatusBarHidden {
    return _playerVC.isFullScreen;
}


@end
