//
//  TSTViewController.m
//  Kopilka
//
//  Created by Семен Власов on 01.07.14.
//  Copyright (c) 2014 SVV. All rights reserved.
//

#import "TSTAuthorizationVC.h"
#import "TSTStringDefines.h"
#import "TSTUtils.h"
#import "NSString+MD5.h"
#import "MBProgressHUD.h"
#import <AFHTTPRequestOperation.h>
#import "TBXML.h"
#import "NSString+Escaping.h"
#import "TSTSharedDef.h"
#import "TSTCustomTextField.h"
#import "ALToastView.h"

@interface TSTAuthorizationVC () <UITextFieldDelegate>

@property (nonatomic) IBOutlet UIButton *connectButt;
@property (nonatomic) IBOutlet TSTCustomTextField *phoneTF;
@property (nonatomic) IBOutlet UITextField *passwordTF;

@end

@implementation TSTAuthorizationVC {
    UITapGestureRecognizer *_tapGesture;
    BOOL _isAlreadyShown;
    NSOperationQueue *_bgQueue;
}


static NSString *const kCurVersionKey = @"curAppVersion";
static NSString *const kCurVersion = @"1.3.3";
- (void)viewDidLoad {
    [super viewDidLoad];
    [_connectButt addTarget:self action:@selector(connectButtTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognize:)];
    _tapGesture.enabled = NO;
    [self.view addGestureRecognizer:_tapGesture];
    
    [TSTSharedDef sharedDef].sBaseUrl = @"https://95.172.129.26:7079/MobyService.svc";
//#warning дебаговый сервак
    //[TSTSharedDef sharedDef].sBaseUrl = @"https://95.172.129.26:9393/MobyService.svc";
    _bgQueue = [NSOperationQueue new];
    _bgQueue.name = @"Auth BG  Queue";
    _bgQueue.maxConcurrentOperationCount = 1;
    
    [_phoneTF setTextInset:UIEdgeInsetsMake(0, 20.5f, 0, 0)];
    NSString *curVersion = [[NSUserDefaults standardUserDefaults] objectForKey:kCurVersionKey];
    if (!curVersion || curVersion.length == 0 || ![curVersion isEqualToString:kCurVersion]) {
        [TSTUtils clearSavedAuthData];
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:ud_CardIDKey]) {
        [self performSegueWithIdentifier:seg_PushRevealFromAuth sender:self];
    }
}

static const NSInteger kSmallScreenOffset = 40;
static const NSInteger kWhiteViewTag = 9999;
- (void)viewWillAppear:(BOOL)animated {
    if (!_isAlreadyShown) {
        if (IS_3_5_DISPLAY) {
            for (UIView *v in self.view.subviews) {
                v.frame = (CGRect){v.frame.origin.x, v.frame.origin.y - kSmallScreenOffset, v.frame.size};
            }
        }
        UIView *whiteView = [UIView new];
        whiteView.backgroundColor = [UIColor whiteColor];
        whiteView.frame = (CGRect){0, 0, self.view.frame.size.width, 40};
        whiteView.tag = kWhiteViewTag;
        [self.view addSubview:whiteView];
        
        _isAlreadyShown = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    NSString *curVersion = [[NSUserDefaults standardUserDefaults] objectForKey:kCurVersionKey];
    if (!curVersion || curVersion.length == 0 || ![curVersion isEqualToString:kCurVersion]) {
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.dimBackground = YES;
        HUD.labelText = @"Обновление данных...";
        [self.view addSubview:HUD];
        [HUD show:YES];
        [_bgQueue addOperationWithBlock:^{
            [TSTUtils clearUserDefaults];
            [[NSUserDefaults standardUserDefaults] setObject:kCurVersion forKey:kCurVersionKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [HUD removeFromSuperview];
                [HUD hide:YES];
            }];
        }];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    _phoneTF.text = @"";
    _passwordTF.text = @"";
}

- (void)connectButtTouchUp:(UIButton *)sender {
    [self tryAuth];
}

- (void)tryAuth {
    if (_phoneTF.text.length > 0 && _passwordTF.text.length > 0){
        [self getCardId];
    }
    else {
        if (_phoneTF.text.length == 0 && _passwordTF.text.length != 0) {
            [TSTUtils shakeAnimation:@[_phoneTF]];
        }
        else if (_passwordTF.text.length == 0 && _phoneTF.text.length != 0) {
            [TSTUtils shakeAnimation:@[_passwordTF]];
        }
        else {
            [TSTUtils shakeAnimation:@[_phoneTF, _passwordTF]];
        }
    }
}

- (void)tapGestureRecognize:(UITapGestureRecognizer *)recognizer {
    if (_phoneTF.editing) [_phoneTF resignFirstResponder];
    else [_passwordTF resignFirstResponder];
}

- (IBAction)regButtTouchUp:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://kopilkaclub.ru/register/account/mobile"]];
}

- (void)getCardId {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.dimBackground = YES;
    [self.navigationController.visibleViewController.view addSubview:HUD];
    [HUD show:YES];
     
    NSString *ident = [[NSUserDefaults standardUserDefaults] objectForKey:ud_DeviceTokenKey];
    NSURL *baseURL = [NSURL URLWithString:[[TSTSharedDef sharedDef] sBaseUrl]];
    NSString *pass = _passwordTF.text;
    NSString *signStr = [NSString stringWithFormat:@"%@%@%@%@%@", _phoneTF.text, pass, ksh_DefCD, ident, ksh_DefKey];
    NSString *soapBody = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                          "<soap:Header/>"
                          "<soap:Body>"
                            "<tem:GetCardID>"
                                "<tem:phone>%@</tem:phone>"
                                "<tem:pass>%@</tem:pass>"
                                "<tem:cd>%@</tem:cd>"
                                "<tem:ident>%@</tem:ident>"
                                "<tem:sign>%@</tem:sign>"
                            "</tem:GetCardID>"
                          "</soap:Body>"
                "</soap:Envelope>", _phoneTF.text, pass, ksh_DefCD, ident, [[signStr MD5String] lowercaseString]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:baseURL];
	[request setValue:@"http://tempuri.org/IMobyService/GetCardID" forHTTPHeaderField:@"SOAPAction"];
	[request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:[NSString stringWithFormat:@"%zu", (unsigned long)[soapBody length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    request.timeoutInterval = 20;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setValidatesDomainName:NO];
    securityPolicy.allowInvalidCertificates = YES;
    operation.securityPolicy = securityPolicy;
    __weak TSTAuthorizationVC *this = self;
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [HUD hide:YES];

        NSError *err = nil;
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSRange r = [str rangeOfString:@"<GetCardIDResult>"];
        if (r.length > 0) {
            str = [str substringFromIndex:r.length + r.location];
            r = [str rangeOfString:@"</GetCardIDResult>"];
            if (r.length > 0) {
                str = [[str substringToIndex:r.location] xmlSimpleUnescapeString];
                NSLog(@"%@", str);
                TBXML *tbx = [TBXML newTBXMLWithXMLString:str error:&err];
                __block BOOL isSuccess = NO;
                if (tbx.rootXMLElement) {
                    [TBXML iterateAttributesOfElement:tbx.rootXMLElement withBlock:^(TBXMLAttribute *attribute, NSString *attributeName, NSString *attributeValue) {
                        if ([attributeName isEqualToString:@"res_code"]) {
                            if ([attributeValue isEqualToString:@"88000"])
                                isSuccess = YES;
                            else if ([attributeValue isEqualToString:@"80001"]) {
                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка авторизации"
                                                                             message:@"Извините, номер телефона или пароль указаны неверно."
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil];
                                [av show];
                            }
                            else {
                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка авторизации"
                                                                             message:@""
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil];
                                [av show];
                            }
                        }
                    }];
                    if (isSuccess)
                        [this traverseElement:tbx.rootXMLElement];
                }
                
                if (isSuccess) {
                    //_phoneTF.text = @"9831423744";
                    [[NSUserDefaults standardUserDefaults] setObject:_phoneTF.text forKey:ud_UserPhoneKey];
                    [[NSUserDefaults standardUserDefaults] setObject:_passwordTF.text forKey:ud_UserPassKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self performSegueWithIdentifier:seg_PushRevealFromAuth sender:self];
                }
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HUD hide:YES];
        [ALToastView toastInView:self.view withText:@"Ошибка подключения к серверу"];
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                     message:error.localizedDescription
//                                                    delegate:nil
//                                           cancelButtonTitle:@"OK"
//                                           otherButtonTitles:nil];
//        [av show];
    }];
    
    [operation start];
}

- (BOOL)traverseElement:(TBXMLElement *)element {
    do {
        if (element->firstChild) {
            if ([self traverseElement:element->firstChild])
                return YES;
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"root"]) {
            [TBXML iterateAttributesOfElement:element withBlock:^(TBXMLAttribute *attribute, NSString *attributeName, NSString *attributeValue) {
                if ([attributeName isEqualToString:@"id_card"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:attributeValue forKey:ud_CardIDKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }];
        }
        else if ([[TBXML elementName:element] isEqualToString:@"fio"]) {
            if (element->text) {
                NSString *str = [NSString stringWithCString:element->text encoding:NSUTF8StringEncoding];
                [[NSUserDefaults standardUserDefaults] setObject:str forKey:ud_UserFIOKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        else if ([[TBXML elementName:element] isEqualToString:@"rest"]) {
            if (element->text) {
                NSString *str = [NSString stringWithCString:element->text encoding:NSUTF8StringEncoding];
                [[NSUserDefaults standardUserDefaults] setObject:str forKey:ud_UserBalansKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    } while ((element = element->nextSibling));
    
    return NO;
}

#pragma mark UITextField delegate

static const NSInteger k4inchYOffset = 40;
static const NSInteger k3inchYOffset = 75;
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _tapGesture.enabled = YES;
    NSInteger yOffset = IS_3_5_DISPLAY? k3inchYOffset: k4inchYOffset;
    if (textField == _passwordTF) {
        [UIView animateWithDuration:0.3f animations:^{
            for (UIView *view in self.view.subviews) {
                if (view.tag != kWhiteViewTag)
                    view.frame = (CGRect){view.frame.origin.x, view.frame.origin.y - yOffset, view.frame.size};
            }
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _tapGesture.enabled = NO;
    if (textField == _passwordTF) {
        NSInteger yOffset = IS_3_5_DISPLAY? k3inchYOffset: k4inchYOffset;
        [UIView animateWithDuration:0.3f animations:^{
            for (UIView *view in self.view.subviews) {
                if (view.tag != kWhiteViewTag)
                    view.frame = (CGRect){view.frame.origin.x, view.frame.origin.y + yOffset, view.frame.size};
            }
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _phoneTF) {
        [_passwordTF becomeFirstResponder];
    }
    else if (textField == _passwordTF) {
        [_passwordTF resignFirstResponder];
        [self tryAuth];
    }
    return NO;
}

@end
