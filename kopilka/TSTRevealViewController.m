//
//  RHRevealViewController.m
//  Kopilka
//
//  Created by Семен Власов on 09.07.14.
//  Copyright (c) 2014 SVV. All rights reserved.
//

#import "TSTRevealViewController.h"

static const NSUInteger kFVShadowRadius = 10;

@interface TSTRevealViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong)IBOutlet UIView  *frontView;
@property (nonatomic, strong)IBOutlet UIView  *backView;

@end

@implementation TSTRevealViewController {
    CGFloat _backViewShownWidth;
    CGFloat _maxBackViewShownOverdraw;
    CGFloat _previousPanOffset;
    CGFloat _backViewPresentationWidth;
    CGFloat _triggerWidthToShowBackView;
    CGFloat _triggerWidthToHideBackView;
    CGFloat _flickVelocity;
    
    NSTimeInterval _toggleAnimationDuration;
    UIPanGestureRecognizer *_panGRecognizer;
    BOOL _isPangGestureProcessing;
}

- (id)init {
    self = [super init];
    if (self != nil) {
        [self setDefaultsConfig];
    }
    return self;
}

static TSTRevealViewController *sharedReveal = nil;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        sharedReveal = self;
        [self setDefaultsConfig];
    }
    return self;
}

+ (TSTRevealViewController *)sharedInstance {
    if (!sharedReveal) {
        sharedReveal = [[TSTRevealViewController alloc] init];
    }
    
    return sharedReveal;
}

- (void)setDefaultsConfig {
    _toggleAnimationDuration = 0.3f;
    _backViewShownWidth = 220.0f;
    if (IS_IPAD)
        _backViewShownWidth = 260.0f;
	_maxBackViewShownOverdraw = 10.0f;
    _backViewPresentationWidth = 220.0f;
    if (IS_IPAD)
        _backViewPresentationWidth = 320.0f;
	_triggerWidthToShowBackView = 145.0f;
	_triggerWidthToHideBackView = 160.0f;
	_flickVelocity = 1300.0f;
    _isPangGestureProcessing = YES;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
    //self.automaticallyAdjustsScrollViewInsets = NO;
    
    CGRect r = CGRectMake(0, 0, 15, 1024);
	UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:r];
	_frontView.layer.masksToBounds = NO;
	_frontView.layer.shadowColor = [UIColor blackColor].CGColor;
	_frontView.layer.shadowOffset = CGSizeMake(-5.0f, 0.0f);
	_frontView.layer.shadowOpacity = 1.0f;
	_frontView.layer.shadowRadius = kFVShadowRadius;
	_frontView.layer.shadowPath = shadowPath.CGPath;
	
	_previousPanOffset = 0.0f;
	_curFrontViewPos = FrontViewPositionLeft;
    
    _backViewShownWidth = _backView.frame.size.width;
    
    [self setPanGestureForViewAppearanceControl];
}

- (void)setPanGestureForViewAppearanceControl {
    _panGRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognize:)];
    _panGRecognizer.delegate = self;
    [self.view addGestureRecognizer:_panGRecognizer];
}

- (void)showToggle{
    if (_curFrontViewPos == FrontViewPositionLeft) {
		[self frontViewAnimation:_toggleAnimationDuration isBackHiding:NO];
		_curFrontViewPos = FrontViewPositionRight;
	}
	else if (self.curFrontViewPos == FrontViewPositionRight) {
		[self frontViewAnimation:_toggleAnimationDuration isBackHiding:YES];
        _curFrontViewPos = FrontViewPositionLeft;
	}
}

- (void)frontViewAnimation:(NSTimeInterval)duration isBackHiding:(BOOL)isBackHiding {
	[UIView animateWithDuration:duration delay:0.0f options:
     UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^ {
         if (isBackHiding)
             _frontView.frame = CGRectMake(0.0f, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
         else
             _frontView.frame = CGRectMake(_backViewShownWidth, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
     }
        completion:^(BOOL finished) {
     }];
}


- (void)frontViewAnimationForRightState:(NSTimeInterval)duration {
    [UIView animateWithDuration:duration delay:0.0f options:
     UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^ {
         _frontView.frame = CGRectMake(_backViewShownWidth - 10, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
     }
                     completion:^(BOOL finished) {
         [UIView animateWithDuration:duration delay:0.0f options:
          UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                          animations:^ {
              _frontView.frame = CGRectMake(_backViewShownWidth, 0.0f, _frontView.frame.size.width, _frontView.frame.size.height);
          }
                          completion:^(BOOL finished) {
              
          }];
     }];
}

- (void)panGestureRecognize:(UIPanGestureRecognizer *)recognizer {
	switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan:
            [self processGestureStateBegan:recognizer];
            break;
		case UIGestureRecognizerStateChanged:
            [self processGestureStateChanged:recognizer];
			break;
			
		case UIGestureRecognizerStateEnded:
			[self processGestureStateEnded:recognizer];
			break;
			
		default:
			break;
	}
}

- (void)processGestureStateBegan:(UIPanGestureRecognizer *)recognizer {
    CGPoint pos = CGPointZero;
    if (recognizer != nil) {
        if (recognizer.numberOfTouches > 0) {
            @try {
                pos = [recognizer locationOfTouch:0 inView:_frontView];
            }
            @catch (NSException *exception) {
            }
        }
        
        if (pos.x > 70)
            _isPangGestureProcessing = NO;
        else
            _isPangGestureProcessing = YES;
    }
}

- (void)processGestureStateChanged:(UIPanGestureRecognizer *)recognizer {
    if (!_isPangGestureProcessing || recognizer == nil)
        return;
    
    if (self.curFrontViewPos == FrontViewPositionLeft) {
		if ([recognizer translationInView:self.view].x < 0.0f) {
			_frontView.frame = (CGRect){0, 0, _frontView.frame.size};
		}
		else {
			float offset = [self calculateOffsetForTranslationInView:[recognizer translationInView:self.view].x];
			_frontView.frame = (CGRect){offset, 0, _frontView.frame.size};
		}
	}
	else {
		if ([recognizer translationInView:self.view].x > 0.0f) {
			float offset = [self calculateOffsetForTranslationInView:([recognizer translationInView:self.view].x + _backViewShownWidth)];
			_frontView.frame = (CGRect){offset, 0, _frontView.frame.size};
		}
		else if ([recognizer translationInView:self.view].x > -_backViewShownWidth) {
			_frontView.frame = (CGRect){[recognizer translationInView:self.view].x + _backViewShownWidth, 0, _frontView.frame.size};
		}
		else {
			_frontView.frame = (CGRect){0, 0, _frontView.frame.size};
		}
	}
}

- (void)processGestureStateEnded:(UIPanGestureRecognizer *)recognizer {
    if (!_isPangGestureProcessing || recognizer == nil)
        return;
    
	if (fabs([recognizer velocityInView:self.view].x) > _flickVelocity) {
		if ([recognizer velocityInView:self.view].x > 0.0f) {
			[self frontViewAnimation:_toggleAnimationDuration isBackHiding:NO];
		}
		else {
            [self frontViewAnimation:_toggleAnimationDuration isBackHiding:YES];
		}
	}
	else {
		float dynamicTriggerLevel = (_curFrontViewPos == FrontViewPositionLeft) ? _triggerWidthToShowBackView : _triggerWidthToHideBackView;
		
		if (_frontView.frame.origin.x >= dynamicTriggerLevel && _frontView.frame.origin.x != _backViewShownWidth) {
            if (_frontView.frame.origin.x < _backViewShownWidth)
                [self frontViewAnimation:_toggleAnimationDuration isBackHiding:NO];
            else
                [self frontViewAnimationForRightState:_toggleAnimationDuration];
		}
		else {
            [self frontViewAnimation:_toggleAnimationDuration isBackHiding:YES];
		}
	}

	if (_frontView.frame.origin.x <= 0.0f) {
		_curFrontViewPos = FrontViewPositionLeft;
	}
	else {
		_curFrontViewPos = FrontViewPositionRight;
	}
}

- (CGFloat)calculateOffsetForTranslationInView:(CGFloat)x {
	CGFloat result;
	if (x <= _backViewShownWidth) {
		result = x;
	}
	else if (x <= _backViewShownWidth + (M_PI * _maxBackViewShownOverdraw / 2.0f)) {
		result = _maxBackViewShownOverdraw * sin((x - _backViewShownWidth) / _maxBackViewShownOverdraw) + _backViewShownWidth;
	}
	else {
		result = _backViewShownWidth + _maxBackViewShownOverdraw;
	}
	
	return result;
}

- (void)openBackMenu {
    [self showToggle];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    //[[NSNotificationCenter defaultCenter] postNotificationName:kNotifOrientationChanged object:@(toInterfaceOrientation)];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (_curFrontViewPos == FrontViewPositionRight)
        return NO;
    return YES;
}

@end
