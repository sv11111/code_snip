//
//  TSTScannerVC.m
//  Kopilka
//
//  Created by Семен Власов on 16.07.14.
//  Copyright (c) 2014 SVV. All rights reserved.
//

#import "TSTScannerVC.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"
#import "TSTScannerResultVC.h"
#import "KGModal.h"

#import "TSTPromoModel.h"
#import "TSTPromoObject.h"
#import "TSTBarcodeModel.h"
#import "TSTAppDelegate.h"

@interface TSTScannerVC () <AVCaptureMetadataOutputObjectsDelegate, UIAlertViewDelegate>

@end

@implementation TSTScannerVC {
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    self.title = @" ";
}

- (void)viewDidAppear:(BOOL)animated {
    UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithFrame:(CGRect){self.view.frame.size.width/2 - 15, self.view.frame.size.height/2 - 15,  30, 30}];
    [self.view addSubview:av];
    [av startAnimating];
    __weak TSTScannerVC *this = self;
    [av removeFromSuperview];
    [this initScanner];
}

- (void)initScanner {
    _highlightView = [[UIView alloc] init];
    _highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
    _highlightView.layer.borderWidth = 3;
    [self.view addSubview:_highlightView];
    
    _label = [[UILabel alloc] init];
    _label.frame = CGRectMake(0, self.view.bounds.size.height - 40, self.view.bounds.size.width, 40);
    _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _label.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
    _label.textColor = [UIColor whiteColor];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.text = @"";
    [self.view addSubview:_label];
    
    UIButton *butt = [UIButton buttonWithType:UIButtonTypeSystem];
    butt.frame = _label.frame;
    [butt addTarget:self action:@selector(showFoundedItem) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:butt];
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.view.bounds;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:_prevLayer];
    
    [_session startRunning];
    
    [self.view bringSubviewToFront:_highlightView];
    [self.view bringSubviewToFront:_label];
}

- (void)showFoundedItem {
    NSLog(@"%@", _label.text);
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type]) {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (detectionString != nil) {
            _label.text = detectionString;
            [self tryToFindGoodWithCode:detectionString];
            break;
        }
        else
            _label.text = @" ";
    }
    
    _highlightView.frame = highlightViewRect;
}

- (void)tryToFindGoodWithCode:(NSString *)code {
    [_session stopRunning];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.dimBackground = YES;
    HUD.labelText = @"Идет поиск товара...";
    [self.navigationController.visibleViewController.view addSubview:HUD];
    [HUD show:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            TSTPromoObject *curPromoObj = nil;
            TSTPromoObject *futurePromoObj = nil;
            for (TSTPromoObject *obj in _curModel.objects) {
                for (TSTBarcodeModel *barcode in obj.barcodes) {
                    if ([barcode.barcode isEqualToString:code]) {
                        curPromoObj = obj;
                        break;
                    }
                }
                if (curPromoObj != nil)
                    break;
            }
            for (TSTPromoObject *obj in _futureModel.objects) {
                for (TSTBarcodeModel *barcode in obj.barcodes) {
                    if ([barcode.barcode isEqualToString:code]) {
                        futurePromoObj = obj;
                        break;
                    }
                }
                if (futurePromoObj != nil)
                    break;
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [HUD hide:YES];
                if (!curPromoObj && !futurePromoObj) {
                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Товар не участвует в акции"
                                                                delegate:self
                                                       cancelButtonTitle:@"Закрыть"
                                                       otherButtonTitles:nil];
                    [av show];
                    _label.text = @" ";
                }
                else {
                    TSTScannerResultVC *scannerRes = [[TSTScannerResultVC alloc] init];
                    scannerRes.futurePromoObj = futurePromoObj;
                    scannerRes.curPromoObj = curPromoObj;
                    [self.navigationController pushViewController:scannerRes animated:YES];
                }
            });
        });
    });
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    [_session startRunning];
}

@end
