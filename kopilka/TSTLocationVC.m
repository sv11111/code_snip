//
//  TSTLocation.m
//  Kopilka
//
//  Created by Семен Власов on 15.07.14.
//  Copyright (c) 2014 SVV. All rights reserved.
//

#import "TSTLocationVC.h"
#import "TSTAbstractClassifyerModel.h"
#import "TSTClassifyerVC.h"
#import "KGModal.h"
#import <GoogleMaps/GoogleMaps.h>
#import "TSTSharedDef.h"
#import "TSTStringDefines.h"
#import "TSTUtils.h"
#import "NSString+MD5.h"
#import <AFHTTPRequestOperation.h>
#import "TBXML.h"
#import "NSString+Escaping.h"
#import "ALToastView.h"
#import "TSTLocationGroup.h"
#import "TSTLocationShop.h"

/*
 12)Возвращает все подразделения с категориями.
 string GetDepartments(int cardId, string sign);
 
 cardId – идентификатор карты,
 sign – подпись.
 
 Возвращает:
 <root>
 <groups>
 <group cat_id="1" cat_name="Магазины" />
 <group cat_id="2" cat_name="Инструменты и крепеж" />
 <group cat_id="3" cat_name="Кафе и Рестораны" />
 <group cat_id="4" cat_name="Кинокомплексы" />
 <group cat_id="5" cat_name="Платежная система" />
 <group cat_id="6" cat_name="ТВ Реклама" />
 <group cat_id="7" cat_name="Товары для дома" />
 <group cat_id="8" cat_name="Ювелирные украшения" />
 <group cat_id="9" cat_name="Аптеки" />
 </groups>
 <shops>
 <shop shop_id="255" shop_name="Командор №11" category_id="2" />
 <shop shop_id="255" shop_name="Командор №11" category_id="1" />
 <shop shop_id="253" shop_name="Командор №10" category_id="1" />
 <shop shop_id="254" shop_name="Командор №9" category_id="1" />
 </shops>
 <shops>
 */

@interface TSTLocationVC () <CLLocationManagerDelegate>

@end

@implementation TSTLocationVC {
    GMSMapView *mapView_;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSMutableArray *_filterArr;
    NSMutableArray *_groupArr;
    NSMutableArray *_markersArr;
    NSMutableDictionary *_groupDict;
    KGModal *_activeModal;
    BOOL _isAlreadyInit;
    BOOL _isMapUpdated;
    NSString *_packageNumber;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    _filterArr = [[NSMutableArray alloc] init];
    _groupArr = [[NSMutableArray alloc] init];
    _markersArr = [[NSMutableArray alloc] init];
    _groupDict = [[NSMutableDictionary alloc] init];
    _packageNumber = @"0";
    [self loadCashmodel];
    
#ifdef __IPHONE_8_0
//    NSUInteger code = [CLLocationManager authorizationStatus];
//    if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
        // choose one request according to your business.
//        if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
    if (IS_OS_8_OR_LATER)
        [locationManager requestAlwaysAuthorization];
//        } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
//            [locationManager  requestWhenInUseAuthorization];
//        } else {
//            NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
//        }
 //   }
#endif
    [locationManager startUpdatingLocation];
}

- (void)viewWillAppear:(BOOL)animated {
    if (!_isAlreadyInit) {
        _isAlreadyInit = YES;
        [self getLocations];
    }
    else {
        [self getLocations];
    }
    [locationManager startUpdatingLocation];
}

- (void)viewDidDisappear:(BOOL)animated {
    [locationManager stopUpdatingLocation];
}

- (void)loadCashmodel {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:ud_LocationModel];
    if (data != nil) {
        _groupArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    NSString *tmpStr = [[NSUserDefaults standardUserDefaults] objectForKey:ud_LocationPackageNumberKey];
    if (tmpStr) _packageNumber = tmpStr;
}

- (void)saveModel {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_groupArr];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:ud_LocationModel];
        [[NSUserDefaults standardUserDefaults] setObject:_packageNumber forKey:ud_LocationPackageNumberKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    if (!mapView_) {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude
                                                                     zoom:15];
        mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        mapView_.myLocationEnabled = YES;
        mapView_.settings.myLocationButton = YES;
        self.view = mapView_;
        
        UIButton *filterButt = [UIButton buttonWithType:UIButtonTypeSystem];
        [filterButt setTitle:@"Фильтр" forState:UIControlStateNormal];
        filterButt.frame = (CGRect){5, 70, 70, 35};
        filterButt.layer.borderWidth = 1;
        filterButt.layer.cornerRadius = 5;
        filterButt.layer.borderColor = UIColorFromRGB(0x006fff).CGColor;
        [filterButt addTarget:self action:@selector(filterButtTouchUp:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:filterButt];
    }
    _isMapUpdated = YES;
    if (_groupArr.count > 0) {
        [self reloadMarkers];
    }
}

- (void)filterButtTouchUp:(UIButton *)sender {
    if (_groupArr.count == 0) return;
    NSMutableArray *sourceArr = [[NSMutableArray alloc] init];
    TSTAbstractClassifyerModel *model = [TSTAbstractClassifyerModel new];
    model.cellText = @"Все";
    model.cellId = @"-1";
    [sourceArr addObject:model];
    if (!_filterArr.count) {
        [_filterArr addObject:model];
    }
    
    for (TSTLocationGroup *group in _groupArr) {
        model = [TSTAbstractClassifyerModel new];
        model.cellText = group.name;
        model.cellId = group.groupId;
        [sourceArr addObject:model];
    }
    
    TSTClassifyerVC *classifyerVC = [[TSTClassifyerVC alloc] init];
    classifyerVC.view.frame = (CGRect){0,0,250, 44 * sourceArr.count + 5};
    classifyerVC.sourceArr = sourceArr;
    classifyerVC.selectedArr = _filterArr;
    classifyerVC.isMultipleChoose = YES;
    classifyerVC.isFirstElementIsAll = YES;

    KGModal *modal = [KGModal sharedInstance];
    //modal.tapOutsideToDismiss = NO;
    __weak TSTLocationVC *this = self;
    modal.dissmissBlock = ^(){
        _filterArr = classifyerVC.selectedArr;
        [this filterDidChanged];
    };
    [modal showWithContentView:classifyerVC.view];
    _activeModal = modal;
}

- (void)reloadMarkers {
    [self filterDidChanged];
}

- (void)filterDidChanged {
    [_markersArr removeAllObjects];
    [mapView_ clear];
    NSMutableDictionary *allShops = [[NSMutableDictionary alloc] init];
    if ((_filterArr.count == 1 && [((TSTAbstractClassifyerModel *)[_filterArr objectAtIndex:0]).cellId isEqualToString:@"-1"]) || _filterArr.count == 0) {
        for (TSTLocationGroup *group in _groupArr) {
            for (TSTLocationShop *shop in group.shops) {
                [allShops setObject:shop forKey:shop.shopId];
            }
        }
    }
    else {
        for (TSTAbstractClassifyerModel *model in _filterArr) {
            for (TSTLocationGroup *group in _groupArr) {
                if ([group.groupId isEqualToString:model.cellId]) {
                    for (TSTLocationShop *shop in group.shops) {
                        [allShops setObject:shop forKey:shop.shopId];
                    }
                }
            }
        }
    }
    for (TSTLocationShop *shop in [allShops allValues]) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(shop.location.x, shop.location.y);
        marker.title = shop.name;
        marker.snippet = shop.address;
        marker.map = mapView_;
        [_markersArr addObject:marker];
    }
}

- (void)getLocations {
    NSString *ident = [[NSUserDefaults standardUserDefaults] objectForKey:ud_DeviceTokenKey];
    NSString *cardID = [[NSUserDefaults standardUserDefaults] objectForKey:ud_CardIDKey];
    NSURL *baseURL = [NSURL URLWithString:[[TSTSharedDef sharedDef] sBaseUrl]];
    NSString *signStr = [NSString stringWithFormat:@"%@%@%@%@%@",  cardID, ksh_DefCD, ident, _packageNumber, ksh_DefKey];
    NSString *soapBody = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                          "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                          "<soap:Header/>"
                          "<soap:Body>"
                          "<tem:GetDepartments>"
                          "<tem:cardId>%@</tem:cardId>"
                          "<tem:cd>%@</tem:cd>"
                          "<tem:ident>%@</tem:ident>"
                          "<tem:sign>%@</tem:sign>"
                          "<tem:packageNumber>%@</tem:packageNumber>"
                          "</tem:GetDepartments>"
                          "</soap:Body>"
                          "</soap:Envelope>", cardID, ksh_DefCD, ident, [[signStr MD5String] lowercaseString], _packageNumber];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:baseURL];
    [request setValue:@"http://tempuri.org/IMobyService/GetDepartments" forHTTPHeaderField:@"SOAPAction"];
    [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%zd", [soapBody length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    request.timeoutInterval = 20;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    operation.securityPolicy = securityPolicy;
    __weak TSTLocationVC *this = self;
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        _isAlreadyInit = YES;
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSRange r = [str rangeOfString:@"<GetDepartmentsResult>"];
        if (r.length > 0) {
            str = [str substringFromIndex:r.length + r.location];
            r = [str rangeOfString:@"</GetDepartmentsResult>"];
            if (r.length > 0) {
                str = [[str substringToIndex:r.location] xmlSimpleUnescapeString];
                NSError *err = nil;
                TBXML *tbx = [TBXML newTBXMLWithXMLString:str error:&err];
                if (tbx.rootXMLElement) {
                    NSMutableArray *tmpArr = [NSMutableArray new];
                    [tmpArr addObjectsFromArray:_groupArr];
                    [_groupArr removeAllObjects];
                    [_groupDict removeAllObjects];
                    [this traverseElement:tbx.rootXMLElement];
                    if (_groupArr.count > 0) {
                        [self saveModel];
                        if (_isMapUpdated)
                            [self reloadMarkers];
                    }
                    else {
                        _groupArr = tmpArr;
                        for (TSTLocationGroup *group in _groupArr) {
                            [_groupDict setObject:group forKey:group.groupId];
                        }
                    }

                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ALToastView toastInView:self.view withText:@"Ошибка подключения к серверу"];
        NSLog(@"%@", error);
    }];
    
    [operation start];
}

- (BOOL)traverseElement:(TBXMLElement *)element {
    do {
        if (element->firstChild) {
            if ([self traverseElement:element->firstChild])
                return YES;
        }
        if ([[TBXML elementName:element] isEqualToString:@"group"]) {
            TSTLocationGroup *newGroup = [TSTLocationGroup new];
            
            [TBXML iterateAttributesOfElement:element withBlock:^(TBXMLAttribute *attribute, NSString *attributeName, NSString *attributeValue) {
                if ([attributeName isEqualToString:@"cat_id"]) {
                    newGroup.groupId = attributeValue;
                }
                else if ([attributeName isEqualToString:@"cat_name"]) {
                    newGroup.name =  [attributeValue stringByReplacingOccurrencesOfString:@"&ampquot;" withString:@"\""];
                }
            }];
            [_groupArr addObject:newGroup];
            [_groupDict setObject:newGroup forKey:newGroup.groupId];
        }
        else if ([[TBXML elementName:element] isEqualToString:@"shops"]) {
            [TBXML iterateAttributesOfElement:element withBlock:^(TBXMLAttribute *attribute, NSString *attributeName, NSString *attributeValue) {
                if ([attributeName isEqualToString:@"current_package"]) {
                    _packageNumber = attributeValue;
                }
            }];
        }
        else if ([[TBXML elementName:element] isEqualToString:@"shop"]) {
            TSTLocationShop *newShop = [TSTLocationShop new];
            __block NSString *categoryId = @"";
            [TBXML iterateAttributesOfElement:element withBlock:^(TBXMLAttribute *attribute, NSString *attributeName, NSString *attributeValue) {
                if ([attributeName isEqualToString:@"shop_id"]) {
                    newShop.shopId = attributeValue;
                }
                else if ([attributeName isEqualToString:@"shop_name"]) {
                    newShop.name = [attributeValue stringByReplacingOccurrencesOfString:@"&ampquot;" withString:@"\""];
                }
                else if ([attributeName isEqualToString:@"category_id"]) {
                    categoryId = attributeValue;
                }
                else if ([attributeName isEqualToString:@"geo"]) {
                    NSArray *arr = [attributeValue componentsSeparatedByString:@","];
                    if (arr.count == 2) {
                        newShop.location = CGPointMake([[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue]);
                    }
                }
                else if ([attributeName isEqualToString:@"address"]) {
                    newShop.address = attributeValue;
                }
            }];
            TSTLocationGroup *group = [_groupDict objectForKey:categoryId];
            if (group) {
                [group.shops addObject:newShop];
            }
        }
    } while ((element = element->nextSibling));
    
    return NO;
}

@end
